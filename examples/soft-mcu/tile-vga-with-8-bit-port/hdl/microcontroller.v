//+------------------------------------------------------------------------------------------------+
//| Softcore microcontroller with a tile-based video generator for VGA and a single 8-bit port.    |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module microcontroller (
  input clk_brd,            //On-board clock input
  input [7:0] gpip0,        //Button input port
  output [3:0] tvd0_red,    //VGA color component signals
  output [3:0] tvd0_green,
  output [3:0] tvd0_blue,
  output tvd0_h_sync,       //VGA synchronization signals
  output tvd0_v_sync
);
  //Signals related to the PLL/reset generator.
  wire clk_sys;
  wire reset;

  //Signals related to the processor.
  wire cpu0_rd_en;
  wire [3:0] cpu0_wr_en;
  wire [31:2] cpu0_addr;
  wire [31:0] cpu0_rd_data;
  wire [31:0] cpu0_wr_data;

  //Signals related to the RAM.
  wire cpu_mem0_rd_en;
  wire [3:0] cpu_mem0_wr_en;
  wire [31:0] cpu_mem0_rd_data;

  //Signals related to the tile-based video generator.
  wire [3:0] tvd0_wr_en;

  //I/O control signals.
  reg io_read;

  //Signals related to the input port.
  reg [7:0] gpip0_latch;

  //PLL/reset generator instance.
  pll_reset #(
    .clk_freq_in(`BOARD_CLOCK),
    .clk_freq_out(`SYSTEM_CLOCK)
  )
  pll_reset_gen (
    .clk_in(clk_brd),
    .clk_out(clk_sys),
    .reset(reset)
  );

  //Tiny-rv32e processor instance.
  tiny_rv32e_cpu cpu0 (
    .clk(clk_sys),
    .reset(reset),
    .hold(1'd0),
    .rd_en(cpu0_rd_en),
    .wr_en(cpu0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //RAM instance.
  ram_32rw_4lwe #(
    .init_file(`CPU_MEM0_INIT_FILE),
    .length(`CPU_MEM0_LENGTH)
  )
  cpu_mem0 (
    .clk(clk_sys),
    .rd_en(cpu_mem0_rd_en),
    .wr_en(cpu_mem0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu_mem0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //Tile-based video generator instance.
  tile_video_vga #(
    .clk_freq(`SYSTEM_CLOCK),
    .tilemap_init_file(`TVD0_TILEMAP_INIT_FILE),
    .patterns_1bpp(`TVD0_1BPP_PATTERNS),
    .patterns_2bpp(`TVD0_2BPP_PATTERNS),
    .patterns_4bpp(`TVD0_4BPP_PATTERNS),
    .pattern_init_file_1bpp(`TVD0_1BPP_PATTERN_INIT_FILE),
    .pattern_init_file_2bpp(`TVD0_2BPP_PATTERN_INIT_FILE),
    .pattern_init_file_4bpp(`TVD0_4BPP_PATTERN_INIT_FILE),
    .palette_init_file(`TVD0_PALETTE_INIT_FILE)
  )
  tvd0 (
    //System signals.
    .clk(clk_sys),
    .reset(reset),

    //Processor bus signals.
    .wr_en(tvd0_wr_en),
    .addr(cpu0_addr),
    .wr_data(cpu0_wr_data),

    //Video output signals.
    .red(tvd0_red),
    .green(tvd0_green),
    .blue(tvd0_blue),
    .h_sync(tvd0_h_sync),
    .v_sync(tvd0_v_sync)
  );

  //Decoding logic for multiplexing data reads from memory and I/O.
  always @(posedge clk_sys) begin
    if (reset)
      io_read <= 1'b0;
    else
      //Addresses from 0x40000000 are mapped to I/O.
      io_read <= cpu0_rd_en && (cpu0_addr[31:30] != 2'd0);
  end

  //Read data assignment for CPU0.
  assign cpu0_rd_data = io_read? { 24'd0, gpip0_latch }: cpu_mem0_rd_data;

  //Read and write enable assignment for RAM.
  assign cpu_mem0_rd_en = (cpu0_addr[31:30] == 2'd0)? cpu0_rd_en: 1'b0;
  assign cpu_mem0_wr_en = (cpu0_addr[31:30] == 2'd0)? cpu0_wr_en: 4'h0;

  //Write enable assignment for video generator.
  assign tvd0_wr_en = (cpu0_addr[31:30] == 2'd1)? cpu0_wr_en: 4'h0;

  //Input port definition.
  always @(posedge clk_sys) begin
    if (cpu0_rd_en && (cpu0_addr[31:30] == 2'd2))
      gpip0_latch <= gpip0;
    else
      gpip0_latch <= 8'd0;
  end
endmodule
