//+------------------------------------------------------------------------------------------------+
//| Testbench for the softcore microcontroller with a tile-based video generator for VGA and a     |
//| single 8-bit port.                                                                             |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

`timescale 1ns/1ps

module testbench();
  //Softcore microcontroller ports.
  wire [7:0] gpip0;       //Button input port
  wire [3:0] tvd0_red;    //VGA color component signals
  wire [3:0] tvd0_green;
  wire [3:0] tvd0_blue;
  wire tvd0_h_sync;       //VGA synchronization signals
  wire tvd0_v_sync;

  //Main processor registers.
  wire [31:0] x1;
  wire [31:0] x2;
  wire [31:0] x3;
  wire [31:0] x4;
  wire [31:0] x5;
  wire [31:0] x6;
  wire [31:0] x7;
  wire [31:0] x8;
  wire [31:0] x9;
  wire [31:0] x10;
  wire [31:0] x11;
  wire [31:0] x12;
  wire [31:0] x13;
  wire [31:0] x14;
  wire [31:0] x15;

  //Memory/peripheral address.
  wire [31:0] addr;

  //Disassembled instruction string.
  wire [64*8:1] instruction;

  //Memory arrays are not dumped to LXT files by default. Copy all register values from the CPU so
  //they can be easily viewed.
  assign x1 = mcu.cpu0.regs.x[1],
         x2 = mcu.cpu0.regs.x[2],
         x3 = mcu.cpu0.regs.x[3],
         x4 = mcu.cpu0.regs.x[4],
         x5 = mcu.cpu0.regs.x[5],
         x6 = mcu.cpu0.regs.x[6],
         x7 = mcu.cpu0.regs.x[7],
         x8 = mcu.cpu0.regs.x[8],
         x9 = mcu.cpu0.regs.x[9],
         x10 = mcu.cpu0.regs.x[10],
         x11 = mcu.cpu0.regs.x[11],
         x12 = mcu.cpu0.regs.x[12],
         x13 = mcu.cpu0.regs.x[13],
         x14 = mcu.cpu0.regs.x[14],
         x15 = mcu.cpu0.regs.x[15];

  //Create a properly aligned memory/peripheral address for viewing with a waveform viewer.
  assign addr = { mcu.cpu0.addr, 2'd0 };

  //Softcore microcontroller instance.
  microcontroller mcu (
    .clk_brd(1'b0),   //No clock input provided. The PLL/reset module generates this automatically.
    .gpip0(gpip0),
    .tvd0_red(tvd0_red),
    .tvd0_green(tvd0_green),
    .tvd0_blue(tvd0_blue),
    .tvd0_h_sync(tvd0_h_sync),
    .tvd0_v_sync(tvd0_v_sync)
  );

  //Disassembler instance.
  rv32e_disassembler disassembler (
    .opcode(mcu.cpu0.rd_data),
    .enable(mcu.cpu0.state_q == mcu.cpu0.st_decode),
    .instruction(instruction)
  );

  //Initialization and control block.
  always begin
    //Generate an LXT file and dump all generated data.
    $dumpfile(`ICARUS_LXT_FILE);
    $dumpvars(0, testbench);

    //Wait for a set amount of cycles.
    if (`SYSTEM_CLOCK == 50000000)
      repeat (1000000) @(posedge mcu.clk_sys);
    else if (`SYSTEM_CLOCK == 25000000)
      repeat (500000) @(posedge mcu.clk_sys);
    else begin
      $display("Invalid parameter value: SYSTEM_CLOCK");
      #1;
    end

    //All done, finish simulation.
    $finish;
  end

  //Test data for the input port.
  assign gpip0 = 8'h55;
endmodule
