//+------------------------------------------------------------------------------------------------+
//| Softcore microcontroller with an UART and a single 8-bit port.                                 |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

//Microcontroller module.
module microcontroller (
  input clk,
  input uart0_rx,
  output uart0_tx,
  output reg [7:0] gpop0
);
  //Signals related to the reset generator.
  wire reset;

  //Signals related to the processor.
  wire cpu0_rd_en;
  wire [3:0] cpu0_wr_en;
  wire [31:2] cpu0_addr;
  wire [31:0] cpu0_rd_data;
  wire [31:0] cpu0_wr_data;

  //Signals related to the RAM.
  wire cpu_mem0_rd_en;
  wire [3:0] cpu_mem0_wr_en;
  wire [31:0] cpu_mem0_rd_data;

  //Signals related to the UART.
  wire uart0_rd_en;
  wire [3:0] uart0_wr_en;
  wire [31:0] uart0_rd_data;

  //Signals related to the output port.
  wire gpop0_rd_en;
  wire gpop0_wr_en;
  reg [31:0] gpop0_rd_data;

  //I/O control signals.
  reg io_read;

  //Reset generator instance.
  simple_reset reset_gen (
    .clk(clk),
    .reset(reset)
  );

  //Tiny-rv32e processor instance.
  tiny_rv32e_cpu cpu0 (
    .clk(clk),
    .reset(reset),
    .hold(1'd0),
    .rd_en(cpu0_rd_en),
    .wr_en(cpu0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //RAM instance.
  ram_32rw_4lwe #(
    .init_file(`CPU_MEM0_INIT_FILE),
    .length(`CPU_MEM0_LENGTH)
  )
  cpu_mem0 (
    .clk(clk),
    .rd_en(cpu_mem0_rd_en),
    .wr_en(cpu_mem0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu_mem0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //UART instance.
  uart #(
    .clk_freq(`BOARD_CLOCK),
    .baud_rate(`UART0_BAUD_RATE)
  )
  uart0 (
    //System signals.
    .clk(clk),
    .reset(reset),

    //Processor bus signals.
    .rd_en(uart0_rd_en),
    .wr_en(uart0_wr_en),
    .addr(cpu0_addr),
    .rd_data(uart0_rd_data),
    .wr_data(cpu0_wr_data),

    //Peripheral I/O signals.
    .rx(uart0_rx),
    .tx(uart0_tx)
  );

  //Output port definition.
  always @(posedge clk) begin
    if (reset) begin
      gpop0 <= 8'd0;
      gpop0_rd_data <= 32'd0;
    end
    else begin
      //Update the output register value on CPU writes.
      if (gpop0_wr_en)
        gpop0 <= cpu0_wr_data;

      //Return the output register value on CPU reads.
      if (gpop0_rd_en)
        gpop0_rd_data <= { 24'd0, gpop0 };
      else
        gpop0_rd_data <= 32'd0;
    end
  end

  //Decoding logic for multiplexing data reads from memory and I/O.
  always @(posedge clk) begin
    if (reset)
      io_read <= 1'b0;
    else
      //Addresses from 0x40000000 are mapped to I/O.
      io_read <= cpu0_rd_en && (cpu0_addr[31:30] != 2'd0);
  end

  //Read data assignment for CPU0.
  assign cpu0_rd_data = io_read? uart0_rd_data | gpop0_rd_data : cpu_mem0_rd_data;

  //Read and write enable assignments for RAM.
  assign cpu_mem0_rd_en = (cpu0_addr[31:30] == 2'd0)? cpu0_rd_en: 1'b0;
  assign cpu_mem0_wr_en = (cpu0_addr[31:30] == 2'd0)? cpu0_wr_en: 4'h0;

  //Read and write enable assignments for UART.
  assign uart0_rd_en = (cpu0_addr[31:30] == 2'd1)? cpu0_rd_en: 1'b0;
  assign uart0_wr_en = (cpu0_addr[31:30] == 2'd1)? cpu0_wr_en: 4'h0;

  //Read and write enable assignments for the output port.
  assign gpop0_rd_en = (cpu0_addr[31:30] == 2'd2)? cpu0_rd_en: 1'b0;
  assign gpop0_wr_en = (cpu0_addr[31:30] == 2'd2)? cpu0_wr_en[0]: 1'b0;
endmodule
