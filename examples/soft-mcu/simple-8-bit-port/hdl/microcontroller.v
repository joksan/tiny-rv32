//+------------------------------------------------------------------------------------------------+
//| Simple softcore microcontroller with a single 8-bit port.                                      |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

//Microcontroller module.
module microcontroller (
  input clk,
  output reg [7:0] gpop0
);
  //Signals related to the reset generator.
  wire reset;

  //Signals related to the processor.
  wire cpu0_rd_en;
  wire [3:0] cpu0_wr_en;
  wire [31:2] cpu0_addr;
  wire [31:0] cpu0_rd_data;
  wire [31:0] cpu0_wr_data;

  //Signals related to the RAM.
  wire [3:0] cpu_mem0_wr_en;

  //Reset generator instance.
  simple_reset reset_gen (
    .clk(clk),
    .reset(reset)
  );

  //Tiny-rv32e processor instance.
  tiny_rv32e_cpu cpu0 (
    .clk(clk),
    .reset(reset),
    .hold(1'd0),
    .rd_en(cpu0_rd_en),
    .wr_en(cpu0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //RAM instance.
  ram_32rw_4lwe #(
    .init_file(`CPU_MEM0_INIT_FILE),
    .length(`CPU_MEM0_LENGTH)
  )
  cpu_mem0 (
    .clk(clk),
    .rd_en(cpu0_rd_en),
    .wr_en(cpu_mem0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //Write enable assignment for RAM.
  assign cpu_mem0_wr_en = cpu0_addr[31]? 4'h0: cpu0_wr_en;

  //Single 8-bit port definition.
  always @(posedge clk) begin
    if (reset)
      gpop0 <= 8'd0;
    else if (cpu0_addr[31] && cpu0_wr_en[0])
      gpop0 <= cpu0_wr_data[7:0];
  end
endmodule
