#+-------------------------------------------------------------------------------------------------+
#| Makefile for the simple softcore microcontroller with a single 8-bit port.                      |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Set the softcore MCU path to the same location as this makefile.
SIMPLE_8_BIT_PORT_MCU_PATH := $(shell realpath --relative-to . $(dir $(lastword $(MAKEFILE_LIST))))

#Set the constraint path to the constraint subdirectory.
HDL_CONSTRAINT_PATH = $(SIMPLE_8_BIT_PORT_MCU_PATH)/constraint

#Add the hdl subdirectory to the HDL source paths.
HDL_SRC_PATHS += $(SIMPLE_8_BIT_PORT_MCU_PATH)/hdl

#Add the HDL sources for this softcore MCU.
HDL_COMMON_SOURCES += microcontroller.v
HDL_SIM_SOURCES += testbench.v

#Set the top module for this softcore.
HDL_SYNTH_TOP_MODULE = microcontroller

#Add the base modules used by this softcore MCU.
BASE_MODULES += simple-reset
BASE_MODULES += ram-32rw-4lwe

#Add icarus to the HDL tools, so this MCU can be simulated.
HDL_TOOLS += icarus

#Register the memory for the processor at address 0 with a default size of 1024 words (4KiB).
$(eval $(call F_ADD_CPU_BRAM,cpu-mem0,cpu_mem0,cpu0,0,1024))

#Register cpu0 as a tiny-rv32e softcore, using the single-bram-region memory profile and minimal
#startup profile.
$(eval $(call F_ADD_SOFT_CPU,cpu0,tiny-rv32e,single-bram-region,minimal-startup))

#Definitions for the microcontroller module.
HDL_COMMON_DEFINES += CPU_MEM0_LENGTH=$(cpu-mem0-length)
HDL_SYNTH_DEFINES += CPU_MEM0_INIT_FILE=\"$(call F_SYNTH_BIF_COND,cpu-mem0)\"
HDL_SIM_DEFINES += CPU_MEM0_INIT_FILE=\"$(call F_SIM_BIF_COND,cpu-mem0)\"
HDL_SIM_DEFINES += BOARD_CLOCK=$(BOARD_CLOCK)

#Pass the CPU clock speed as C and assembler definitions. Also pass the memory size to the linker.
cpu0-ld-defines += MEMORY_SIZE_WORDS=$(cpu-mem0-length)
cpu0-asm-defines += CPU_CLOCK=$(BOARD_CLOCK)
cpu0-c-defines += CPU_CLOCK=$(BOARD_CLOCK)

#Base addresses for peripherals.
cpu0-asm-defines += GPOP0_BASE_ADDR=0x80000000
cpu0-c-defines += GPOP0_BASE_ADDR=0x80000000
