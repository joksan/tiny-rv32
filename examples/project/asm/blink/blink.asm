#+-------------------------------------------------------------------------------------------------+
#| Example LED blink program for the tiny-rv32e softcore processor.                                |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

.text
.global main
main:
  li s0, 0x66                 #Load the initial LED pattern
  li s1, GPOP0_BASE_ADDR      #Load the port address

loop:
  sb s0, (s1)                 #Store the current LED pattern in the port
  xori s0, s0, 0xFF           #Invert the current LED pattern
  li a0, (CPU_CLOCK / 8) / 2  #Load the delay count (CPU_CLOCK / cycles_per_loop / output_freq)
  call delay                  #Wait for half a second
  j loop                      #Do everything again

delay:
  addi a0, a0, -1             #Decrease the delay count
  bltu zero, a0, delay        #Repeat until the delay count reaches zero
  ret                         #Delay finished
