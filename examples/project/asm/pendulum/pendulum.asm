#+-------------------------------------------------------------------------------------------------+
#| Example LED pendulum program for the tiny-rv32e softcore processor.                             |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

.text
.global main
main:
  li s0, 0x01                 #Load the initial LED pattern
  li s1, GPOP0_BASE_ADDR      #Load the port address

left:
  sb s0, (s1)                 #Store the current LED pattern in the port
  li a0, (CPU_CLOCK / 8) / 4  #Load the delay count (CPU_CLOCK / cycles_per_loop / output_freq)
  call delay                  #Wait for a quarter second

  andi t0, s0, 0x40           #Test whether the second last bit is set (keep result in a temporary)
  slli s0, s0, 1              #Shift left one more time
  beq zero, t0, left          #Repeat if the second last bit was not set

right:
  sb s0, (s1)                 #Store the current LED pattern in the port
  li a0, (CPU_CLOCK / 8) / 4  #Load the delay count (CPU_CLOCK / cycles_per_loop / output_freq)
  call delay                  #Wait for a quarter second

  andi t0, s0, 0x02           #Test whether the second bit is set (keep result in a temporary)
  srli s0, s0, 1              #Shift right one more time
  beq zero, t0, right         #Repeat if the second bit was not set

  j left                      #Do everything again

delay:
  addi a0, a0, -1             #Decrease the delay count
  bltu zero, a0, delay        #Repeat until the delay count reaches zero
  ret                         #Delay finished
