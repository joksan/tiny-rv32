//+------------------------------------------------------------------------------------------------+
//| Example LED blink program for the tiny-rv32e softcore processor.                               |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>

//Define the LED port at its fixed memory address.
#define LED_PORT (*((volatile uint8_t *) GPOP0_BASE_ADDR))

void delay();

int main() {
  //Loop indefinitely.
  for (;;) {
    //Blink the LEDs with delay between transitions.
    LED_PORT = 0x55;
    delay();
    LED_PORT = 0xAA;
    delay();
  }
}

void delay() {
  //Define the loop counter as volatile to avoid elimination by optimization.
  volatile uint32_t i;

  //Wait for half a second. Period = CPU_CLOCK / cycles_per_loop / output_freq.
  //(cycles_per_loop has been determined by analysis of disassembled code and simulation).
  for (i = 0; i < CPU_CLOCK/24/2; i++);
}
