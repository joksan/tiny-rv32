#+-------------------------------------------------------------------------------------------------+
#| Makefile for the tile-based video generator example program.                                    |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Add the sources that will run inside cpu0.
cpu0-c-sources += hello.c

#Use the softcore MCU with a tile-based video generator for VGA and a single 8-bit port by default.
SOFT_MCU ?= tile-vga-with-8-bit-port

#Synthesize for the iCE40-HX8K breakout board by default.
BOARD ?= ice40hx8k-b-evn

#Use the example 8x8 font as the 1BPP pattern.
tvd0-pattern-mem-1bpp-patterns = 128
tvd0-pattern-mem-1bpp-data-src = video-data/8x8-font-1bpp.png

#Use the low-resolution tall RISC-V logo as the 2BPP pattern.
tvd0-pattern-mem-2bpp-patterns = 32
tvd0-pattern-mem-2bpp-data-src = video-data/RISC-V-Logo-Tall_2.png

#Use the example border image as the 4BPP pattern.
tvd0-pattern-mem-4bpp-patterns = 16
tvd0-pattern-mem-4bpp-data-src = video-data/borders.png

#Use the example palette as the BRAM initialization file for the color palette module.
tvd0-palette-mem-data-src = video-data/palette.data

#Include the tiny-rv32 base makefile.
include ../../../../Makefile.tiny-rv32
