//+------------------------------------------------------------------------------------------------+
//| Example program for the tiny-rv32e softcore processor with a tile-based video generator.       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>
#include <stdbool.h>

#include "tile-video.h"

//This function clears the screen.
void clear_screen() {
  for (uint16_t j = 0; j < 30; j++)
    for (uint16_t i = 0; i < 40; i++)
      TILE_VIDEO(0).tilemap_mem[j * 41 + i] = 0x8000;
}

//This function draws the screen border.
void draw_border(uint8_t rot) {
  //Draw the corners. Place a differently animated tile depending on current palette rotation.
  if (rot < 8) {
    TILE_VIDEO(0).tilemap_mem[0] = 0x0101;
    TILE_VIDEO(0).tilemap_mem[39] = 0x0100;
    TILE_VIDEO(0).tilemap_mem[41 * 29] = 0x0105;
    TILE_VIDEO(0).tilemap_mem[41 * 29 + 39] = 0x0104;
  }
  else {
    TILE_VIDEO(0).tilemap_mem[0] = 0x0103;
    TILE_VIDEO(0).tilemap_mem[39] = 0x0102;
    TILE_VIDEO(0).tilemap_mem[41 * 29] = 0x0107;
    TILE_VIDEO(0).tilemap_mem[41 * 29 + 39] = 0x0106;
  }

  //Draw the horizontal borders.
  for (uint16_t i = 0; i < 19; i++) {
    TILE_VIDEO(0).tilemap_mem[i * 2 + 1] = 0x0100;
    TILE_VIDEO(0).tilemap_mem[i * 2 + 2] = 0x0101;
    TILE_VIDEO(0).tilemap_mem[41 * 29 + i * 2 + 1] = 0x0104;
    TILE_VIDEO(0).tilemap_mem[41 * 29 + i * 2 + 2] = 0x0105;
  }

  //Draw the vertical borders.
  for (uint16_t i = 0; i < 14; i++) {
    TILE_VIDEO(0).tilemap_mem[i * 82 + 41] = 0x0107;
    TILE_VIDEO(0).tilemap_mem[i * 82 + 82] = 0x0103;
    TILE_VIDEO(0).tilemap_mem[i * 82 + 80] = 0x0106;
    TILE_VIDEO(0).tilemap_mem[i * 82 + 121] = 0x0102;
  }
}

//This function performs animation through palette rotation.
void rotate_border_palette(uint8_t rot) {
  //Set both halves of the color palette to white and black around the rotation position.
  for (uint8_t i = 0; i < 16; i++)
    TILE_VIDEO(0).palette_mem[16 + i] = (((i + rot) & 0x0F) < 8)? 0x0FFF: 0x0000;
}

//This function draws/erases a RISC-V logo at an arbitrary screen location.
void show_logo(uint16_t x, uint16_t y, bool enable) {
  //If enable is true draw the logo, else erase it.
  if (enable) {
    //Logo dimension is 6x5 tiles or 48x40 pixel.
    for (uint16_t j = 0; j < 5; j++)
      for (uint16_t i = 0; i < 6; i++)
        //The image is composed by consecutive patterns numbered from 0 to 29. Also, logical screen
        //is 41 tiles wide.
        TILE_VIDEO(0).tilemap_mem[(y + j) * 41 + x + i] = 0x4200 | (j * 6 + i);
  }
  else {
    for (uint16_t j = 0; j < 5; j++)
      for (uint16_t i = 0; i < 6; i++)
        TILE_VIDEO(0).tilemap_mem[(y + j) * 41 + x + i] = 0x8000;
  }
}

//This function draws an ascii-encoded text string to the screen at the specified tile coordinates
//using the specified palette range. 1BPP patterns are used to generate text.
void print_str(const char *str, uint16_t x, uint16_t y, uint8_t pal) {
  //Calculate starting tilemap location.
  volatile uint16_t *dst = &TILE_VIDEO(0).tilemap_mem[41 * y + x];

  //Draw the characters until reaching the zero terminator. Each tile location has its palette
  //selection fields in the higher byte. Pattern selection field is in the lower byte.
  while (*str != 0)
    *dst++ = *str++ | 0x8000 | pal << 8;
}

int main() {
  volatile uint32_t i;      //Delay loop counter - volatile avoids elimination by optimization
  uint8_t rot = 0;          //Current palette rotation position for border animation
  uint16_t x = 0, y = 0;    //Initial coordinates are in the top-left of the screen
  uint16_t dx = 1, dy = 1;  //Initial displacement goes in the down-right direction

  clear_screen();

  //Perform an animation loop forever.
  for (;;) {
    //Erase the logo at old coordinates.
    show_logo(x, y, false);

    //Update the logo coordinates. Have the logo bounce at screen edges.
    x += dx;
    dx = (x >= 40 - 6)? -1: (x == 0)? 1: dx;
    y += dy;
    dy = (y >= 30 - 5)? -1: (y == 0)? 1: dy;

    //Perform screen draw updates for 8 palette cycles.
    for (uint8_t j = 0; j < 8; j++) {
      //Draw the screen border first, then show the logo at new cordinates, finally draw the text.
      draw_border(rot);
      show_logo(x, y, true);
      print_str("Hello world from RISC-V!", 8, 8, 2);

      //Animate the screen border by performing palette rotation.
      rotate_border_palette(rot);
      rot = (rot + 1) & 0x0F;

      //Wait for 1/16 of a second. Period = CPU_CLOCK / cycles_per_loop / output_freq.
      //(cycles_per_loop has been determined by analysis of disassembled code and simulation).
      for (i = 0; i < CPU_CLOCK/24/16; i++);
    }
  }
}
