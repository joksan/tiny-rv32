//+------------------------------------------------------------------------------------------------+
//| Example UART program for the tiny-rv32e softcore processor.                                    |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include "uart.h"

//Shorthand macros for accessing the UART status bits.
#define UART_TXREADY() (UART(0).status_bits.txready)
#define UART_RXREADY() (UART(0).status_bits.rxready)

void uart_send(char c) {
  //Wait until UART(0) is ready for transmission, then write to the data register to transmit.
  while (!UART_TXREADY());
  UART(0).data = c;
}

void uart_send_str(const char *p_str) {
  //Send individual characters until reaching the end of the string.
  while (*p_str != '\0')
    uart_send(*p_str++);
}

char uart_recv() {
  //Wait until UART(0) has completed receiving a byte, then read the data register and return it.
  while (!UART_RXREADY());
  return UART(0).data;
}

void delay() {
  //Define the loop counter as volatile to avoid elimination by optimization.
  volatile uint32_t i;

  //Wait for a second. Period = CPU_CLOCK / cycles_per_loop / output_freq.
  //(cycles_per_loop has been determined by analysis of disassembled code and simulation).
  for (i = 0; i < CPU_CLOCK/24/1; i++);
}

int main() {
  char c;

  //Loop until a character is received.
  while (!UART_RXREADY()) {
    uart_send_str("Hello world!\n");
    delay();
  }

  uart_send_str("UART reception test\n");
  uart_send_str("Received characters will be echoed back with reversed capitalization\n");

  //Loop indefinitely.
  for (;;) {
    //Wait for the next charater and read it.
    c = uart_recv();

    //Reverse capitalization.
    if (c >= 'a' && c <= 'z')
      c += 'A' - 'a';
    else if (c >= 'A' && c <= 'Z')
      c -= 'A' - 'a';

    //Echo the character.
    uart_send(c);
  }
}
