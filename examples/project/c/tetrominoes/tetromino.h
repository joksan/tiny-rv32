//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Header for the tetromino manipulation routines.                                                |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef TETROMINO_H_
#define TETROMINO_H_

#include <stdint.h>

//Enumeration of all tetromino types.
typedef enum {
  TT_I = 0, TT_O, TT_T, TT_J, TT_L, TT_S, TT_Z,
} tetromino_type_t;

//Tile array for the current tetromino.
extern uint8_t tetromino_tiles[4][4];

//Tetromino manipulation functions.
void set_tetromino(tetromino_type_t type, uint8_t palette);
void rotate_tetromino_left();
void rotate_tetromino_right();

#endif //TETROMINO_H_
