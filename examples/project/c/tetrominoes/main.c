//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Main code and finite state machine module.                                                     |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>
#include <stdbool.h>

#include "draw.h"
#include "tetromino.h"
#include "playing-field.h"
#include "config.h"

//Define the button port at its fixed memory address.
#define BUTTON_PORT (*((volatile uint8_t *) GPIP0_BASE_ADDR))

//States of the game FSM.
typedef enum {
  s_drop_start, s_drop, s_line_complete,
} state_t;

//Button state structure.
typedef struct {
  bool previous;  //Previous button state
  bool current;   //Current button state
  bool pulse;     //Button has been pulsed/pushed recently
} button_state_t;

//State array for all 8 possible buttons.
static button_state_t buttons[8];

//Current coordinates of the tetromino in play.
static int32_t tetromino_x = 0;
static int32_t tetromino_y = 0;

//Last generated random number. Used to sequence the pseudo random number generator.
static uint32_t random_current = 1;

static void init_buttons() {
  //Initialize the button array state so no false pulse events are triggered at startup.
  for (uint32_t i = 0; i < sizeof(buttons) / sizeof(buttons[0]); i++)
    buttons[i].previous = false;
}

static void read_buttons() {
  uint32_t mask = 0x01;
  uint32_t i;

  //Update all buton states: Store previous state, read current state from the port and detect
  //pulse events.
  for (i = 0; i < sizeof(buttons) / sizeof(buttons[0]); i++) {
    buttons[i].previous = buttons[i].current;
    buttons[i].current = ((BUTTON_PORT & mask) != 0) == BUTTON_ACTIVE_LEVEL;
    buttons[i].pulse = !buttons[i].previous && buttons[i].current;
    mask <<= 1;
  }
}

static bool do_controls() {
  bool retval = false;

  //If the move-left button is pulsed, try moving the tetromino left by one tile and check for
  //collisions. If there's a collision undo the movement, else set the return value to true to tell
  //the main loop that an screen redraw is required.
  if (buttons[BUTTON_MOVE_LEFT].pulse) {
    tetromino_x--;
    if (collision(tetromino_x, tetromino_y))
      tetromino_x++;
    else
      retval = true;
  }

  //Repeat the above logic for the remaining buttons.
  if (buttons[BUTTON_MOVE_RIGHT].pulse) {
    tetromino_x++;
    if (collision(tetromino_x, tetromino_y))
      tetromino_x--;
    else
      retval = true;
  }

  if (buttons[BUTTON_ROTATE_LEFT].pulse) {
    rotate_tetromino_left();
    if (collision(tetromino_x, tetromino_y))
      rotate_tetromino_right();
    else
      retval = true;
  }

  if (buttons[BUTTON_ROTATE_RIGHT].pulse) {
    rotate_tetromino_right();
    if (collision(tetromino_x, tetromino_y))
      rotate_tetromino_left();
    else
      retval = true;
  }

  return retval;
}

static uint32_t random_next(uint32_t r) {
  const uint32_t poly = 0xB4BCD35C;

  //This is a typical LFSR implementation. For reference, see:
  //https://en.wikipedia.org/wiki/Linear-feedback_shift_register (basic principles),
  //http://users.ece.cmu.edu/~koopman/lfsr/index.html (code implementation) and
  //https://www.maximintegrated.com/en/app-notes/index.mvp/id/4400 (improving statistical
  //properties).

  //By repeating the process by a factor of the period, random numbers with better statistical
  //properties can be generated. This comes at the cost of a reduced period and increased processor
  //overhead, so the smallest factor for 32 bits (3 in this case) is chosen.
  for (uint32_t i = 0; i < 3; i++) {
    if (r & 1)
      r = (r >> 1) ^ poly;
    else
      r = r >> 1;
  }

  return r;
}

static void new_random_tetromino() {
  static tetromino_type_t last_tetromino = 0;   //Initialize to invalid (no initial match)
  tetromino_type_t new_tetromino;
  uint8_t palette;

  //Relocate tetromino coordinates to the top.
  tetromino_x = 3;
  tetromino_y = -4;

  //Select a new pseudo random tetromino. This implementation is inspired by the NES version of the
  //game, which is in fact biased. This causes the human perception of randomness to the player.
  for (uint32_t i = 0; i < 2; i++) {
    //Generate a pseudo random number between 0 and 6. The number is chosen by simply taking the 3
    //LSB of the generated random number. Discard 7, as only 7 tetrominoes exist (numbered 0-6).
    do {
      random_current = random_next(random_current);
      new_tetromino = random_current & 0x7;
    } while (new_tetromino == 7);

    //If the new tetromino is different from the previous one, keep it. Else try to select a new one
    //once more. If the same tetromino is selected again, then the selection is final.
    if (new_tetromino != last_tetromino)
      break;
  }

  //Now choose a palette at random. Range is 1 to 4, so take the 2 LSB and add 1.
  random_current = random_next(random_current);
  palette = (random_current & 0x03) + 1;

  //Set the new tetromino, then remember it for the next time.
  set_tetromino(new_tetromino, palette);
  last_tetromino = new_tetromino;
}

static void delay(uint32_t ms) {
  //Define the loop counter as volatile to avoid elimination by optimization.
  volatile uint32_t i;

  for (; ms > 0; ms--) {
    //Wait for a milli second. Period = CPU_CLOCK / cycles_per_loop / 1000.
    //(cycles_per_loop has been determined by analysis of disassembled code and simulation).
    for (i = 0; i < CPU_CLOCK/24/1000; i++);
  }
}

static void redraw_screen() {
  //Redraw all screen segments that require periodic updates.
  draw_deploy_area();
  draw_field_tiles();
  draw_tetromino_tiles(tetromino_x, tetromino_y);
}

int main() {
  state_t state = s_drop_start;
  uint32_t time_counter = 0;
  uint32_t line_counter = 0;
  uint32_t tile_counter = 0;

  //Initialize game state.
  init_buttons();
  clear_field_tiles();
  new_random_tetromino();
  draw_field_border();
  redraw_screen();

  //Game FSM.
  for (;;) {
    //Update input controls.
    read_buttons();

    switch (state) {
      case s_drop_start:
        //This state allows to move the tetromino without falling for a short period.

        //Perform user actions (excludes pressing the move-down key). Redraw if user input was
        //received.
        if (do_controls())
          redraw_screen();

        time_counter++;

        //Wait for the time counter to expire or the user to pulse the move-down key to transition
        //to the next state.
        if (time_counter >= 200 || buttons[BUTTON_MOVE_DOWN].pulse) {
          time_counter = 0;
          state = s_drop;
        }

        break;
      case s_drop:
        //This state allows to move the tetromino while falling until it lands onto something.

        //Perform user actions. Redraw if user input was received.
        if (do_controls())
          redraw_screen();

        time_counter++;

        //Wait for the time counter to expire or the user to press the move-down key to trigger the
        //tetromino falling event.
        if (time_counter >= 25 || buttons[BUTTON_MOVE_DOWN].current) {
          time_counter = 0;

          //Attempt to move the tetromino down by one tile.
          tetromino_y++;
          if (collision(tetromino_x, tetromino_y)) {
            //There is a collision. Have the tetromino go back one tile and try to place it.
            tetromino_y--;
            if (place_tetromino(tetromino_x, tetromino_y)) {
              //Tetromino placement successful.
              if (complete_line_count) {
                //Lines were completed. Switch state to animate them.
                state = s_line_complete;
              }
              else {
                //No lines completed. Dispatch a new tetromino and switch state.
                new_random_tetromino();
                state = s_drop_start;
              }
            }
            else {
              //Tetromino could not be placed (game is over). Clear the field, dispatch a new
              //tetromino and switch state to start over.
              clear_field_tiles();
              new_random_tetromino();
              state = s_drop_start;
            }
          }

          redraw_screen();
        }

        break;
      case s_line_complete:
        //This state performs the line completion animation.

        time_counter++;

        //Wait for the time counter to expire so the next line animation step can be performed.
        if (time_counter >= 5) {
          time_counter = 0;

          //Erase the next pair of tiles, starting from the center, then redraw the field.
          field_tiles[complete_lines[line_counter]][4-tile_counter] = 0;
          field_tiles[complete_lines[line_counter]][5+tile_counter] = 0;
          draw_field_tiles();

          //Keep track of the erased tile count.
          tile_counter++;
          if (tile_counter >= 5) {
            tile_counter = 0;
            //Once 5 tile pairs have been erased, increase the line count.
            line_counter++;
          }

          //Keep track of the erased line count.
          if (line_counter >= complete_line_count) {
            line_counter = 0;
            //Once all lines have been erased, move the field tiles down, select a new tetromino at
            //random, redraw the screen and switch state.
            move_field_tiles_down();
            new_random_tetromino();
            redraw_screen();
            state = s_drop_start;
          }
        }

        break;
    }

    //Delay by 10ms (program loop updates at about 100 times per second).
    delay(10);
  }
}
