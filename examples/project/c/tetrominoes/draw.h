//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Header for the screen drawing routines.                                                        |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef DRAW_H_
#define DRAW_H_

#include <stdint.h>

//Screen drawing functions.
void draw_field_border();
void draw_deploy_area();
void draw_field_tiles();
void draw_tetromino_tiles(int32_t px, int32_t py);

#endif //DRAW_H_
