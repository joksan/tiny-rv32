//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Screen drawing routines.                                                                       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>
#include <stdbool.h>

#include "tile-video.h"

#include "tetromino.h"
#include "playing-field.h"
#include "config.h"

//Macro used to compose an screen tile value for a block.
#define BLOCK(PALETTE, PATTERN) (0x4000 | (PALETTE << 8) | PATTERN)

#define BORDER_PALETTE 0
#define BLANK_BLOCK 0

void draw_field_border() {
  //Draw the Corners.
  TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 5) * 41 + FIELD_X - 1] = BLOCK(BORDER_PALETTE, 4);
  TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 5) * 41 + FIELD_X + 10] = BLOCK(BORDER_PALETTE, 5);
  TILE_VIDEO(0).tilemap_mem[(FIELD_Y + 20) * 41 + FIELD_X - 1] = BLOCK(BORDER_PALETTE, 6);
  TILE_VIDEO(0).tilemap_mem[(FIELD_Y + 20) * 41 + FIELD_X + 10] = BLOCK(BORDER_PALETTE, 7);

  //Draw the top and bottom sides.
  for (uint32_t i = 0; i < 10; i++) {
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 5) * 41 + FIELD_X + i] = BLOCK(BORDER_PALETTE, 3);
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y + 20) * 41 + FIELD_X + i] = BLOCK(BORDER_PALETTE, 3);
  }

  //Draw the left and right sides.
  for (uint32_t i = 0; i < 24*41; i += 41) {
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 4) * 41 + FIELD_X - 1 + i] = BLOCK(BORDER_PALETTE, 2);
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 4) * 41 + FIELD_X + 10 + i] = BLOCK(BORDER_PALETTE, 2);
  }
}

void draw_deploy_area() {
  for (uint32_t i = 0; i < 10; i++) {
    //Clean the blank space.
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 4) * 41 + FIELD_X + i] = BLANK_BLOCK;
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 3) * 41 + FIELD_X + i] = BLANK_BLOCK;
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 2) * 41 + FIELD_X + i] = BLANK_BLOCK;
    //Draw the dotted line.
    TILE_VIDEO(0).tilemap_mem[(FIELD_Y - 1) * 41 + FIELD_X + i] = BLOCK(BORDER_PALETTE, 8);
  }
}

void draw_field_tiles() {
  uint32_t tx, ty;
  uint32_t x, y;

  //Draw the content of the playing field tiles array onto the screen.
  for (ty = 0, y = FIELD_Y * 41; ty < 20; ty++, y += 41)
    for (tx = 0, x = FIELD_X; tx < 10; tx++, x++)
      TILE_VIDEO(0).tilemap_mem[x + y] =
        (field_tiles[ty][tx] != 0)? BLOCK(field_tiles[ty][tx], 1): BLANK_BLOCK;
}

void draw_tetromino_tiles(int32_t px, int32_t py) {
  int32_t tx, ty;
  int32_t x, y;

  //Draw the content of the tetromino tiles array onto the screen.
  for (ty = 0, y = (FIELD_Y + py) * 41; ty < 4; ty++, y += 41)
    for (tx = 0, x = FIELD_X + px; tx < 4; tx++, x++)
      if (tetromino_tiles[ty][tx] != 0)
        TILE_VIDEO(0).tilemap_mem[x + y] = BLOCK(tetromino_tiles[ty][tx], 1);
}
