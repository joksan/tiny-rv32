//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Tetromino manipulation routines.                                                               |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdbool.h>
#include <stdint.h>

#include "tetromino.h"

//Tile array for the current tetromino. This is handled as an array to generalize handling and
//collision detection. If a location is non-zero, there is a tile is in that position. Each
//value encodes the color palette of the tile.
uint8_t tetromino_tiles[4][4];

//Type of the tetromino currently in play.
static tetromino_type_t tetromino_type;

//Tetromino templates. Used to initialize the tetromino tile array when a new tetromino type is set.
static const bool tetromino_template[7][4][4] = {
  { //I tetromino
    { 0, 0, 0, 0, },
    { 1, 1, 1, 1, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
  },
  { //O tetromino
    { 0, 0, 0, 0, },
    { 0, 1, 1, 0, },
    { 0, 1, 1, 0, },
    { 0, 0, 0, 0, },
  },
  { //T tetromino
    { 0, 1, 0, 0, },
    { 1, 1, 1, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
  },
  { //J tetromino
    { 1, 0, 0, 0, },
    { 1, 1, 1, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
  },
  { //L tetromino
    { 0, 0, 1, 0, },
    { 1, 1, 1, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
  },
  { //S tetromino
    { 0, 1, 1, 0, },
    { 1, 1, 0, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
  },
  { //Z tetromino
    { 1, 1, 0, 0, },
    { 0, 1, 1, 0, },
    { 0, 0, 0, 0, },
    { 0, 0, 0, 0, },
  },
};

void set_tetromino(tetromino_type_t type, uint8_t palette) {
  uint32_t tx, ty;

  //Store the newly selected tetromino type.
  tetromino_type = type;

  //Initialize the tetromino tile array from the corresponding template.
  for (ty = 0; ty < 4; ty++)
    for (tx = 0; tx < 4; tx++)
      tetromino_tiles[ty][tx] = tetromino_template[type][ty][tx]? palette: 0;
}

void rotate_tetromino_left() {
  uint32_t tx, ty;
  uint8_t tetromino_temp[4][4];

  //Create a temporary copy before rotation.
  for (ty = 0; ty < 4; ty++)
    for (tx = 0; tx < 4; tx++)
      tetromino_temp[ty][tx] = tetromino_tiles[ty][tx];

  if (tetromino_type == TT_I || tetromino_type == TT_O) {
    //If the tetromino type is I or O, rotate the 4x4 matrix, as the size is even.
    for (ty = 0; ty < 4; ty++)
      for (tx = 0; tx < 4; tx++)
        tetromino_tiles[ty][tx] = tetromino_temp[tx][3 - ty];
  }
  else {
    //All other tetrominoes have odd size. Rotate the 3x3 portion of the matrix at upper left.
    for (ty = 0; ty < 3; ty++)
      for (tx = 0; tx < 3; tx++)
        tetromino_tiles[ty][tx] = tetromino_temp[tx][2 - ty];
  }
}

void rotate_tetromino_right() {
  uint32_t tx, ty;
  uint8_t tetromino_temp[4][4];

  //Create a temporary copy before rotation.
  for (ty = 0; ty < 4; ty++)
    for (tx = 0; tx < 4; tx++)
      tetromino_temp[ty][tx] = tetromino_tiles[ty][tx];

  if (tetromino_type == TT_I || tetromino_type == TT_O) {
    //If the tetromino type is I or O, rotate the 4x4 matrix, as the size is even.
    for (ty = 0; ty < 4; ty++)
      for (tx = 0; tx < 4; tx++)
        tetromino_tiles[ty][tx] = tetromino_temp[3 - tx][ty];
  }
  else {
    //All other tetrominoes have odd size. Rotate the 3x3 portion of the matrix at upper left.
    for (ty = 0; ty < 3; ty++)
      for (tx = 0; tx < 3; tx++)
        tetromino_tiles[ty][tx] = tetromino_temp[2 - tx][ty];
  }
}
