//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Header for general configuration.                                                              |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef CONFIG_H_
#define CONFIG_H_

//Button configurations. These map to bit positions inside the input port and depend on the board
//layout (if the board has any buttons).
#if defined(NEXYS2_1200K)
  #define BUTTON_MOVE_LEFT    3
  #define BUTTON_MOVE_RIGHT   1
  #define BUTTON_MOVE_DOWN    2
  #define BUTTON_ROTATE_LEFT  4
  #define BUTTON_ROTATE_RIGHT 0
  #define BUTTON_ACTIVE_LEVEL 1
#else
  //Default button layout for boards without buttons (requires external connections).
  #define BUTTON_MOVE_LEFT    0
  #define BUTTON_MOVE_RIGHT   2
  #define BUTTON_MOVE_DOWN    1
  #define BUTTON_ROTATE_LEFT  6
  #define BUTTON_ROTATE_RIGHT 7
  #define BUTTON_ACTIVE_LEVEL 0
#endif

//Playing field coordinates. These are inside the frame, excluding the area above the dotted line.
#define FIELD_X 15
#define FIELD_Y 7

#endif //CONFIG_H_
