//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Header for the playing field manipulation routines.                                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef PLAYING_FIELD_H_
#define PLAYING_FIELD_H_

#include <stdbool.h>
#include <stdint.h>

//Playing field tile array. If a location is set, there is a tile is in that position.
extern uint8_t field_tiles[20][10];

//Complete line count and array containing complete line positions.
extern uint32_t complete_line_count;
extern uint32_t complete_lines[4];

//Playing field manipulation functions.
void clear_field_tiles();
bool collision(int32_t tetromino_x, int32_t tetromino_y);
bool place_tetromino(int32_t tetromino_x, int32_t tetromino_y);
void move_field_tiles_down();

#endif //PLAYING_FIELD_H_
