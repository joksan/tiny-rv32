//+------------------------------------------------------------------------------------------------+
//| Tetrominoes video game example source.                                                         |
//|                                                                                                |
//| Playing field manipulation routines.                                                           |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdbool.h>
#include <stdint.h>

#include "playing-field.h"
#include "tetromino.h"

//Playing field tile array. If a location is non-zero, there is a tile is in that position. Each
//value encodes the color palette of the tile.
uint8_t field_tiles[20][10];

//Complete line count and array containing complete line positions.
uint32_t complete_line_count;
uint32_t complete_lines[4];

void clear_field_tiles() {
  uint32_t tx, ty;

  //Clear the tile array.
  for (ty = 0; ty < 20; ty ++)
    for (tx = 0; tx < 10; tx++)
      field_tiles[ty][tx] = 0;
}

bool collision(int32_t tetromino_x, int32_t tetromino_y) {
  int32_t tx, ty;

  //Check for collisions against playing field sides.
  for (tx = 0; tx < 4; tx++) {
    //Check for collision against the left side.
    if (tetromino_x + tx < 0)
      for (ty = 0; ty < 4; ty++)
        if (tetromino_tiles[ty][tx] != 0)
          return true;

    //Check for collision against the right side.
    if (tetromino_x + tx >= 10)
      for (ty = 0; ty < 4; ty++)
        if (tetromino_tiles[ty][tx] != 0)
          return true;
  }

  //Check for collission against playing field bottom.
  for (ty = 0; ty < 4; ty++)
    if (tetromino_y + ty >= 20)
      for (tx = 0; tx < 4; tx++)
        if (tetromino_tiles[ty][tx] != 0)
          return true;

  //Check for collision against playing field tiles.
  for (ty = 0; ty < 4; ty++)
    for (tx = 0; tx < 4; tx++)
      if (tetromino_x + tx >= 0 && tetromino_x + tx < 10)
        if (tetromino_y + ty >= 0 && tetromino_y + ty < 20)
          if (field_tiles[tetromino_y + ty][tetromino_x + tx] != 0 && tetromino_tiles[ty][tx] != 0)
            return true;

  return false;
}

bool place_tetromino(int32_t tetromino_x, int32_t tetromino_y) {
  int32_t tx, ty;

  //Iterate over all tetromino tiles, checking their destination placement.
  for (ty = 0; ty < 4; ty++) {
    for (tx = 0; tx < 4; tx++) {
      if (tetromino_y + ty < 0) {
        //Check wether a tile is placed over the top of the playing field.
        if (tetromino_tiles[ty][tx] != 0)
          //Tetromino cannot be placed, as it would fall outside the playing field.
          return false;
      }
      else {
        //Make sure to place tiles inside the field only.
        if (tetromino_x + tx >= 0 && tetromino_x + tx < 10)
          if (tetromino_y + ty >= 0 && tetromino_y + ty < 20)
            //Place a tile if the corresponding coordinate in the tetromino tiles has one.
            if (tetromino_tiles[ty][tx] != 0)
              field_tiles[tetromino_y + ty][tetromino_x + tx] = tetromino_tiles[ty][tx];
      }
    }
  }

  complete_line_count = 0;

  //Iterate over the 4 lines that may be occupied by the placed tetromino.
  for (ty = 3; ty >= 0; ty--) {
    //Make sure this line is inside the field.
    if (tetromino_y + ty >= 0 && tetromino_y + ty < 20) {
      //Check wether all tiles in the line are present.
      for (tx = 0; tx < 10; tx++)
        if (field_tiles[tetromino_y + ty][tx] == 0)
          break;

      //If all tiles are present, add the line to the complete line set.
      if (tx == 10) {
        complete_lines[complete_line_count] = tetromino_y + ty;
        complete_line_count++;
      }
    }
  }

  return true;
}

void move_field_tiles_down() {
  uint32_t tx, ty;
  uint32_t line_counter;

  //Iterate over all completed lines.
  for (line_counter = 0; line_counter < complete_line_count; line_counter++) {
    //Move all tiles one row down, starting from the completed line and going up.
    for (ty = complete_lines[line_counter]; ty > 0; ty--) {
      for (tx = 0; tx < 10; tx++)
        field_tiles[ty][tx] = field_tiles[ty - 1][tx];
    }

    //Clear the top line.
    for (tx = 0; tx < 10; tx++)
      field_tiles[0][tx] = 0;

    //Since all tiles were shifted one row down, increment the positions of the lines that remain
    //unprocessed.
    for (uint32_t i = line_counter + 1; i < complete_line_count; i++)
      complete_lines[i]++;
  }
}
