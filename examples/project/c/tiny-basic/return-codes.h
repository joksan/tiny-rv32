//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Header for codes returned by the parser.                                                       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef RETURN_CODES_H_
#define RETURN_CODES_H_

typedef enum {
  rc_inval_chr,   //Invalid character
  rc_unexp_tok,   //Unexpected token
  rc_unexp_eol,   //Unexpected end of line
  rc_div_zero,    //Division by zero
  rc_no_line,     //Line not found (for GOTO and GOSUB)
  rc_no_mem,      //No more memory (to store lines)
  rc_stk_ovf,     //Stack overflow (for GOSUB)
  rc_ret_wo_gs,   //RETURN without GOSUB (stack underflow)
  rc_inval_stm,   //Invalid statement (for the current state)
  rc_success,     //Parsing successful
} ret_code_t;

#endif //RETURN_CODES_H_
