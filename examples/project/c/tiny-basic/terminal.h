//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Header for terminal handling routines.                                                         |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <stdint.h>
#include <stdbool.h>

//Input handling functions.
uint16_t get_line(char *p_buffer, uint16_t buffer_len);
bool char_arrived();

//Output and formatting functions.
void print_char(char c);
void print_str(const char *s);
void print_str_n(const char *s, unsigned len);
void print_str_ln(const char *s);
void print_uint32(uint32_t val);
void print_uint32_ln(uint32_t val);
void print_int32(int32_t val);
void print_int32_ln(int32_t val);

#endif //TERMINAL_H_
