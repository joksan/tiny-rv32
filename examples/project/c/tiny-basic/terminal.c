//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Terminal handling routines.                                                                    |
//|                                                                                                |
//| This module handles all input and output functions related to user interface. At the moment    |
//| only a serial interface is handled, but it's decoupled with the intention to open up the       |
//| possibility to add other interfaces such as screen and keyboard in the future.                 |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>
#include <stdbool.h>

#include "uart.h"

#include "terminal.h"

//Array containing consecutive powers of 10 from 10^0 to 10^9. The last value is the largest one
//that can fit in a 32-bit integer. Used for conversion to base 10.
static const uint32_t power[] =
  { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };

//Length of previous array.
static const unsigned power_len = sizeof(power) / sizeof(power[0]);

//Next character in buffer. Declared at module level so previous value is remembered.
static char next_char = '\0';

//This function waits for the next character to be received.
//Return value: The new character.
static char get_char() {
  //Wait until a new character arrives through serial.
  while (!UART(0).status_bits.rxready);

  //Read the character and return it.
  return UART(0).data;
}

//This function waits for a complete line to be received and stores it in the provided buffer. It
//appends a terminating zero before returning.
//Parameters:
//- p_buffer: Pointer to holding buffer.
//- buffer_len: The size of the holding buffer.
//Return value: The length of the received line (excluding terminating zero).
uint16_t get_line(char *p_buffer, uint16_t buffer_len) {
  uint16_t pos = 0;   //Buffer position
  char prev_char;     //Previously received character

  //Loop until an exit condition is encountered.
  for (;;) {
    //Take a copy of the previous character, then wait and retrieve the next character.
    prev_char = next_char;
    next_char = get_char();

    //Check whether any end of line character was received (new line or carriage return).
    if (next_char == '\n' || next_char == '\r') {
      //If both the new and the previous characters form a CR/LF pair, drop the LF character (the
      //new line is already handled).
      if (prev_char == '\r' && next_char == '\n')
        continue;

      //Echo the new line with a full CR/LF pair regardless of how it arrived.
      print_str("\r\n");

      //Append a terminating zero and finish.
      p_buffer[pos] = '\0';
      break;
    }
    //Not a end of line. Check for a backspace.
    else if (next_char == '\b') {
      //If characters were input before, echo the backspace and move back the buffer position.
      if (pos > 0) {
        print_char(next_char);
        --pos;
      }
    }
    //Not a backspace. Assume a printable character. Check whether the end of the buffer has been
    //reached (minus 1 so there's always space for a terminating zero).
    else if (pos >= buffer_len - 1) {
      //End reached. Drop any additional characters and don't echo.
      continue;
    }
    else {
      //All good. Echo the character, store it and advance the buffer position.
      print_char(next_char);
      p_buffer[pos++] = next_char;
    }
  }

  return pos;
}

//This function tells whether a new character has arrived, discarding it in the process. Control is
//returned immediately whether a character has arrived or not.
//Return value: True if a character had arrived recently. False otherwise.
bool char_arrived() {
  char prev_char;

  //Check whether a character arrived through serial.
  if (!UART(0).status_bits.rxready)
    return false;

  //Take a copy of the previous character, then retrieve the next character.
  prev_char = next_char;
  next_char = get_char();

  //In the special case that a CR/LF pair arrived return false (drop the LF silently). Return true
  //otherwise.
  return !(prev_char == '\r' && next_char == '\n');
}

//This function prints a single character.
//Parameters:
//- c: The character to be printed.
void print_char(char c) {
  //Wait for the UART to be ready to transmit, then put the character in the buffer.
  while (!UART(0).status_bits.txready);
  UART(0).data = c;
}

//This function prints a zero-terminated string. No line ending is added.
//Parameters:
//- s: The string to be printed.
void print_str(const char *s) {
  //Print individual characters from the string until reaching the terminating zero.
  while(*s != '\0')
    print_char(*s++);
}

//This function prints a length-delimited string. No line ending is added.
//Parameters:
//- s: The string to be printed.
//- len: The string length.
void print_str_n(const char *s, unsigned len) {
  //Print individual characters until no more are left.
  while (len-- > 0)
    print_char(*s++);
}

//This function prints a zero-terminated string. Line ending is added afterwards.
//Parameters:
//- s: The string to be printed.
void print_str_ln(const char *s) {
  //Print the string, then append a CR/LF pair.
  print_str(s);
  print_str("\r\n");
}

//This function prints an unsigned 32-bit integer. No line ending is added.
//Parameters:
//- val: The integer to be printed.
void print_uint32(uint32_t val) {
  signed p = 0;   //Amount of digits at first, digit position or power of 10 afterwards
  uint8_t digit;  //Digit value

  //Find the amount of digits that the number has by comparing it to each power of 10 until finding
  //the largest one that fits in it. Limit the comparison to the length of the power array.
  while (p < power_len - 1 && val >= power[p + 1])
    p++;

  //Iterate for each digit, counting down.
  for (; p >= 0; p--) {
    //Find the current digit by integer division. Repeatedly subtract the current power of 10 from
    //the value while counting.
    for (digit = 0; val >= power[p]; digit++)
      val -= power[p];

    //Convert digit to ASCII and print it.
    print_char(digit + '0');
  }
}

//This function prints an unsigned 32-bit integer. Line ending is added afterwards.
//Parameters:
//- val: The integer to be printed.
void print_uint32_ln(uint32_t val) {
  //Print the integer, then append a CR/LF pair.
  print_uint32(val);
  print_str("\r\n");
}

//This function prints a signed 32-bit integer. No line ending is added.
//Parameters:
//- val: The integer to be printed.
void print_int32(int32_t val) {
  //Check the sign.
  if (val < 0) {
    //Negative number. Simply prepend a minus and then print the number converted to positive.
    print_char('-');
    print_uint32(-val);
  }
  else {
    //Positive number or zero. Print as is.
    print_uint32(val);
  }
}

//This function prints a signed 32-bit integer. Line ending is added afterwards.
//Parameters:
//- val: The integer to be printed.
void print_int32_ln(int32_t val) {
  //Print the integer, then append a CR/LF pair.
  print_int32(val);
  print_str("\r\n");
}
