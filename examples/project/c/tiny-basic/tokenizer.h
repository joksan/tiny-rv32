//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Header for the tokenizer module.                                                               |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef TOKENIZER_H_
#define TOKENIZER_H_

#include <stdint.h>

//Enumeration used to represent all token type codes.
typedef enum {  // Description          Regex
  tt_integer,   // A decimal integer    [0-9]+
  tt_var,       // A variable name      [A-Z]
  tt_string,    // A quoted string      \"[^\"]+\"
  tt_print,     // PRINT instruction    PRINT
  tt_if,        // IF instruction       IF
  tt_then,      // THEN instruction     THEN
  tt_goto,      // GOTO instruction     GOTO
  tt_input,     // INPUT instruction    INPUT
  tt_let,       // LET instruction      LET
  tt_gosub,     // GOSUB instruction    GOSUB
  tt_return,    // RETURN instruction   RETURN
  tt_clear,     // CLEAR instruction    CLEAR
  tt_list,      // LIST instruction     LIST
  tt_run,       // RUN instruction      RUN
  tt_end,       // END instruction      END
  tt_delay,     // DELAY instruction    DELAY
  tt_out,       // OUT instruction      OUT
  tt_par_l,     // ( symbol             \(
  tt_par_r,     // ) symbol             \)
  tt_op_plus,   // + symbol             \+
  tt_op_minus,  // - symbol             -
  tt_op_mul,    // * symbol             \*
  tt_op_div,    // / symbol             /
  tt_eq,        // = symbol             =
  tt_ne,        // <> or >< symbol      <>|><
  tt_le,        // <= symbol            <=
  tt_lt,        // < symbol             <
  tt_ge,        // >= symbol            >=
  tt_gt,        // > symbol             >
  tt_comma,     // , symbol             ,
  tt_eol,       // end of line          \0
  tt_invalid,   // invalid token
} token_type_t;

//Functions for setting and getting the input line of the tokenizer.
void tokenizer_set_line(const char *line);
const char *tokenizer_get_line();

//Main tokenizer task function.
token_type_t tokenizer_get_next();

//Token detail query functions.
uint32_t tokenizer_get_value_int();
const char *tokenizer_get_string();

#endif //TOKENIZER_H_
