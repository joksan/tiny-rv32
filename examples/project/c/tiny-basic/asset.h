//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Header for the BASIC assets manager.                                                           |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef ASSET_H_
#define ASSET_H_

#include <stdint.h>
#include <stdbool.h>

//General management functions.
void asset_init();

//Functions used for managing single-letter variables.
void asset_var_set(uint8_t num, int32_t value);
int32_t asset_var_get(uint8_t num);

//Functions used for managing the stored program.
bool asset_line_set(uint16_t num, const char *p_text, uint16_t len);
const char *asset_line_get(uint16_t num);
const char *asset_line_get_next(uint16_t *p_num);
uint16_t asset_line_get_bytes_free();

//Functions used for managing the call stack.
void asset_callstack_clear();
bool asset_callstack_push(uint16_t line_num);
bool asset_callstack_pop(uint16_t *p_line_num);

#endif //ASSET_H_
