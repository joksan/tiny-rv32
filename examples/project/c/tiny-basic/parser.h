//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Header for the parser module.                                                                  |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef INTERPRETER_H_
#define INTERPRETER_H_

#include "return-codes.h"

//Main parser task function.
ret_code_t parser_parse_line(const char *p_text, uint16_t line_len);

#endif //INTERPRETER_H_
