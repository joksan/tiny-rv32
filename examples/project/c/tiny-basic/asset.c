//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| BASIC assets manager.                                                                          |
//|                                                                                                |
//| This module manages all BASIC assets, that is, all program resources that are exposed to the   |
//| user either explicitly or implicitly, such as single-letter variables, the stored program or   |
//| the call stack.                                                                                |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "asset.h"

//Single-letter-variable array. Stores an integer value for each letter of the alphabet. Note that
//variables are case insensitive so only a single version is stored for each.
static int32_t var[26];

//The code memory, as the name implies, stores the user program in BASIC syntax. An approach that is
//similar to C64's memory scheme is used where lines are stored one after another with insertions
//and deletions causing memory contents to be shifted.

//What is different in this approach though, is that word (32-bit) alignment is carefully kept in
//order not to cause the limited tiny-rv32 CPU core to try to perform unaligned memory accesses,
//which it's not capable of. Also, no tokenization is used (only the line number is parsed during
//input) so the whole line is stored as ASCII text. Finally, line lengths are stored instead of
//pointers to lines that follow. This is because tiny-rv32 has 32-bit pointers, which are larger
//than the 16-bits that are used for length, which saves memory. This also makes it easer to shift
//existing lines (no pointer adjustment needs to be done). The obvious disadvantage is that new
//pointers have to be calculated while traversing the memory, but that's actually pretty cheap in
//this architecture.

//The code memory is handled as an array of characters (bytes) in order to make pointer casting and
//dimensioning a bit easier. Its layout looks like this:

//        |<- word boundary           |<- word boundary           |<- word boundary
//        +-------------+-------------+-------------+-------------+
//        |        Line header        |             |             |
// Line 0 +-------------+-------------+ Stored text |   padding   |
//        | Line length | Line number |             | (alignment) |
//        +-------------+-------------+-------------+-------------+
//        |        Line header        |             |             |
// Line 1 +-------------+-------------+ Stored text |   padding   |
//        | Line length | Line number |             |             |
//        +-------------+-------------+-------------+-------------+
// ...

//Structure used for line headers.
typedef struct {
  uint16_t line_len;  //Line length (this header and stored text)
  uint16_t line_num;  //Line number
} line_header_t;

//Code memory array. The extra memory is for the end-of-program marker so the user gets a clean
//power-of-2 number as the amount of free memory.
static char code_mem[8192 + sizeof(line_header_t)];

//This tracks the amount of code memory in use.
static uint16_t code_mem_bytes_used;

//This macro calculates a word pointer to the first word after all space allocated in code memory.
#define WP_CODE_END ((uint32_t *) (code_mem + code_mem_bytes_used))

//Call stack and current stack position. Line numbers are stored in the stack array. Stack policy is
//post-increment on push and pre-decrement on pop (stack position points to next push position).
static uint16_t call_stack[8];
static unsigned stack_pos;

//This function initializes most assets. It's intended to be called at startup and when executing a
//CLEAR statement. Note that this only clears the single-letter variables and the stored program,
//not the call stack (the call stack is cleared at different moments, particularly between stored
//program runs).
void asset_init() {
  line_header_t *p_lh;

  //Clear all single-letter variables.
  for (int i = 0; i < sizeof(var) / sizeof(var[0]); i++)
    var[i] = 0;

  //Clear the code memory by writing the end-of-program marker at the start.
  p_lh = (line_header_t *) code_mem;                      //Set pointer to start of code memory
  p_lh->line_len = sizeof(line_header_t);                 //Set the line length to just the header
  p_lh->line_num = 0;                                     //Set line number to 0

  //Initialize the code memory usage count (it's just the size of the end marker).
  code_mem_bytes_used = sizeof(line_header_t);
}

//This function sets the value of a single-letter variable.
//Parameters:
//- num: The variable number. 0 = A, 1 = B ... 25 = Z.
//- value: The integer value to assign.
void asset_var_set(uint8_t num, int32_t value) {
  var[num] = value;
}

//This function retrieves the value of a single-letter variable.
//- num: The variable number. 0 = A, 1 = B ... 25 = Z.
//Return value: The assigned integer value.
int32_t asset_var_get(uint8_t num) {
  return var[num];
}

//This function copies the requested line to the code memory at the position indicated by the line
//number. If the line is empty then it's removed from the code memory instead. Code memory is
//automatically shifted to make room for new/extended lines or clean up residual space left by
//deleted/shortened ones.
//Parameters:
//- num: The line number. Cannot be zero.
//- p_text: The text (BASIC code) to be stored.
//- len: The length of the text (not including terminating zero). If zero (empty) erase that line.
//Return value: True if successful. False if code memory full or not enough space.
bool asset_line_set(uint16_t num, const char *p_text, uint16_t len) {
  uint16_t line_len;                                  //Total line length (header + text)
  int16_t shift_amount;                               //Displacement distance in bytes (signed)
  line_header_t *p_lh = (line_header_t *) code_mem;   //Pointer to inserted/deleted/modified line
  uint32_t *wp_src, *wp_dst;                          //Word-aligned source/dst. pointers for shifts
  char *bp_dst;                                       //Byte-aligned dst. pointer for copying text
  uint16_t count;                                     //Counter used for shifting and copying

  //Start by calculating the amount of space that the line needs. If the text is empty, force to
  //zero to cause deletion. Otherwise it's equal to the size of the header plus the text, padded
  //with up to 4 bytes (set to terminating zeroes) to keep it all word-aligned.
  line_len = (len == 0)? 0: (sizeof(line_header_t) + len + 4) & 0xFFFC;

  //Find the position where a new line will be inserted or an existing line will be modified or
  //deleted. Iterate through the code memory until the end marker (line number 0) is reached.
  while (p_lh->line_num != 0) {
    //Check whether the current line number matches or is larger than the requested one.
    if (p_lh->line_num >= num)
      break;
    //Advance the line header pointer to the next one.
    p_lh = (line_header_t *) (((char *) p_lh) + p_lh->line_len);
  }
  //Notes:
  //- In the case where the search reaches the end, any insertion will be performed there.
  //- Attempting to delete a non-existent line will cause this loop to either traverse the entire
  //  code memory or stop at any point along the way. This is intentional as this is the only way to
  //  verify the inexistence of the line. It's not relevant where the search stops as this function
  //  won't perform any shifting nor copying.

  //Calculate the amount of shifting that needs to take place. If the line number exists already
  //it's the size difference from the new line and the existing one. Otherwise it's just the new
  //line size. Note that this calculation works well when deleting lines, even if attempting to
  //delete a non existent one.
  shift_amount = (p_lh->line_num == num)? line_len - p_lh->line_len: line_len;

  //Make sure there's enough space for the operation.
  if (shift_amount > (int16_t) (sizeof(code_mem) - code_mem_bytes_used))
    return false;

  //Perform shifting, if needed.
  if (shift_amount > 0) {
    //Shift existing lines to upper locations. Set up shift variables. Source starts at the end of
    //initially allocated memory. Destination starts at the end of the newly allocated memory. Word
    //count is set to count all words from the insertion point to the end of the initially allocated
    //memory.
    wp_src = WP_CODE_END;
    wp_dst = WP_CODE_END + (shift_amount / sizeof(uint32_t));
    count = WP_CODE_END - (uint32_t *) p_lh;

    //Perform shifting. Do this in descending order.
    //
    //                | <- insertion point                   | <- wp_src
    //+---------------+--------------------+ ~~~ +-----------+
    //| previous line | existing/next line | ... | last line |
    //+---------------+--------------------+ ~~~ +-----------+
    //                | <------------- count --------------> |
    //
    //                                                                   | <- wp_dst
    //+---------------+-----------+--------------------+ ~~~ +-----------+
    //| previous line | new space | existing/next line | ... | last line |
    //+---------------+-----------+--------------------+ ~~~ +-----------+
    while (count > 0) {
      *--wp_dst = *--wp_src;
      count--;
    }
  }
  else if (shift_amount < 0) {
    //Shift existing lines to lower locations. Set up shift variables. Source starts at the end of
    //the residual space. Destination starts at the beginning of the residual space. Word count is
    //set to count all words from the end of the residual space to the end of the initially
    //allocated memory.
    wp_src = ((uint32_t *) p_lh) - (shift_amount / sizeof(uint32_t));
    wp_dst = (uint32_t *) p_lh;
    count = WP_CODE_END - wp_src;

    //Perform shifting. Do this in ascending order.
    //
    //                | <- wp_dst      | <- wp_src
    //+---------------+----------------+--------------------+ ~~~ +-----------+
    //| previous line | residual space | existing/next line | ... | last line |
    //+---------------+----------------+--------------------+ ~~~ +-----------+
    //                                 | <------------- count --------------> |
    //
    //                | <- deletion point
    //+---------------+--------------------+ ~~~ +-----------+
    //| previous line | existing/next line | ... | last line |
    //+---------------+--------------------+ ~~~ +-----------+
    while (count > 0) {
      *wp_dst++ = *wp_src++;
      count--;
    }
  }

  //Store the new line, if needed.
  if (line_len > 0) {
    //Store the new header.
    p_lh->line_len = line_len;
    p_lh->line_num = num;

    //Set up copy variables. Destination starts at stored text location, which is right after the
    //header.
    bp_dst = (char *) (p_lh + 1);
    count = 0;

    //Copy the text. Loop until the whole line (all text including padding) has been written.
    while (count < line_len - sizeof(line_header_t)) {
      //Copy characters from source to destination until the text is depleted. Then pad with zeroes.
      if (count < len)
        *bp_dst++ = *p_text++;
      else
        *bp_dst++ = '\0';
      count++;
    }
  }

  //Adjust the code memory usage count.
  code_mem_bytes_used += shift_amount;

  return true;
}

//This function finds the requested line number in the stored program and returns a pointer to its
//stored text.
//Parameters:
//- num: The line number to find.
//Return value: If the line exists, a pointer to its stored text. NULL otherwise.
const char *asset_line_get(uint16_t num) {
  line_header_t *p_lh = (line_header_t *) code_mem;

  //Iterate through the code memory until the end marker (line number 0) is reached.
  while (p_lh->line_num != 0) {
    //Check whether the current line number matches the requested one.
    if (p_lh->line_num == num)
      //Return the stored text. It follows the line header immediately
      return (const char *) (p_lh + 1);
    //Advance the line header pointer to the next one.
    p_lh = (line_header_t *) (((char *) p_lh) + p_lh->line_len);
  }

  return NULL;
}

//This function finds the line that follows the requested one in the stored program and returns its
//number along with a pointer to its stored text.
//Parameters:
//- p_num: A pointer to an integer with the previous line number. If a line follows (not the end of
//         the stored program), this gets updated to its number.
//Return value: If the line exists, a pointer to its stored text. NULL otherwise.
const char *asset_line_get_next(uint16_t *p_num) {
  line_header_t *p_lh = (line_header_t *) code_mem;

  //Iterate through the code memory until the end marker (line number 0) is reached.
  while (p_lh->line_num != 0) {
    //Check whether the current line number is larger than (follows) the requested one.
    if (p_lh->line_num > *p_num) {
      //Return the current line number and stored text.
      *p_num = p_lh->line_num;
      return (const char *) (p_lh + 1);   //The stored text follows the line header immediately
    }
    //Advance the line header pointer to the next one.
    p_lh = (line_header_t *) (((char *) p_lh) + p_lh->line_len);
  }

  return NULL;
}

//This function tells the amount of program memory that is available.
//Return value: The amount of free memory in bytes.
uint16_t asset_line_get_bytes_free() {
  return sizeof(code_mem) - code_mem_bytes_used;
}

//This function clears the call stack. It's intended to be called between stored program runs.
void asset_callstack_clear() {
  //Simply set the stack position to 0 to clear the stack.
  stack_pos = 0;
}

//This function pushes the requested line number into the call stack. Used for GOSUB statements.
//Parameters:
//- line_num: The line number to be pushed.
//Return value: True if successful. False if stack overflowed.
bool asset_callstack_push(uint16_t line_num) {
  //Check for a full stack.
  if (stack_pos >= sizeof(call_stack) / sizeof(call_stack[0]))
    return false;   //Stack overflow

  //Push the value (post increment stack pointer).
  call_stack[stack_pos++] = line_num;
  return true;
}

//This function pops the last pushed line number from the call stack. Used for RETURN statements.
//Parameters:
//- p_line_num: Pointer to an integer where the popped line number is returned.
//Return value: True if successful. False if stack underflowed.
bool asset_callstack_pop(uint16_t *p_line_num) {
  //Check for an empty stack.
  if (stack_pos == 0)
    return false;   //Stack underflow

  //Pop the value (pre decrement stack pointer).
  *p_line_num = call_stack[--stack_pos];
  return true;
}
