//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Tokenizer module.                                                                              |
//|                                                                                                |
//| This module provides lexical analysis for the BASIC interpreter. A pointer to the line to be   |
//| analyzed is passed to the module to initialize the its internal state, then repeated calls are |
//| performed to get indidividual tokens until the line is over.                                   |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stddef.h>
#include <stdint.h>

#include "tokenizer.h"

//Macro used to identify digit characters.
#define IS_DIGIT(c) ((c) >= '0' && (c) <= '9')

//Macos used to identify uppercase and lowercase characters.
#define IS_LCASE(c) ((c) >= 'a' && (c) <= 'z')
#define IS_UCASE(c) ((c) >= 'A' && (c) <= 'Z')

//Macro used to convert a character to uppercase.
#define TO_UCASE(c) (IS_LCASE(c)? (c) + 'A' - 'a': (c))

//State variables.
static const char *p_input = NULL;    //Pointer to current position in the input line
static const char *p_quoted_string;   //Pointer to last quoted string found
static uint32_t value_int;            //Integer value of last token or quoted string length

//Data type used to represent the token table.
typedef struct {
  token_type_t type;  //Token type code
  const char *text;   //String with the reserved word
} tok_table_t;

//Token table. Contains all reserved words and symbols. Please note that order is important, as this
//is traversed in order when looking for tokens, meaning that longer or more composite reserved
//words or symbols should appear first if they are to be identified correctly (e.g. ">=" has to be
//before ">").
static const tok_table_t tok_table[] = {
  { tt_print,    "PRINT" },
  { tt_if,       "IF" },
  { tt_then,     "THEN" },
  { tt_goto,     "GOTO" },
  { tt_input,    "INPUT" },
  { tt_let,      "LET" },
  { tt_gosub,    "GOSUB" },
  { tt_return,   "RETURN" },
  { tt_clear,    "CLEAR" },
  { tt_list,     "LIST" },
  { tt_run,      "RUN" },
  { tt_end,      "END" },
  { tt_delay,    "DELAY" },
  { tt_out,      "OUT" },
  { tt_par_l,    "(" },
  { tt_par_r,    ")" },
  { tt_op_plus,  "+" },
  { tt_op_minus, "-" },
  { tt_op_mul,   "*" },
  { tt_op_div,   "/" },
  { tt_eq,       "=" },
  { tt_ne,       "<>" },
  { tt_ne,       "><" },
  { tt_le,       "<=" },
  { tt_lt,       "<" },
  { tt_ge,       ">=" },
  { tt_gt,       ">" },
  { tt_comma,    "," },
};

//Length of the token table.
static const int tok_table_len = sizeof(tok_table) / sizeof(tok_table[0]);

//This function tokenizes an integer string from the input line, consuming its characters and
//storing its numeric value. It's assumed that the next character at the input line is a digit (as
//detected by caller) and therefore always succeeds.
//Return value: The type code for an integer token.
static token_type_t tokenize_integer() {
  //The integer value of the last token will be calculated incrementally. Initialize to 0.
  value_int = 0;

  //Iterate until the next character is not a digit anymore.
  do {
    //Take the previous value and multiply it by 10 (shift one decimal digit left), then add the
    //current digit converted from ASCII.
    value_int = value_int * 10 + *p_input - '0';
    p_input++;
  } while (IS_DIGIT(*p_input));

  return tt_integer;
}

//This function tokenizes a quoted string from the input line, consuming its characters and storing
//its position and length. It's assumed that the next character at the input line is an opening
//quote (as detected by caller).
//Return value: The type code for a quoted string token if successful. Otherwise the type code for
//an invalid token.
static token_type_t tokenize_quoted_string() {
  //The integer value of the last token is used to calculate the quoted string length incrementally
  //and to store it afterwards. Set to 0.
  value_int = 0;

  //Store the start of the quoted string (discard the opening quote).
  p_quoted_string = ++p_input;

  //Loop until finding either the closing quote or the end of the line.
  while (*p_input != '"' && *p_input != '\0') {
    value_int++;  //Increase quoted string length
    p_input++;    //Point to next character
  }

  //If a closing quote is found, return the quoted string token code. Otherwise return an invalid
  //token code.
  if (*p_input == '"') {
    p_input++;
    return tt_string;
  }
  else {
    return tt_invalid;
  }
}

//This function checks whether an specific word is found at the current position in the input line.
//Parameters:
//- p_word: Pointer to the matched word.
//Return value: The length of the matched word on match. 0 otherwise.
static int word_match(const char *p_word) {
  int pos;

  //Iterate until reaching the end of the matched word.
  for (pos = 0; p_word[pos] != '\0'; pos++) {
    //Return 0 on first mismatch (case insensitive). End of input also causes a mismatch.
    if (TO_UCASE(p_input[pos]) != p_word[pos])
      return 0;
  }

  //If flow reaches this point the word is found in the input. Return the current position which is
  //also the length of the matched word.
  return pos;
}

//This function is used to prepare the tokenizer to start processing a new line of text.
//Parameters:
//- p_line: Pointer to the line to be tokenized next.
void tokenizer_set_line(const char *p_line) {
  //Simply set the current input position to the provided location.
  p_input = p_line;
}

//This function returns the current position of the line, after discarding any whitespaces that
//could be present. It's intended to be called to store a line in memory or saving the previous
//input line position.
//Return value: Pointer to the current line position.
const char *tokenizer_get_line() {
  //Discard whitespaces.
  while (*p_input == ' ')
    p_input++;

  return p_input;
}

//This function tokenizes the next lexeme in the input, consuming the characters that make it up.
//Depending on the token type, additional information is stored and can be retrieved by using other
//functions.
//Return value: The token type code.
token_type_t tokenizer_get_next() {
  //Discard whitespaces first.
  while (*p_input == ' ')
    p_input++;

  //Check if end of input has been reached.
  if (*p_input == '\0')
    return tt_eol;

  //Attempt to tokenize an integer number.
  if (IS_DIGIT(*p_input))
    return tokenize_integer();

  //Attempt to tokenize a quoted string.
  if (*p_input == '"')
    return tokenize_quoted_string();

  //Not a number or string. Attempt to tokenize any reserved word or symbol.
  for (int t = 0; t < tok_table_len; t++) {
    //Check if the input matches the current word in the token table.
    int len = word_match(tok_table[t].text);

    //Advance the input and return the token type on match.
    if (len > 0) {
      p_input += len;
      return tok_table[t].type;
    }
  }

  //Not a reseved word or symbol. Attempt to tokenize a single-letter variable name.
  if (IS_LCASE(*p_input) || IS_UCASE(*p_input)) {
    value_int = TO_UCASE(*p_input) - 'A';   //Convert the variable name to integer (0 to 25)
    p_input++;
    return tt_var;
  }

  //Not any of the above.
  return tt_invalid;
}

//This function returns the last encountered integer value. If a variable was found instead, a
//number in the range [0, 25] is returned that represents its single-letter name. If a quoted string
//was found instead, its length is returned.
//Return value: The last integer value converted from ascii, the position in the alphabet of the
//last single-letter variable or the length of the last quoted string.
uint32_t tokenizer_get_value_int() {
  return value_int;
}

//This function returns the pointer to the last quoted string that was encountered.
//Return value: Pointer to quoted string inside the input line.
const char *tokenizer_get_string() {
  return p_quoted_string;
}
