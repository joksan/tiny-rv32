//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Parser module.                                                                                 |
//|                                                                                                |
//| This module provides syntactic analysis and execution logic for the BASIC interpreter. A       |
//| single function call for parsing a line of text is all what is needed to run the complete      |
//| interpreter. All behaviors, simple and complex, such as running an stored program or storing   |
//| lines of BASIC code in memory happen as a consequence of parsing and executing lines from the  |
//| input.                                                                                         |
//|                                                                                                |
//| The dialect implemented by this interpreter resembles Tiny BASIC, as documented in wikipedia:  |
//| https://en.wikipedia.org/wiki/Tiny_BASIC                                                       |
//|                                                                                                |
//| The list of supported statements in Backus-Naur form is shown below. Please look at the        |
//| implementation in this source and the tokenizer source for details on the rest of the grammar. |
//|                                                                                                |
//| statement ::= PRINT expr-list                                                                  |
//|               IF expression relop expression THEN statement                                    |
//|               GOTO expression                                                                  |
//|               INPUT var-list                                                                   |
//|               LET var = expression                                                             |
//|               GOSUB expression                                                                 |
//|               RETURN                                                                           |
//|               CLEAR                                                                            |
//|               LIST (number|(number-number)|empty)                                              |
//|               RUN                                                                              |
//|               END                                                                              |
//|               DELAY expression                                                                 |
//|               OUT expression                                                                   |
//|                                                                                                |
//| As can be seen, some additions have been made to this interpreter, such as the DELAY and OUT   |
//| instructions which pause execution for the specified amount of milliseconds and output data    |
//| to the LED port, respectively. Also, the LIST instruction has been extended to accept line     |
//| numbers and ranges in an style that is similar to C64's implementation.                        |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stdbool.h>
#include <stddef.h>

#include "terminal.h"
#include "tokenizer.h"
#include "asset.h"
#include "parser.h"

//State variables.
static token_type_t next_token_type;  //Type code for the token that comes next
static int32_t last_result_int;       //Result of last integer calculation
static bool skip_to_eol;              //Tells whether an IF condition is false and should be skipped
static bool running;                  //Tells whether an stored program is running
static bool jumping;                  //Tells whether to fetch the next program line directly
static uint16_t next_line;            //Line number to be executed next

//Forward declarations.
static ret_code_t parse_expression();
static ret_code_t parse_term();
static ret_code_t parse_factor();

//Define the LED port at its fixed memory address.
#define LED_PORT (*((volatile uint8_t *) GPOP0_BASE_ADDR))

//This macro evaluates an expression as a return code and causes its return value to be propagated
//back to the caller (returned) if this code represents an error condition.
#define PROPAGATE_ON_ERROR(exp) do {\
  int _rc = (exp);\
  if (_rc != rc_success)\
    return _rc;\
} while (0)

//This macro attempts to consume a token of an specified type from the input line. If the token that
//comes next doesn't match, it propagates an unexpected token error.
#define CONSUME_TOKEN_OR_FAIL(tt) do {\
  if (!compare_consume_token(tt))\
    return rc_unexp_tok;\
} while (0)

//This function formats and prints a listing of the stored program using the provided line number
//range. Since valid line numbers range from 1 to 65535 the line number 0 is used to mean that an
//specific range limit is not defined or used. If both start and end line numbers are 0 (invalid)
//then the entire listing is printed. If only the start line number is valid then a single line is
//printed (if it exists). Otherwise a listing covering the specified range is printed. A report of
//available memory is always printed at the end.
//Parameters:
//- start: Start line number (inclusive).
//- end: End line number (inclusive).
static void print_listing(uint16_t start, uint16_t end) {
  uint16_t num = (start == 0)? 0: start - 1;  //Start from previous line number if not zero
  const char *p_text;                         //Pointer to stored BASIC code

  //Loop for as long as valid lines are retrieved. Since the get next line function is used, the
  //line number that immediately precededs the start line number is passed in the first iteration.
  while ((p_text = asset_line_get_next(&num)) != NULL) {
    //If a single line has been requested (only start is valid) and this line number doesn't match,
    //don't print anything.
    if (start > 0 && end == 0 && num != start)
      break;

    //If an end line number has been provided (is valid) and this line number is larger, stop.
    if (end > 0 && num > end)
      break;

    //All good. Print the current line.
    print_uint32(num);
    print_char(' ');
    print_str_ln(p_text);
  }

  //Print the amount of free memory.
  print_uint32(asset_line_get_bytes_free());
  print_str_ln(" bytes free");
}

//This function pauses execution for the specified amount of milliseconds.
//Parameters:
//- ms: Amount of milliseconds.
static void delay(uint32_t ms) {
  //Define the loop counter as volatile to avoid elimination by optimization.
  volatile uint32_t i;

  //Wait for half a second. Period = CPU_CLOCK / cycles_per_loop / output_freq.
  //(cycles_per_loop has been determined by analysis of disassembled code and simulation).
  for (i = 0; i < CPU_CLOCK/24/1000*ms; i++);
}

//This function compares the specified token type code with the one that comes next and consumes it,
//if it matches. By "consuming" we mean that the next token type is requested from the tokenizer,
//replacing the previous value.
//Parameters:
//- type: The specified token type code.
//Return value: True on match and token consumed. False otherwise.
static bool compare_consume_token(token_type_t type) {
  if (type == next_token_type) {
    next_token_type = tokenizer_get_next();
    return true;
  }
  else {
    return false;
  }
}

//This function parses and executes a BASIC statement. Look at the source header for the full BNF.
//Return value: The return code.
static ret_code_t parse_statement() {
  //Check for a PRINT statement.
  if (compare_consume_token(tt_print)) {
    //BNF:  statement ::= PRINT expr-list
    //      expr-list ::= (string|expression) (, (string|expression) )*

    //Loop for as long as strings or expressions separated by commas are found.
    do {
      if (next_token_type == tt_string) {
        print_str_n(tokenizer_get_string(), tokenizer_get_value_int());   //Print the string
        CONSUME_TOKEN_OR_FAIL(tt_string);                                 //Consume the string token
      }
      else {
        PROPAGATE_ON_ERROR(parse_expression());                           //Parse next expression
        print_int32(last_result_int);                                     //Print the result
      }
    }
    while (compare_consume_token(tt_comma));

    print_str_ln("");                                                     //Print an end of line
  }
  //Check for an IF statement.
  else if (compare_consume_token(tt_if)) {
    //BNF:  statement ::= IF expression relop expression THEN statement
    //      relop ::= < (>|=|empty) | > (<|=|empty) | =

    PROPAGATE_ON_ERROR(parse_expression());       //Parse the expression before relative operator
    int left_hand_val = last_result_int;          //Store the result (left hand side of relop)
    token_type_t relop_token = next_token_type;   //Store the relop token for later

    //Make sure the relative operator token is valid.
    if (relop_token != tt_eq && relop_token != tt_lt && relop_token != tt_le &&
        relop_token != tt_gt && relop_token != tt_ge && relop_token != tt_ne)
      return rc_unexp_tok;

    CONSUME_TOKEN_OR_FAIL(relop_token);           //Consume the relop token unconditionally now
    PROPAGATE_ON_ERROR(parse_expression());       //Parse the expression after relative operator
    CONSUME_TOKEN_OR_FAIL(tt_then);               //Consume a THEN token or fail on mismatch

    //Evaluate the condition based on the relop token.
    switch (relop_token) {
      case tt_eq:   //Equal condition
        //Parse next statement if condition is true. Skip to end of line otherwise.
        if (left_hand_val == last_result_int) PROPAGATE_ON_ERROR(parse_statement());
        else skip_to_eol = true;
        break;
      case tt_lt:   //Less than condition
        if (left_hand_val < last_result_int) PROPAGATE_ON_ERROR(parse_statement());
        else skip_to_eol = true;
        break;
      case tt_le:   //Less than or equal condition
        if (left_hand_val <= last_result_int) PROPAGATE_ON_ERROR(parse_statement());
        else skip_to_eol = true;
        break;
      case tt_gt:   //Greater than condition
        if (left_hand_val > last_result_int) PROPAGATE_ON_ERROR(parse_statement());
        else skip_to_eol = true;
        break;
      case tt_ge:   //Greater than or equal condition
        if (left_hand_val >= last_result_int) PROPAGATE_ON_ERROR(parse_statement());
        else skip_to_eol = true;
        break;
      default:      //Not equal condition
        if (left_hand_val != last_result_int) PROPAGATE_ON_ERROR(parse_statement());
        else skip_to_eol = true;
        break;
    }
  }
  //Check for a GOTO statement.
  else if (compare_consume_token(tt_goto)) {
    //BNF:  statement ::= GOTO expression

    PROPAGATE_ON_ERROR(parse_expression());   //Parse next expression
    next_line = last_result_int;              //Set the next line number to the result
    running = true;                           //If not running the stored program, do so from now on
    jumping = true;                           //Set the jump flag so the exact line is fetched next
  }
  //Check for an INPUT statement.
  else if (compare_consume_token(tt_input)) {
    //BNF:  statement ::= INPUT var-list
    //      var-list ::= var (, var)*

    //Loop for as long as variables separated by commas are found.
    do {
      //Make sure the next token is a variable.
      if (next_token_type != tt_var)
        return rc_unexp_tok;

      int var = tokenizer_get_value_int();        //Look up a variable number
      const char *p_line = tokenizer_get_line();  //Store the current line position to resume later
      char tmp_buffer[16];                        //Temporary input buffer

      print_str("? ");                            //Show the variable input prompt
      get_line(tmp_buffer, sizeof(tmp_buffer));   //Capture next user input to temporary buffer

      tokenizer_set_line(tmp_buffer);             //Switch the tokenizer to the temporary buffer
      next_token_type = tokenizer_get_next();     //Get the first token from temporary buffer
      PROPAGATE_ON_ERROR(parse_expression());     //Parse temporary buffer as expression

      asset_var_set(var, last_result_int);        //Store the result to the variable

      tokenizer_set_line(p_line);                 //Resume from current line position
      next_token_type = tokenizer_get_next();     //Get the next token
    } while (compare_consume_token(tt_comma));
  }
  //Check for a LET statement.
  else if (compare_consume_token(tt_let)) {
    //BNF:  statement ::= LET var = expression

    int var = tokenizer_get_value_int();      //Look up a variable number ahead of time
    CONSUME_TOKEN_OR_FAIL(tt_var);            //Consume a variable token or fail on mismatch
    CONSUME_TOKEN_OR_FAIL(tt_eq);             //Consume an '=' token or fail on mismatch
    PROPAGATE_ON_ERROR(parse_expression());   //Parse next expression
    asset_var_set(var, last_result_int);      //Store the result to the variable
  }
  //Check for a GOSUB statement.
  else if (compare_consume_token(tt_gosub)) {
    //BNF:  statement ::= GOSUB expression

    PROPAGATE_ON_ERROR(parse_expression());   //Parse next expression

    //If the stored program is not running (program started via GOSUB) set the current line to 0 so
    //the program ends upon RETURN.
    if (!running)
      next_line = 0;

    //Push the current line number as the return address.
    if (!asset_callstack_push(next_line))
      return rc_stk_ovf;

    next_line = last_result_int;  //Set the next line number to the result
    running = true;               //If not running the stored program, do so from now on
    jumping = true;               //Set the jump flag so the exact line is fetched next
  }
  //Check for a RETURN statement.
  else if (compare_consume_token(tt_return)) {
    //BNF:  statement ::= RETURN

    //Pop the stored line number.
    if (!asset_callstack_pop(&next_line))
      return rc_ret_wo_gs;

    //If returning to line 0 end the program (program was started with GOSUB).
    if (next_line == 0)
      running = false;
  }
  //Check for a CLEAR statement.
  else if (compare_consume_token(tt_clear)) {
    //BNF:  statement ::= CLEAR

    //CLEAR is not allowed while running (program would erase itself).
    if (running)
      return rc_inval_stm;

    asset_init();   //Reinitialize assets (clear stored program and single-letter variables)
  }
  //Check for a LIST statement.
  else if (compare_consume_token(tt_list)) {
    //BNF:  statement ::= LIST (number|(number-number)|empty)

    uint16_t start = 0;   //Start and end line numbers default to 0
    uint16_t end = 0;

    //Check whether an integer comes next.
    if (next_token_type == tt_integer) {
      start = tokenizer_get_value_int();    //Integer is next, take as start line

      //Line number 0 is invalid.
      if (start == 0)
        return rc_unexp_tok;

      CONSUME_TOKEN_OR_FAIL(tt_integer);    //Consume the integer token

      //Check for '-' token, consume on match.
      if (compare_consume_token(tt_op_minus)) {
        end = tokenizer_get_value_int();    //Look up a line number ahead of time, take as end line

        //Again, line number 0 is invalid.
        if (end == 0)
          return rc_unexp_tok;

        CONSUME_TOKEN_OR_FAIL(tt_integer);  //Consume the integer token
      }
    }

    print_listing(start, end);              //Print the listing using provided/default line numbers
  }
  //Check for a RUN statement.
  else if (compare_consume_token(tt_run)) {
    //BNF:  statement ::= RUN

    //RUN is not allowed while running.
    if (running)
      return rc_inval_stm;

    running = true;   //Run the stored program from now on
  }
  //Check for a END statement.
  else if (compare_consume_token(tt_end)) {
    //BNF:  statement ::= END

    //END is not allowed while not running.
    if (!running)
      return rc_inval_stm;

    running = false;  //Stop running the program
  }
  //Check for a DELAY statement.
  else if (compare_consume_token(tt_delay)) {
    //BNF:  statement ::= DELAY expression

    PROPAGATE_ON_ERROR(parse_expression());   //Parse next expression
    delay(last_result_int);                   //Pause for the resulting amount of milliseconds
  }
  //Assume an OUT statement otherwise.
  else {
    CONSUME_TOKEN_OR_FAIL(tt_out);            //Consume an OUT token or fail on mismatch
    PROPAGATE_ON_ERROR(parse_expression());   //Parse next expression
    LED_PORT = last_result_int;               //Output result to LED port
  }

  return rc_success;
}

//This function parses and executes a BASIC expression. The result of the calculation is returned
//through a global variable.
//Return value: The return code.
static ret_code_t parse_expression() {
  //BNF:  expression ::= (+|-|empty) term ((+|-) term)*

  int32_t accum;                                        //Accumulated value

  //Check for any preceding '+' or '-' tokens.
  if (compare_consume_token(tt_op_plus)) {              //Check for '+' token, consume on match
    PROPAGATE_ON_ERROR(parse_term());                   //Parse next higher priority operand
    accum = last_result_int;                            //Save result in accumulator
  }
  else if (compare_consume_token(tt_op_minus)) {        //Check for '-' token, consume on match
    PROPAGATE_ON_ERROR(parse_term());                   //Parse next higher priority operand
    accum = -last_result_int;                           //Save negative of result in accumulator
  }
  else {
    PROPAGATE_ON_ERROR(parse_term());                   //Parse next higher priority operand
    accum = last_result_int;                            //Save result in accumulator
  }

  //Loop for as long as consecutve '+' and/or '-' tokens that follow previous terms are found.
  //Operate and accumulate the result (looping this way causes left-associativity).
  while (next_token_type == tt_op_plus || next_token_type == tt_op_minus) {
    if (compare_consume_token(tt_op_plus)) {            //Check for '+' token, consume on match
      PROPAGATE_ON_ERROR(parse_term());                 //Parse next higher priority operand
      accum += last_result_int;                         //Operate and accumulate
    }
    else if (compare_consume_token(tt_op_minus)) {      //Check for '-' token, consume on match
      PROPAGATE_ON_ERROR(parse_term());                 //Parse next higher priority operand
      accum -= last_result_int;                         //Operate and accumulate
    }
  }

  last_result_int = accum;                              //Return accumulated value
  return rc_success;
}

//This function parses and executes a BASIC term. The result of the calculation is returned through
//a global variable.
//Return value: The return code.
static ret_code_t parse_term() {
  //BNF:  term ::= factor ((*|/) factor)*

  int32_t accum;                                      //Accumulated value

  PROPAGATE_ON_ERROR(parse_factor());                 //Parse next higher priority operand
  accum = last_result_int;                            //Save result in accumulator

  //Loop for as long as consecutve '*' and/or '*' tokens that follow previous terms are found.
  //Operate and accumulate the result (looping this way causes left-associativity).
  while (next_token_type == tt_op_mul || next_token_type == tt_op_div) {
    if (compare_consume_token(tt_op_mul)) {           //Check for '*' token, consume on match
      PROPAGATE_ON_ERROR(parse_factor());             //Parse next higher priority operand
      accum *= last_result_int;                       //Operate and accumulate
    }
    else if (compare_consume_token(tt_op_div)) {      //Check for '/' token, consume on match
      PROPAGATE_ON_ERROR(parse_factor());             //Parse next higher priority operand
      if (last_result_int == 0)                       //Check for division by zero
        return rc_div_zero;
      accum /= last_result_int;                       //Operate and accumulate
    }
  }

  last_result_int = accum;                            //Return accumulated value
  return rc_success;
}

//This function parses and executes a BASIC factor. The result of the calculation is returned
//through a global variable.
//Return value: The return code.
static ret_code_t parse_factor() {
  //BNF:  factor ::= var | number | (expression)

  last_result_int = tokenizer_get_value_int();          //Look up an integer value ahead of time
  if (compare_consume_token(tt_var)) {                  //Check for variable token, consume on match
    last_result_int = asset_var_get(last_result_int);   //Return the value of the variable
  }
  else if (compare_consume_token(tt_integer)) {         //Check for integer token, consume on match
    //Nothing else to be done. Return value is already set.
  }
  else {
    CONSUME_TOKEN_OR_FAIL(tt_par_l);                    //Consume '(' token or fail on mismatch
    PROPAGATE_ON_ERROR(parse_expression());             //Parse parethesized expression
    CONSUME_TOKEN_OR_FAIL(tt_par_r);                    //Consume ')' token or fail on mismatch
  }

  return rc_success;
}

//This function parses and executes a BASIC line, which is the top-level hierarchy in the
//implemented grammar. When called, this function causes most the functionality of the BASIC
//interpreter to be executed.
//Return value: The return code.
ret_code_t parser_parse_line(const char *p_text, uint16_t line_len) {
  //BNF:  line ::= number statement CR | statement CR

  ret_code_t rc;            //Return code

  //Initialize the execution environment.
  running = false;          //Don't run by default
  jumping = false;          //Execute next line (no direct jump)
  next_line = 0;            //Set initial line to 0 (will execute lowest line number)
  asset_callstack_clear();  //Clear the call stack

  //Loop until an stop condition is met.
  for (;;) {
    //Initialize the tokenizer and get the first token to prepare for parsing.
    tokenizer_set_line(p_text);
    next_token_type = tokenizer_get_next();

    //Check whether the line is preceeded by a number. If so, this line will be stored instead of
    //executed.
    if (next_token_type == tt_integer) {
      //Make sure a program is not running. Otherwise treat this is an error (self-modyfing code is
      //disallowed by design and unsupported).
      if (running)
        return rc_unexp_tok;

      uint32_t line_num = tokenizer_get_value_int();  //Look up the next integer (line number)

      //Line number 0 is invalid.
      if (line_num == 0)
        return rc_unexp_tok;

      const char *p_line = tokenizer_get_line();      //Get the current line position

      //Adjust the line size to remove the characters consumed up to this point.
      line_len -= p_line - p_text;

      //Store the line in memory.
      return asset_line_set(line_num, p_line, line_len)? rc_success: rc_no_mem;
    }

    skip_to_eol = false;      //Clear the skip flag before parsing a new statement
    rc = parse_statement();   //Parse the statement (continue at the next broadest hierarchy level)

    //If an unexpected token was found and it happens to be the end of the line, override the error
    //code and propagate it.
    if (rc == rc_unexp_tok && next_token_type == tt_eol)
      return rc_unexp_eol;

    //Propagate any other error code.
    if (rc != rc_success)
      return rc;

    //If the statement was parsed successfully but the end of the line wasn't reached (assuming no
    //skip to end of line), promote this situation to an error.
    if (!skip_to_eol && next_token_type != tt_eol)
      return rc_unexp_tok;

    //Stop running the program if the running flag is clear.
    if (!running)
      return rc_success;

    //The program will continue running. Terminate execution if a character arrives.
    if (char_arrived())
      return rc_success;

    //Fetch the next line.
    if (jumping) {
      //If jumping, clear the jump flag and get the line directly.
      jumping = false;
      p_text = asset_line_get(next_line);
      if (!p_text)
        return rc_no_line;  //Invalid jump
    }
    else {
      //If not jumping, get the line that follows the current one.
      p_text = asset_line_get_next(&next_line);
      if (!p_text) {
        running = false;    //End of program
        return rc_success;
      }
    }
  }
}
