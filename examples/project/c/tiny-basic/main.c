//+------------------------------------------------------------------------------------------------+
//| Tiny basic demonstration program for the tiny-rv32e softcore processor.                        |
//|                                                                                                |
//| Main module.                                                                                   |
//|                                                                                                |
//| This module provides the entry point and manages the program loop. It also provides the main   |
//| input buffer and handles error messages. The BASIC interpreter is run by invoking the line     |
//| parsing function.                                                                              |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#include <stddef.h>

#include "asset.h"
#include "terminal.h"
#include "parser.h"
#include "return-codes.h"

//Buffer used for storing program lines from the terminal (e.g. serial port).
static char line_buffer[32];

//Strings for all error conditions. These match the ones defined for the ret_code_t type definition.
const char *err_name[] = {
  "inval chr",  //rc_inval_chr - Invalid character
  "unexp tok",  //rc_unexp_tok - Unexpected token
  "unexp eol",  //rc_unexp_eol - Unexpected end of line
  "div zero",   //rc_div_zero  - Division by zero
  "no line",    //rc_no_line   - Line not found (for GOTO and GOSUB)
  "no mem",     //rc_no_mem    - No more memory (to store lines)
  "stk ovf",    //rc_stk_ovf   - Stack overflow (for GOSUB)
  "ret wo gs",  //rc_ret_wo_gs - RETURN without GOSUB (stack underflow)
  "inval stm",  //rc_inval_stm - Invalid statement (for the current state)
};

int main () {
  uint16_t line_len;  //Length of the current line, as returned by the terminal
  ret_code_t rc;      //Return code from the line parser

  //Initialize all BASIC assets.
  asset_init();

  print_str_ln("Tiny basic interpreter");

  //Main interpreter loop.
  for (;;) {
    //Show the prompt. Capture next line.
    print_str("> ");
    line_len = get_line(line_buffer, sizeof(line_buffer));

    //Ignore empty lines (user just presses enter). Useful when starting a terminal session to let
    //the user know that the microcontroller is expecting a new command.
    if (!line_len)
      continue;

    //Parse the captured line. This executes the main logic for the BASIC interpreter. Control is
    //returned after executing a single statement, the program ends by itself (RUN statement), an
    //error condition is encountered or the user stops the program.
    rc = parser_parse_line(line_buffer, line_len);

    //Print an error message if parsing/execution failed.
    if (rc != rc_success) {
      print_str("Err: ");
      print_str_ln(err_name[rc]);
    }
  }
}
