//+------------------------------------------------------------------------------------------------+
//| Memory mapped, dual input/ouput SPI, FLASH memory controller peripheral for the tiny-rv32e     |
//| softcore processor.                                                                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module mm_dspi_flash #(
  parameter clk_freq = 0,         //Operating clock frequency
  parameter mem_size = 0,         //Memory size in bytes
  parameter dummy_cycles = 8,     //Amount of cycles to wait between address and data phases
  parameter wake_up_time_us = 0   //Time to wait after resuming from power down (disabled if 0)
)
(
  //System signals.
  input clk,
  input reset,
  output reg hold,

  //Processor bus signals.
  input rd_en,
  input [31:2] addr,
  output reg [31:0] rd_data,

  //Peripheral I/O signals.
  output sck,
  inout miso,
  inout mosi,
  output reg ss
);
  //Work around an XST bug that disallows system functions being used with localparam.
  `ifdef XST
    `define localparam parameter
  `else
    `define localparam localparam
  `endif

  //Local parameters.
  localparam [7:0] read_cmd = 8'hBB;                    //Dual I/O fast read command
  localparam [7:0] wake_up_cmd = 8'hAB;                 //Resume from deep power down command
  `localparam mem_addr_bits = $clog2(mem_size);         //Amount of bits used for memory access
  localparam spi_addr_bytes = (mem_addr_bits + 7) / 8;  //Amount of address bytes sent over SPI
  localparam spi_addr_bits = spi_addr_bytes * 8;        //Same as above but in bits
  //Amount of cycles to wait for the memory to wake up (if zero then no wake up logic is generated).
  `localparam wake_up_cycles = $rtoi($ceil(clk_freq / 1000000.0 * wake_up_time_us));
  //Amount of bits for the cycle count. A minimum of 4 are required for data operations, but more
  //may be needed for waiting the device to wake up.
  `localparam count_bits = $clog2(wake_up_cycles) > 4? $clog2(wake_up_cycles): 4;

  //FSM state definitions. The following states can be transitioned forcefully into the deselect
  //state in order to start another transaction if the CPU reads a memory address that doesn't match
  //the current one.
  localparam s_deselect   = 4'd0;   //Slave is deselected (reset SPI transaction)
  localparam s_read_cmd   = 4'd1;   //Read command phase
  localparam s_address    = 4'd2;   //Address phase
  localparam s_dummy      = 4'd3;   //Dummy cycles phase
  localparam s_data       = 4'd4;   //Data phase
  localparam s_wait_read  = 4'd5;   //Wait for CPU read
  //The following states cannot be transitioned forcefully - they have to be waited until finished
  //because the device is busy. Note that they all have the MSB set, so they can be told apart.
  localparam s_desel_wake = 4'd8;   //Slave is deselected before wake up (reset SPI transaction)
  localparam s_wake_cmd   = 4'd9;   //Wake up command phase
  localparam s_wake_wait  = 4'd10;  //Wait after wake up phase

  reg [3:0] state;                    //Current FSM state
  reg sck_en;                         //When set, the system clock signal is output on SCK
  reg miso_oe;                        //Output enable for MISO/D1 port
  reg mosi_oe;                        //Output enable for MOSI/D0 port
  reg [mem_addr_bits-1:2] mem_addr;   //The currently known external memory address (minus 2 LSB)
  wire [spi_addr_bits-1:0] spi_addr;  //A byte-aligned alias for the previous address
  reg [31:0] shift_reg;               //The main shift register (shifts 2 at a time)
  reg [count_bits-1:0] cycle_count;   //The current cycle count

  //Make sure that the input clock frequency is configured correctly.
  generate
    if (wake_up_time_us > 0 && clk_freq == 0)
      invalid_parameter_value clk_freq();
  endgenerate

  //Make sure that the memory size is configured correctly.
  generate
    if (mem_size == 0)
      invalid_parameter_value mem_size();
  endgenerate

  //MISO/D1 and MOSI/D0 reflect the most significant bits of the shift register if their respective
  //output is enabled. Otherwise they're left in high impedance state.
  assign miso = miso_oe? shift_reg[31]: 1'bz;
  assign mosi = mosi_oe? shift_reg[30]: 1'bz;

  //SCK generates an inverted signal from the system clock for as long as it's enabled (SPI mode 0).
  assign sck = !clk && sck_en;

  //The byte-aligned address is used for transmission over the SPI bus.
  assign spi_addr = { { spi_addr_bits - mem_addr_bits { 1'b0 } }, mem_addr, 2'd0 };

  //Main logic block.
  always @(posedge clk) begin
    if (reset) begin
      state <= wake_up_cycles? s_desel_wake: s_deselect;
      sck_en <= 1'b0;
      miso_oe <= 1'b0;
      mosi_oe <= 1'b0;
      mem_addr <= { mem_addr_bits { 1'b0 } };
      shift_reg <= 32'd0;
      cycle_count <= { count_bits { 1'b0 } };
      hold <= 1'b0;
      rd_data <= 32'd0;
      ss <= 1'b1;
    end
    else begin
      //Default value for signals that are active for a single clock cycle (overriden below).
      rd_data <= 32'd0;

      //Main FSM.
      case (state)
        s_deselect: begin
          //SPI connection is reset during this state. Prepare the signals for the next one.
          sck_en <= 1'b1;   //Start generating SCK
          mosi_oe <= 1'b1;  //Enable MOSI/D0 to send the command
          ss <= 1'b0;       //Select the flash memory

          //Prepare the shift register with the read command. Since only MOSI/D0 is used and only 8
          //bits are transferred, only the most significant even bits are set (the rest don't
          //matter, set to X to allow for synthesis optimization).
          shift_reg <= { 1'bX, read_cmd[7], 1'bX, read_cmd[6], 1'bX, read_cmd[5], 1'bX, read_cmd[4],
                         1'bX, read_cmd[3], 1'bX, read_cmd[2], 1'bX, read_cmd[1], 1'bX, read_cmd[0],
                         16'hXXXX };
          cycle_count <= 7;
          state <= s_read_cmd;
        end
        s_read_cmd: begin
          //The read command is sent during this state. Shift the data by 2 bits at a time.
          shift_reg <= { shift_reg[29:0], miso, mosi };
          cycle_count <= cycle_count - 1'd1;

          //Check for when the last command bit is transferred.
          if (cycle_count == 0) begin
            //Prepare the signals for the next state.
            miso_oe <= 1'b1;  //Enable MISO/D1 to send the address along with MOSI/D0

            //Prepare the shift register with the address. Note that the shift register is larger so
            //it's also stuffed.
            shift_reg <= { spi_addr, { 32 - spi_addr_bits { 1'bX } } };
            cycle_count <= (spi_addr_bytes * 4) - 1;
            state <= s_address;
          end
        end
        s_address: begin
          //The address is sent during this state. Shift the data by 2 bits at a time.
          shift_reg <= { shift_reg[29:0], miso, mosi };
          cycle_count <= cycle_count - 1'd1;

          //Check for when the last address bits are transferred.
          if (cycle_count == 0) begin
            //Prepare the signals for the next state.
            miso_oe <= 1'b0;      //Done transmitting - disable MISO/D1 and MOSI/D0
            mosi_oe <= 1'b0;
            cycle_count <= dummy_cycles - 1;
            state <= s_dummy;
          end
        end
        s_dummy: begin
          //Dummy cycles are inserted in this state to allow for access time and data direction
          //change. Shift the data the same as in previous states just to reduce the logic overhead
          //on the shift register.
          shift_reg <= { shift_reg[29:0], miso, mosi };
          cycle_count <= cycle_count - 1'd1;

          //Check for when the last dummy cycle has elapsed.
          if (cycle_count == 0) begin
            //Prepare the signals for the next state.
            cycle_count <= 15;
            state <= s_data;
          end
        end
        s_data: begin
          //The data is received during this state. Shift the data by 2 bits at a time.
          shift_reg <= { shift_reg[29:0], miso, mosi };
          cycle_count <= cycle_count - 1'd1;

          //Check for when the last data bits are transferred.
          if (cycle_count == 0) begin
            //Prepare the signals for the next state.
            sck_en <= 1'b0;         //Stop generating SCK
            state <= s_wait_read;
          end
        end
        s_wait_read: begin
          //A CPU read is awaited during this state (this is effectively the idle state). Check
          //whether a new read or a pending read (as indicated by hold) happens.
          if (rd_en || hold) begin
            //A read occurred. Pass the data to the CPU and prepare the signals for the next state.
            //By transitioning to the data phase again the next set of data will be speculatively
            //read in advance.
            sck_en <= 1'b1;               //Start generating SCK again
            rd_data <= { shift_reg[7:0], shift_reg[15:8], shift_reg[23:16], shift_reg[31:24] };
            mem_addr <= mem_addr + 1'd1;  //Increase the currently known external memory address
            cycle_count <= 15;
            state <= s_data;
            //Notes:
            //- In case of an address mismatch during this read, the speculative data is still
            //  placed on the bus, but since the hold signal is asserted below, it should be ignored
            //  by the CPU.
            //- Data bytes are reordered for the correct endianness, as lower addresses come in
            //  first and end up on the most significant byte.
          end
        end
        s_desel_wake: begin
          //Only generate logic for this state if it's actually used.
          if (wake_up_cycles) begin
            //SPI connection is reset during this state. Prepare the signals for the next one.
            sck_en <= 1'b1;   //Start generating SCK
            mosi_oe <= 1'b1;  //Enable MOSI/D0 to send the command
            ss <= 1'b0;       //Select the flash memory

            //Prepare the shift register with the wake up command. Only MOSI/D0 is used.
            shift_reg <= { 1'bX, wake_up_cmd[7], 1'bX, wake_up_cmd[6],
                           1'bX, wake_up_cmd[5], 1'bX, wake_up_cmd[4],
                           1'bX, wake_up_cmd[3], 1'bX, wake_up_cmd[2],
                           1'bX, wake_up_cmd[1], 1'bX, wake_up_cmd[0],
                           16'hXXXX };
            cycle_count <= 7;
            state <= s_wake_cmd;
          end
        end
        s_wake_cmd: begin
          //Only generate logic for this state if it's actually used.
          if (wake_up_cycles) begin
            //The wake up command is sent during this state. Shift the data by 2 bits at a time.
            shift_reg <= { shift_reg[29:0], miso, mosi };
            cycle_count <= cycle_count - 1'd1;

            //Check for when the last command bit is transferred.
            if (cycle_count == 0) begin
              //Prepare the signals for the next state.
              sck_en <= 1'b0;
              mosi_oe <= 1'b0;
              ss <= 1'b1;
              cycle_count <= wake_up_cycles - 1;
              state <= s_wake_wait;
            end
          end
        end
        s_wake_wait: begin
          //Only generate logic for this state if it's actually used.
          if (wake_up_cycles) begin
            //The internal wake up process is waited during this state. Do nothing but count cycles.
            cycle_count <= cycle_count - 1'd1;

            //Check for when the last wait cycle has elapsed.
            if (cycle_count == 0) begin
              //Simply move on to the deselect state (SPI bus is inactive already).
              state <= s_deselect;
            end
          end
        end
      endcase

      //Check if a read for an address that doesn't match the current memory address happens.
      if (rd_en && mem_addr != addr[mem_addr_bits-1:2]) begin
        //Address mismatch. Store the new requested address.
        mem_addr <= addr[mem_addr_bits-1:2];

        //If the current state can be cancelled (MSB of the state code is clear) then perform a
        //forceful transition into the deselect state, aborting any ongoing transaction. Override
        //all relevant registers to ensure a clean transition.
        if (!state[3]) begin
          sck_en <= 1'b0;                       //Stop generating SCK
          miso_oe <= 1'b0;                      //Force MISO/D1 and MOSI/D0 off
          mosi_oe <= 1'b0;
          ss <= 1'b1;                           //Deselect the flash memory
          state <= s_deselect;
        end
      end

      //Hold is asserted on the next cycle whenever the CPU attempts to read this peripheral and
      //either there's an address mismatch (new data needs to be retrieved) or the controller is
      //busy. Hold is always deasserted upon reaching the idle state.
      if (rd_en && (mem_addr != addr[mem_addr_bits-1:2] || state != s_wait_read))
        hold <= 1'b1;
      else if (state == s_wait_read)
        hold <= 1'b0;
    end
  end
endmodule
