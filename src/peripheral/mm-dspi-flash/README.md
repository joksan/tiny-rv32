Memory mapped, dual input/ouput SPI, FLASH memory controller peripheral for the tiny-rv32e softcore
processor.
---------------------------------------------------------------------------------------------------

This peripheral allows to access FLASH memories commonly found in many FPGA development boards in a
transparent fashion, as memory-mapped devices. It's pretty common to see such devices connected
using classic (4 wire) SPI to the FPGA in order to allow to load bitstreams at power up, but often
an amount of unusued space is left in them which can be used for other purposes.

Using the interface as classic SPI makes it a bit slow, so in order to gain a bit more speed without
connecting additional wires, dual I/O SPI mode is used, which pretty much cuts the access time in
half. This mode is very commonly available in most recent devices.

This controller has some very interesting use cases such as storing read-only application data, or
executing code in place (XIP), which allows for programs and/or data larger than the BRAM memory
available in many cases.

The device is mapped as a whole in CPU memory starting from device address 0, meaning that even the
stored bitstream can be read out, if desired, but note that this controller only returns data 32
bits at a time, due to the peculiarities of the tiny-rv32e softcore CPU. This doesn't mean that data
can't be accessed in smaller units (the CPU already takes care of that automatically), only that it
may be a bit inneficient to access data in smaller units.

Also, by design, the controller greatly favors sequential access, as is the nature of SPI memories
to return subsequent data if the clock signal is left on. This means that the controller will read
the next 32 bits of data in advance on its own, speculatively, even if it's not requested. This is
in order to lower the access time of the next read, just in case the CPU turns out to request the
following address next. This comes at negligible penalty, as the controller is intelligent enough to
suspend any speculative operation and start a new one right away at the next clock cycle.
