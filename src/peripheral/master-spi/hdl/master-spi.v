//+------------------------------------------------------------------------------------------------+
//| Master SPI peripheral for the tiny-rv32e softcore processor.                                   |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module master_spi #(
  parameter clk_freq = 0,
  parameter baud_rate = 100000,
  parameter cpol = 0,
  parameter cpha = 0,
  parameter num_slaves = 1
)
(
  //System signals.
  input clk,
  input reset,

  //Processor bus signals.
  input rd_en,
  input [3:0] wr_en,
  input [31:2] addr,
  output reg [31:0] rd_data,
  input [31:0] wr_data,

  //Peripheral I/O signals.
  output reg sck,
  input miso,
  output mosi,
  output [num_slaves-1:0] ss
);
  //Work around an XST bug that disallows system functions being used with localparam.
  `ifdef XST
    `define localparam parameter
  `else
    `define localparam localparam
  `endif

  //Calculate the integer ratio between the clock frequency and the desired baud rate. Round up so
  //that the specified baud rate is never exceeded.
  `define prescale_ratio ($rtoi($ceil($itor(clk_freq) / $itor(baud_rate))))

  //Prescale period and number of prescaler bits for baud rate generation. Note that the prescale
  //period is set to 2 at minimum.
  `localparam prescale_period = (`prescale_ratio >= 2)? `prescale_ratio: 2;
  `localparam prescaler_bits = $clog2(prescale_period);

  //Amount of bits to allocate to the slave_sel register. There should always be enough so that an
  //integer value that doesn't activate any slave select line can be encoded.
  `localparam slave_sel_len = $clog2(num_slaves + 1);

  //Make sure that the input clock frequency is configured correctly.
  generate
    if (clk_freq == 0)
      invalid_parameter_value clk_freq();
  endgenerate

  //Registers associated with the clock prescaler.
  reg [prescaler_bits-1:0] prescale_count;  //Current prescale count
  reg shift_cken;                           //Tells whether to transfer a bit in the current cycle

  //Registers associated with the main shift register.
  reg [7:0] shift_reg;    //The contents of the shift register
  reg [2:0] shift_count;  //Tracks how many bits of the current byte have been transferred
  reg shift_active;       //Tells whether the shift register is actively transferring data
  reg shift_rx_full;      //Tells whether the shift register holds unretrieved inbound data

  //Registers associated with the transmit and receive FIFOs.
  reg [7:0] tx_fifo_data;   //Contents of the transmit FIFO
  reg [7:0] rx_fifo_data;   //Contents of the receive FIFO
  reg tx_fifo_full;         //Tells whether the transmit FIFO holds unsent data
  reg rx_fifo_full;         //Tells whether the receive FIFO holds unretrieved data

  //Slave select register.
  reg [slave_sel_len-1:0] slave_sel;

  //The MOSI line is hardwired to the MSB of the shift register.
  assign mosi = shift_reg[7];

  //Demultiplexer for slave select signals.
  generate
    genvar i;
    for (i = 0; i < num_slaves; i = i + 1)
      assign ss[i] = slave_sel != i;
  endgenerate

  always @(posedge clk) begin
    if (reset) begin
      prescale_count <= 0;
      shift_cken <= 1'b0;
      shift_reg <= 8'd0;
      shift_count <= 3'd0;
      shift_active <= 1'b0;
      shift_rx_full <= 1'b0;
      tx_fifo_data <= 8'd0;
      rx_fifo_data <= 8'd0;
      tx_fifo_full <= 1'b0;
      rx_fifo_full <= 1'b0;
      slave_sel <= { slave_sel_len { 1'b1 } };
      sck <= cpol;
      rd_data <= 32'd0;
    end
    else begin
      //Check whether there's new data to transmit. If also the shift register is inactive and
      //available, start a new transfer.
      if (tx_fifo_full && !shift_active && !shift_rx_full) begin
        //Load new data into the shift register, clear the transmit FIFO full flag and activate the
        //shift register.
        shift_reg <= tx_fifo_data;
        tx_fifo_full <= 1'b0;
        shift_active <= 1'b1;
      end

      //Update the prescaler logic and sck only if the shift register is active.
      if (shift_active) begin
        //Count up until the clock enable is asserted, then restart.
        prescale_count <= shift_cken? 0: prescale_count + 1'd1;

        //Update SCK at the middle of the prescale period. If the period is odd, update it a little
        //bit after the actual half period.
        if (prescale_count == prescale_period / 2 - !prescale_period[0])
          sck <= !cpol ^ cpha;

        //Also update sck at the end of the prescale period.
        if (prescale_count == prescale_period - 1)
          sck <= cpol ^ cpha;

        //Assert the clock enable one cycle before the last prescale count value is reached
        //(shift_cken is registered and delayed one cycle).
        shift_cken <= prescale_count == prescale_period - 2'd2;
      end

      //Update the shift register logic only when shift_cken is active (this signal only activates
      //when the shift register is active, so no need to check for that).
      if (shift_cken) begin
        //Shift the data by default (can be overriden below). Increase the shift count.
        shift_reg <= { shift_reg[6:0], miso };
        shift_count <= shift_count + 1'd1;

        //Perform additional logic when shifting the last bit.
        if (shift_count == 7) begin
          //Check whether more data awaits in the transmit FIFO. If there's also space in the
          //receive FIFO then prepare for the next byte transfer (back to back transfers).
          if (tx_fifo_full && !rx_fifo_full) begin
            //Fill the shift register with new data (override the default shift behavior), clear the
            //transmit FIFO full flag, store the received byte (it has to be shifted, as the last
            //shift has been overriden) and set the receive FIFO full flag.
            shift_reg <= tx_fifo_data;
            tx_fifo_full <= 1'b0;
            rx_fifo_data <= { shift_reg[6:0], miso };
            rx_fifo_full <= 1'b1;
          end
          else begin
            //Either no more data in the transmit FIFO or the receive FIFO is full. Set the shift
            //register receive full flag and deactivate the shift register.
            shift_rx_full <= 1'b1;
            shift_active <= 0;
          end
        end
      end

      //Check whether the shift register holds unretrieved data and there's space in the receive
      //FIFO. If so, flush the data.
      if (shift_rx_full && !rx_fifo_full) begin
        //Load the contents of the shift register into the receive FIFO, set the receive FIFO full
        //flag and clear the shift register receive full flag.
        rx_fifo_data <= shift_reg;
        rx_fifo_full <= 1'b1;
        shift_rx_full <= 1'b0;
      end

      //Check for CPU write accesses to this peripheral. Note that only writes to the LSB lane are
      //recognized as no register is larger than 8 bits.
      if (wr_en[0]) begin
        //Write accesses to the data register store the value in the transmit FIFO, but are only
        //effective if it's not full.
        if (addr[3:2] == 2'd0 && !tx_fifo_full) begin
          tx_fifo_data <= wr_data[7:0];
          tx_fifo_full <= 1'b1;
        end

        //Write accesses to the slave select register simply store the value in it.
        if (addr[3:2] == 2'd1) begin
          slave_sel <= wr_data[slave_sel_len-1:0];
        end
      end

      //Check for CPU read accesses to this peripheral.
      if (rd_en) begin
        if (addr[3:2] == 2'd0) begin
          //Read accesses to the data register return the value stored in the receive FIFO and cause
          //it to become empty.
          rd_data <= { 24'd0, rx_fifo_data };
          rx_fifo_full <= 1'b0;
        end
        else if (addr[3:2] == 2'd1) begin
          //Read accesses to the slave select register simply return the value stored in it.
          rd_data <= { { 32 - slave_sel_len { 1'b0 } }, slave_sel };
        end
        else begin
          //Read accesses to the status register return the values of the flags.
          rd_data <= { 30'd0, rx_fifo_full, !tx_fifo_full };
        end
      end
      else begin
        //Set the output register to 0 when not reading, so all peripheral outputs can be combined
        //with a simple OR operation.
        rd_data <= 32'd0;
      end
    end
  end
endmodule
