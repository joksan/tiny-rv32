//+------------------------------------------------------------------------------------------------+
//| Hardware abstraction layer header for the master SPI peripheral.                               |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef MASTER_SPI_H_
#define MASTER_SPI_H_

#include <stdint.h>

//Structure describing the register mapping for the master SPI peripheral.
typedef struct {
  uint8_t data;             //Incoming/outgoing data register
  uint8_t reserved1[3];
  uint8_t slave_sel;        //Slave select register
  uint8_t reserved2[3];
  union {
    uint8_t status;         //Status register (full byte)
    struct {
      uint8_t txready: 1;   //Transmission ready status bit
      uint8_t rxready: 1;   //Reception ready status bit
    } status_bits;
  };
} mspi_t;

//Bitmasks for the status register.
#define MSPI_STATUS_TXREADY_MSK 0x01
#define MSPI_STATUS_RXREADY_MSK 0x02

//Macro used for accessing an specific instance of the master SPI peripheral.
#define MSPI(inst) (*((volatile mspi_t *) MSPI ## inst ## _BASE_ADDR))

#endif //MASTER_SPI_H_
