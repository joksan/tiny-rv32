//+------------------------------------------------------------------------------------------------+
//| UART peripheral for the tiny-rv32e softcore processor.                                         |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module uart #(
  parameter clk_freq = 0,
  parameter baud_rate = 0
)
(
  //System signals.
  input clk,
  input reset,

  //Processor bus signals.
  input rd_en,
  input [3:0] wr_en,
  input [31:2] addr,
  output reg [31:0] rd_data,
  input [31:0] wr_data,

  //Peripheral I/O signals.
  input rx,
  output tx
);
  //Signals from the transmitter module.
  wire uart_transmitter_ready;

  //Signals from the receiver module.
  wire [7:0] uart_receiver_data;
  wire uart_receiver_ready;

  //Make sure that the input clock frequency and the baud rate are configured correctly.
  generate
    if (clk_freq == 0)
      invalid_parameter_value clk_freq();
    if (baud_rate == 0)
      invalid_parameter_value baud_rate();
  endgenerate

  //UART transmitter module instance.
  uart_transmitter #(
    .clk_freq(clk_freq),
    .baud_rate(baud_rate)
  )
  transmitter (
    //System signals.
    .clk(clk),
    .reset(reset),

    //Control signals.
    .wr_en(wr_en[0]),
    .data(wr_data[7:0]),
    .ready(uart_transmitter_ready),

    //Peripheral I/O signals.
    .tx(tx)
  );

  //UART receiver module instance.
  uart_receiver #(
    .clk_freq(clk_freq),
    .baud_rate(baud_rate)
  )
  receiver (
    //System signals.
    .clk(clk),
    .reset(reset),

    //Control signals.
    .rd_en(rd_en && addr[2] == 1'd0),
    .data(uart_receiver_data),
    .ready(uart_receiver_ready),

    //Peripheral I/O signals.
    .rx(rx)
  );

  //Multiplexer block for data read by the processor.
  always @(posedge clk) begin
    if (rd_en) begin
      if (addr[2] == 1'd0)
        //Addresses pointing to even 32-bit words cause the data register to be read.
        rd_data <= { 24'd0, uart_receiver_data };
      else
        //Addresses pointing to odd 32-bit words cause the status register to be read.
        rd_data <= { 30'd0, uart_receiver_ready, uart_transmitter_ready };
    end
    else begin
      //When not read, place zeroes on the bus so all peripheral outputs can be combined using OR.
      rd_data <= 32'd0;
    end
  end
endmodule

//Transmitter module. Not intended to be instantiated directly by other sources.
module uart_transmitter #(
  parameter clk_freq = 0,
  parameter baud_rate = 0
)
(
  //System signals.
  input clk,
  input reset,

  //Control signals.
  input wr_en,
  input [7:0] data,
  output ready,

  //Peripheral I/O signals.
  output tx
);
  localparam prescale_period = clk_freq / baud_rate;   //Prescale period for baud rate generation
`ifndef XST
  localparam prescaler_bits = $clog2(prescale_period);   //Prescaler bits
`else
  parameter prescaler_bits = $clog2(prescale_period);    //Same (work around an XST bug)
`endif

  //Prescale count and clock enable.
  reg [prescaler_bits-1:0] prescale_count;
  reg cken;

  //Main shift register and shift count. This module is idle whenever the shift count is zero.
  reg [8:0] shift_reg;
  reg [3:0] shift_count;

  //FIFO register and full flag.
  reg [7:0] fifo_reg;
  reg fifo_full;

  //The ready signal is asserted as long as the FIFO is not full.
  assign ready = !fifo_full;

  //The TX signal is tied directly to the LSB of the shift register.
  assign tx = shift_reg[0];

  //Transmitter logic block.
  always @(posedge clk) begin
    if (reset) begin
      prescale_count <= 0;
      cken <= 1'b0;
      shift_reg <= 9'h001;
      shift_count <= 4'd0;
      fifo_reg <= 8'd0;
      fifo_full <= 1'b0;
    end
    else begin
      //Update the prescale count only if the module is not idle (shift count is non zero).
      if (shift_count != 4'd0)
        //Count up until the clock enable is asserted, then restart.
        prescale_count <= cken? 0: prescale_count + 1'd1;

      //Assert the clock enable one cycle before the last prescale count value is reached (cken is
      //registered and delayed one cycle).
      cken <= prescale_count == prescale_period - 2'd2;

      //Accept bus writes only if the FIFO is not full.
      if (wr_en && !fifo_full) begin
        //Upon accepting a write, copy the data to the FIFO register and set the full flag.
        fifo_reg <= data;
        fifo_full <= 1'b1;
      end

      //Check whether this module is idle (shift count is zero).
      if (shift_count == 4'd0) begin
        //If idle, check if FIFO is full.
        if (fifo_full) begin
          //The module is ready and there's data to transmit. Copy the data from the FIFO to the
          //shift register, with the LSB set to zero to signal the start bit. Clear the full flag
          //and set the shift count to 10 bits (1 start, 8 data and 1 stop).
          shift_reg <= { fifo_reg, 1'b0 };
          fifo_full <= 1'b0;
          shift_count <= 4'd10;
        end
      end
      else begin
        //If active, check whether clock enable is active (bit time has expired).
        if (cken) begin
          //Time to transmit the next bit of data. Advance the shift register to the right with ones
          //entering from the left so the last (stop bit) comes as a one. Also decrease the shift
          //count until reaching 0.
          shift_reg <= { 1'b1, shift_reg[8:1] };
          shift_count <= shift_count - 1'd1;
        end
      end
    end
  end
endmodule

//Receiver module. Not intended to be instantiated directly by other sources.
module uart_receiver #(
  parameter clk_freq = 0,
  parameter baud_rate = 0
)
(
  //System signals.
  input clk,
  input reset,

  //Control signals.
  input rd_en,
  output [7:0] data,
  output ready,

  //Peripheral I/O signals.
  input rx
);
  localparam prescale_period = clk_freq / baud_rate;  //Prescale period for baud rate generation
`ifndef XST
  localparam prescaler_bits = $clog2(prescale_period * 3 / 2);  //Prescaler bits
`else
  parameter prescaler_bits = $clog2(prescale_period * 3 / 2);
`endif

  //Receiver active flag. Turns on whenever a new frame is detected.
  reg active;

  //Prescale count and clock enable.
  reg [prescaler_bits-1:0] prescale_count;
  reg cken;

  //Main shift register and shift count.
  reg [6:0] shift_reg;    //Only 7 bits needed
  reg [2:0] shift_count;

  //FIFO register and full flag.
  reg [7:0] fifo_reg;
  reg fifo_full;

  //Input sampling registers. T0 is the newest sample. T1 is the previous sample.
  reg rx_sample_t0;
  reg rx_sample_t1;

  //The data output is tied directly to the FIFO register.
  assign data = fifo_reg;

  //The ready signal is asserted as long as the FIFO is full.
  assign ready = fifo_full;

  //Receiver logic block.
  always @(posedge clk) begin
    if (reset) begin
      active <= 1'b0;
      prescale_count <= prescale_period * 3 / 2 - 1;  //1.5 times the prescale period (read below)
      cken <= 1'b0;
      shift_reg <= 7'h00;
      shift_count <= 3'd7;
      fifo_reg <= 8'd0;
      fifo_full <= 1'b0;
      rx_sample_t0 <= 1'b0;
      rx_sample_t1 <= 1'b0;
    end
    else begin
      //Refresh the input sampling registers.
      rx_sample_t0 <= rx;
      rx_sample_t1 <= rx_sample_t0;

      //Activate reception when a negative edge of the RX signal (start bit) is detected while
      //inactive.
      if (!active && !rx_sample_t0 && rx_sample_t1)
        active <= 1'b1;

      //Update the prescale count only if the module is active.
      if (active) begin: prescaler_update
        integer preload_value;

        //When the shift count is zero (a frame is about to end) set the preload value to 1.5 times
        //the normal value, so that the first bit of the next frame is sampled at the middle of the
        //LSB past the start bit (which is detected separately when reception is activated). For the
        //remaining bits set the preload value to the regular prescale period.
        preload_value = (shift_count == 0)? prescale_period * 3 / 2 - 1: prescale_period - 1;

        //Count down until the clock enable is asserted, then restart.
        prescale_count <= cken? preload_value: prescale_count - 1'd1;
      end

      //Assert the clock enable one cycle before the last prescale count value is reached (cken is
      //registered and delayed one cycle).
      cken <= prescale_count == 4'd1;

      //Check whether clock enable is active (bit time has expired) while reception is active.
      if (active && cken) begin
        //Time to capture the next bit of data. Advance the shift register to the right with the new
        //sampled bit entering from the left. Also decrease the shift count until reaching 0.
        shift_reg <= { rx_sample_t0, shift_reg[6:1] };
        shift_count <= shift_count - 1'd1;

        //Check whether the shift count has ended.
        if (shift_count == 3'd0) begin
          //Frame is (almost) finished. Copy the accumulated bits from the shift register (7 of
          //them) and the current sampled bit (MSB) to the FIFO register and set the full flag.
          //Also deactivate reception.
          fifo_reg <= { rx_sample_t0, shift_reg };
          fifo_full <= 1'b1;
          active <= 1'b0;
        end
      end

      //Clear the FIFO full flag whenever data is read.
      if (rd_en)
        fifo_full <= 1'b0;
    end
  end
endmodule
