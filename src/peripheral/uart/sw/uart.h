//+------------------------------------------------------------------------------------------------+
//| Hardware abstraction layer header for the UART peripheral.                                     |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef UART_H_
#define UART_H_

#include <stdint.h>

//Structure describing the register mapping for the UART peripheral.
typedef struct {
  uint8_t data;             //Incoming/outgoing data register
  uint8_t reserved[3];
  union {
    uint8_t status;         //Status register (full byte)
    struct {
      uint8_t txready: 1;   //Transmission ready status bit
      uint8_t rxready: 1;   //Reception ready status bit
    } status_bits;
  };
} uart_t;

//Bitmasks for the status register.
#define UART_STATUS_TXREADY_MSK 0x01
#define UART_STATUS_RXREADY_MSK 0x02

//Macro used for accessing an specific instance of the UART peripheral.
#define UART(inst) (*((volatile uart_t *) UART ## inst ## _BASE_ADDR))

#endif //UART_H_
