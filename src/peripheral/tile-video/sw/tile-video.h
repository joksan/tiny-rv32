//+------------------------------------------------------------------------------------------------+
//| Hardware abstraction layer header for the tile-based video generator.                          |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

#ifndef TILE_VIDEO_H_
#define TILE_VIDEO_H_

#include <stdint.h>

//Structure describing the register mapping for the tile-video peripheral.
typedef struct {
  uint8_t x_offset;                 //Offset/scroll registers
  uint8_t y_offset;
  uint8_t reserved_0[0x0000FFFE];
  uint16_t tilemap_mem[1280];       //Tilemap memory
  uint8_t reserved_1[0x0000F600];
  uint16_t palette_mem[256];        //Palette memory
  uint8_t reserved_2[0x0001FE00];
  uint16_t pattern_1bpp_mem[1024];  //Image pattern memory - 1bpp resolution
  uint8_t reserved_3[0x0000FC00];
  uint16_t pattern_2bpp_mem[2048];  //Image pattern memory - 2bpp resolution
  uint8_t reserved_4[0x0000F800];
  uint16_t pattern_4bpp_mem[4096];  //Image pattern memory - 4bpp resolution
} tile_video_t;

//Macro used for accessing an specific instance of the tile-video peripheral.
#define TILE_VIDEO(inst) (*((volatile tile_video_t *) TILE_VIDEO_ ## inst ## _BASE_ADDR))

#endif //TILE_VIDEO_H_
