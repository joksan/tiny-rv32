//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - Video compositor module.    |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_compositor #(
  parameter init_file = ""
)
(
  //System and common signals.
  input clk,
  input reset,
  input hold,

  //Processor bus signals.
  input [3:0] cpu_wr_en,
  input [31:2] cpu_addr,
  input [31:0] cpu_wr_data,

  //Input control signals.
  input lrow_prep,            //Logical row needs to be prepared

  //Pipeline output signals.
  output reg wr_en,           //Write enable for next stage in pipeline (tile_video_pattern)
  output reg [8:0] scr_x,     //X coordinate of pixel in screen
  output reg [0:0] scr_y,     //Y coordinate of pixel in screen (LSB only)
  output reg [2:0] pat_x,     //X coordinate of pixel in pattern
  output reg [2:0] pat_y,     //Y coordinate of pixel in pattern
  output [7:0] palette_sel,   //Color palette selection byte
  output [7:0] pattern_num    //Pattern number selection byte
);
  //Module state signals.
  //---------------------
  reg active;                     //Active state register (tells when pixel data generation is on)
  reg [8:0] scr_x_count;          //Screen coordinate counters
  reg [7:0] scr_y_count;
  reg [2:0] pat_x_count;          //Pattern coordinate counters
  reg [2:0] pat_y_count;
  reg [10:0] tilemap_row_start;   //Row start address in tilemap memory
  reg [2:0] x_offset;             //Position offset registers (mapped in memory and exposed to the
  reg [2:0] y_offset;             //CPU)

  //Tilemap memory signals.
  //-----------------------
  //The visible area of the screen at QGVA resolution (320x240 pixels) contains 40x30 tiles with a
  //8x8 size each. The non-visible area allows for a single additional tile so pixel-resolution
  //scrolling is possible by altering the offset registers, meaning that the logical space is 41x31
  //tiles or 1271 tiles in total. This is not close to a power of 2 (next one is 2048), so the
  //tilemap memory is truncated at the next 256-tile boundary, which is 1280 tiles (with only 9
  //tiles left over).
  //In the case of iCE40 FPGAs, this tile boundary allows to use BRAMs very efficiently since each
  //one can hold 256 tiles, meaning that 5 BRAMs are required in total. In other FPGA families,
  //space usage efficiency may be lower if they have larger (lower granularity) BRAMs.

  wire mem_wr_en;               //Memory write enable (single signal for 16 bits - covers 2 lanes)
  reg [10:0] mem_rd_addr;       //Address to read from tilemap memory
  wire [10:0] mem_wr_addr;      //Address to write to tilemap memory
  wire [15:0] mem_rd_data;      //Data read from tilemap memory
  wire [15:0] mem_wr_data;      //Data written to tilemap memory

  //Hardware description section.
  //-----------------------------
  //Tilemap memory is at offset 0x10000. Activate the memory write enable signal only if the
  //relevant address bits are decoded correctly. Since only 16-bit writes are supported, activate
  //the write enable signal whenever either the lower or higher write enable signals from the CPU
  //are activated.
  assign mem_wr_en = (cpu_addr[18:16] == 3'd1) &&
                     ((cpu_wr_en == 4'b0011) || (cpu_wr_en == 4'b1100));

  //Adjust the LSB of the tilemap memory address depending on whether the lower or higher half word
  //is being written.
  assign mem_wr_addr = { cpu_addr[11:2], |cpu_wr_en[3:2] };

  //Use either the lower or higher portion from the CPU data bus based on which half is being
  //written.
  assign mem_wr_data = (|cpu_wr_en[3:2])? cpu_wr_data[31:16]: cpu_wr_data[15:0];

  //Tilemap memory.
  ram_16r_16w #(
    .init_file(init_file),
    .memory_size(1280),
    .addr_width($clog2(1280))
  )
  tilemap_memory (
    .clk(clk),
    .hold(hold),
    .wr_en(mem_wr_en),
    .rd_addr(mem_rd_addr),
    .wr_addr(mem_wr_addr),
    .rd_data(mem_rd_data),
    .wr_data(mem_wr_data)
  );

  //Write process for memory mapped registers.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      x_offset <= 3'd0;
      y_offset <= 3'd0;
    end
    else begin
      //Memory mapped registers are at offset 0x00000. Since there are only 2 registers, use the
      //byte-lane write-enable signals to differentiate them.
      if (cpu_addr[18:16] == 3'd0) begin
        if (cpu_wr_en[0])
          x_offset <= cpu_wr_data[2:0];
        if (cpu_wr_en[1])
          y_offset <= cpu_wr_data[10:8];
      end
    end
  end

  //The color palette and pattern number selection signals come directly from tilemap memory.
  assign palette_sel = mem_rd_data[15:8];
  assign pattern_num = mem_rd_data[7:0];

  //Video compositor FSM block.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      //Reset module state signals.
      active <= 1'b0;
      scr_x_count <= 9'd0;
      scr_y_count <= 8'd0;
      pat_x_count <= 3'd0;
      pat_y_count <= 3'd0;
      tilemap_row_start <= 11'd0;

      //Reset tilemap memory signals.
      mem_rd_addr <= 11'd0;

      //Reset the pipeline output signals.
      wr_en <= 1'b0;
      scr_x <= 9'd0;
      scr_y <= 1'd0;
      pat_x <= 3'd0;
      pat_y <= 3'd0;
    end

    //Update the FSM only when the hold signal is not asserted.
    else if (!hold) begin
      //Translate signals through the pipeline after each clock cycle.
      wr_en <= active;
      scr_x <= scr_x_count;
      scr_y <= scr_y_count[0];
      pat_x <= pat_x_count;
      pat_y <= pat_y_count;

      //Act upon current FSM state.
      if (!active) begin
        //During the inactive state wait for the logical row preparation control signal.
        if (lrow_prep) begin
          //Upon receiving the signal, transition to next state and initialize all relevant
          //registers.
          active <= 1'b1;                     //Set state to active
          scr_x_count <= 9'd0;                //Reset the screen X coordinate before the new row
          pat_x_count <= x_offset;            //Set the pattern X coordinate counter to offset value
          mem_rd_addr <= tilemap_row_start;   //Set the memory read address to the next row start

          //In addition to row initialization, perform further initialization when a new frame is
          //started.
          if (scr_y_count == 8'd0) begin
            pat_y_count <= y_offset;      //Set the pattern Y coordinate counter to offset value
            tilemap_row_start <= 11'd0;   //Reset the row start address
            mem_rd_addr <= 11'd0;         //Also reset the memory read address (overrides above)
          end
        end
      end
      else begin
        //During the active state update the module state signals in order to generate the pixel
        //write transactions to the next stage in pipeline.
        scr_x_count <= scr_x_count + 1;   //Advance the screen and pattern X coordinate counters
        pat_x_count <= pat_x_count + 1;

        //Advance the memory read address upon ending the current pattern.
        if (pat_x_count == 3'd7)
          mem_rd_addr <= mem_rd_addr + 1;

        //Prepare for the next row upon ending the current one.
        if (scr_x_count == 9'd319) begin
          active <= 1'b0;                   //Go inactive until next logical row prepare
          scr_y_count <= scr_y_count + 1;   //Advance the screen and pattern Y coordinate counters
          pat_y_count <= pat_y_count + 1;

          //Advance the row start address to the next one upon ending the current pattern row.
          if (pat_y_count == 3'd7)
            tilemap_row_start <= tilemap_row_start + 41;

          //Reset the screen Y coordinate counter after finishing the last row.
          if (scr_y_count == 8'd239)
            scr_y_count <= 8'd0;
        end
      end
    end
  end
endmodule
