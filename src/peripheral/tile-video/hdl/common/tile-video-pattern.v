//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - Pattern generator module.   |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_pattern #(
  parameter patterns_1bpp = 0,
  parameter patterns_2bpp = 0,
  parameter patterns_4bpp = 0,
  parameter init_file_1bpp = "",
  parameter init_file_2bpp = "",
  parameter init_file_4bpp = ""
)
(
  //System and common signals.
  input clk,
  input reset,
  input hold,

  //Processor bus signals.
  input [3:0] cpu_wr_en,
  input [31:2] cpu_addr,
  input [31:0] cpu_wr_data,

  //Pipeline input signals from tile_video_compositor (see respective module for port descriptions).
  input in_wr_en,
  input [8:0] in_scr_x,
  input [0:0] in_scr_y,
  input [2:0] pat_x,
  input [2:0] pat_y,
  input [7:0] in_palette_sel,
  input [7:0] pattern_num,

  //Pipeline output signals.
  output reg out_wr_en,               //Write enable for next stage in pipeline (tile_video_palette)
  output reg [8:0] out_scr_x,         //X coordinate of pixel in screen
  output reg [0:0] out_scr_y,         //Y coordinate of pixel in screen (LSB only)
  output reg [7:0] out_palette_sel,   //Color palette selection byte
  output reg color_1bpp,              //Palette-color outputs
  output reg [1:0] color_2bpp,
  output reg [3:0] color_4bpp
);
  //Patterns are 8x8 pixel images that can be stored in different color resolutions, including 1BPP,
  //2BPP and 4BPP (bits per pixel). For each resolution an independent set of memory, registers and
  //pipeline logic are instantiated.

  //The parameters called patterns_1bpp, patterns_2bpp and patterns_4bpp define the amount of
  //patterns per color resolution. If one is unset (left as 0), that particular color resolution is
  //not implemented and all related resources are freed. Otherwise, the parameter can be set to a
  //value up to 256 (see each section below for details on memory usage). All color resolutions are
  //optional, but at least one should be enabled in order to provide any functionality.

  //Internal pipeline state signals.
  //================================================================================================
  //There are two internal pipeline stages in this module. The C1 and C2 prefixes indicate which
  //cycle/stage they belong to. These signals are shared for all color resolutions.
  reg c1_wr_en;               //Write enable signals.
  reg c2_wr_en;
  reg [8:0] c1_scr_x;         //X coordinate of pixel in screen
  reg [8:0] c2_scr_x;
  reg [0:0] c1_scr_y;         //Y coordinate of pixel in screen (LSB only)
  reg [0:0] c2_scr_y;
  reg [2:0] c1_pat_x;         //X coordinate of pixel in pattern (consumed partially in C1)
  reg [1:0] c2_pat_x;
  reg [0:0] c1_pat_y;         //Y coordinate of pixel in pattern (consumed completely in C1)
  reg [7:0] c1_palette_sel;   //Color palette selection byte
  reg [7:0] c2_palette_sel;
  wire [15:0] mem_wr_data;    //Data from CPU (may be written to memory for any color resolution)

  //Shared pipeline logic.
  //================================================================================================
  //Logic here is shared for all color resolutions.

  //Use either the lower or higher portion from the CPU data bus based on which half is being
  //written.
  assign mem_wr_data = (|cpu_wr_en[3:2])? cpu_wr_data[31:16]: cpu_wr_data[15:0];

  //Common pipeline block for the pattern generator. This propagates state signals.
  always @(posedge clk) begin
    if (reset) begin
      //Reset the internal pipeline state signals.
      c1_wr_en <= 1'b0;
      c2_wr_en <= 1'b0;
      c1_scr_x <= 9'd0;
      c2_scr_x <= 9'd0;
      c1_scr_y <= 1'd0;
      c2_scr_y <= 1'd0;
      c1_pat_x <= 3'd0;
      c2_pat_x <= 2'd0;
      c1_pat_y <= 1'd0;
      c1_palette_sel <= 8'd0;
      c2_palette_sel <= 8'd0;

      //Reset the pipeline output signals.
      out_wr_en <= 1'b0;
      out_scr_x <= 9'd0;
      out_scr_y <= 1'd0;
      out_palette_sel <= 8'd0;
    end
    //Update the pipeline only when the hold signal is not asserted.
    else if (!hold) begin
      //Translate signals through the pipeline after each clock cycle.
      c1_wr_en <= in_wr_en;               //Write enable goes from input to output in 3 cycles
      c2_wr_en <= c1_wr_en;
      out_wr_en <= c2_wr_en;

      c1_scr_x <= in_scr_x;               //Screen X coordinate goes from input to output
      c2_scr_x <= c1_scr_x;
      out_scr_x <= c2_scr_x;

      c1_scr_y <= in_scr_y;               //Screen Y coordinate goes from input to output
      c2_scr_y <= c1_scr_y;
      out_scr_y <= c2_scr_y;

      c1_pat_x <= pat_x;                  //Pattern X coordinate propagates only towards cycle 2
      c2_pat_x <= c1_pat_x[1:0];

      c1_pat_y <= pat_y[0];               //Pattern Y coordinate propagates only towards cycle 1

      c1_palette_sel <= in_palette_sel;   //Color palette selection byte goes from input to output
      c2_palette_sel <= c1_palette_sel;
      out_palette_sel <= c2_palette_sel;
    end
  end

  //Pipeline logic for individual color resolutions.
  //================================================================================================
  //Signals and logic within each block are instatiated only if the corresponding color resolution
  //is enabled.
  generate
    //1BPP color resolution section.
    //------------------------------
    if (patterns_1bpp) begin
      //The following table shows memory usage for 1BPP patterns in different FPGA families. Note
      //that the maximum amount of patterns per color resolution is limited by design at 256.

      //   Family   | Patterns per BRAM | Max BRAM count
      //------------+-------------------+---------------
      //   iCE40    |         64        |       4
      // Cyclone IV |        128        |       2
      // Spartan 3E |        256        |       1
      // Spartan 6  |        256        | 1 (RAMB16BWER)

      if (patterns_1bpp > 256)
        invalid_parameter_value patterns_1bpp();

      //Pattern memory signals.
`ifndef XST
      localparam pat_addr_bits_1bpp = $clog2(patterns_1bpp);  //Address bus bits
`else
      parameter pat_addr_bits_1bpp = $clog2(patterns_1bpp);   //Same (work around an XST bug)
`endif
      wire mem_wr_en_1bpp;                                    //Write enable (for all 16 bits)
      wire [pat_addr_bits_1bpp+1:0] mem_rd_addr_1bpp;         //Address to read from pattern memory
      wire [pat_addr_bits_1bpp+1:0] mem_wr_addr_1bpp;         //Address to write to pattern memory
      wire [15:0] mem_rd_data_1bpp;                           //Data read from pattern memory

      //Data multiplexer signals.
      reg [3:0] c2_mux_1bpp;  //Intermediate data. Updated during cycle 2. Holds 4 pixels.

      //1BPP pattern memory is at offset 0x40000. Activate the memory write enable signal only if
      //the relevant address bits are decoded correctly. Since only 16-bit writes are supported,
      //activate the write enable signal whenever either the lower or higher write enable signals
      //from the CPU are activated.
      assign mem_wr_en_1bpp = (cpu_addr[18:16] == 3'd4) &&
                              ((cpu_wr_en == 4'b0011) || (cpu_wr_en == 4'b1100));

      //Each location of the memory can hold 2 rows simultaneously. Use the 2 MSBs of the pattern Y
      //coordinate to select a pair of rows.
      assign mem_rd_addr_1bpp = { pattern_num[pat_addr_bits_1bpp-1:0], pat_y[2:1] };

      //Adjust the LSB of the pattern memory address depending on whether the lower or higher half
      //word is being written.
      assign mem_wr_addr_1bpp = { cpu_addr[pat_addr_bits_1bpp+2:2], |cpu_wr_en[3:2] };

      //1BPP pattern memory.
      ram_16r_16w #(
        .init_file(init_file_1bpp),
        .memory_size(patterns_1bpp*4),
        .addr_width(pat_addr_bits_1bpp+2)
      )
      pattern_memory_1bpp (
        .clk(clk),
        .hold(hold),
        .wr_en(mem_wr_en_1bpp),
        .rd_addr(mem_rd_addr_1bpp),
        .wr_addr(mem_wr_addr_1bpp),
        .rd_data(mem_rd_data_1bpp),
        .wr_data(mem_wr_data)
      );

      //Pipeline block for 1bpp patterns.
      always @(posedge clk) begin
        //Pattern memory contents are multiplexed in 2 cycles to reduce the amount of required logic
        //and propagation time. Multiplexing is done differently depending on color resolution, as
        //each memory can hold a different amount of pixels per 16-bit location.
        if (reset) begin
          //Reset pipeline signals.
          c2_mux_1bpp <= 4'd0;
          color_1bpp <= 1'd0;
        end
        //Update the pipeline only when the hold signal is not asserted.
        else if (!hold) begin
          //Each location holds 2 rows of pattern pixels (16). Select the row with y[0] and the half
          //of the row with x[2] (4-to-1 multiplexers are implemented very efficiently with 4-input
          //LUTs).
          c2_mux_1bpp <= ({ c1_pat_y[0], c1_pat_x[2] } == 2'd3)? mem_rd_data_1bpp[15:12]:
                         ({ c1_pat_y[0], c1_pat_x[2] } == 2'd2)? mem_rd_data_1bpp[11:8]:
                         ({ c1_pat_y[0], c1_pat_x[2] } == 2'd1)? mem_rd_data_1bpp[7:4]:
                                                                 mem_rd_data_1bpp[3:0];

          //The intermediate multiplexer holds half a row (4 pixels). Select the final pixel with
          //x[1:0].
          color_1bpp <= (c2_pat_x[1:0] == 2'd3)? c2_mux_1bpp[3]:
                        (c2_pat_x[1:0] == 2'd2)? c2_mux_1bpp[2]:
                        (c2_pat_x[1:0] == 2'd1)? c2_mux_1bpp[1]:
                                                 c2_mux_1bpp[0];
        end
      end
    end
    else begin
      //1BPP color resolution disabled. Set output to 0.
      initial color_1bpp <= 1'd0;
    end

    //2BPP color resolution section.
    //------------------------------
    if (patterns_2bpp) begin
      //The following table shows memory usage for 2BPP patterns in different FPGA families.

      //   Family   | Patterns per BRAM | Max BRAM count
      //------------+-------------------+---------------
      //   iCE40    |         32        |       8
      // Cyclone IV |         64        |       4
      // Spartan 3E |        128        |       2
      // Spartan 6  |        128        | 2 (RAMB16BWER)

      if (patterns_2bpp > 256)
        invalid_parameter_value patterns_2bpp();

      //Pattern memory signals.
`ifndef XST
      localparam pat_addr_bits_2bpp = $clog2(patterns_2bpp);  //All similar to the 1BPP case
`else
      parameter pat_addr_bits_2bpp = $clog2(patterns_2bpp);
`endif
      wire mem_wr_en_2bpp;
      wire [pat_addr_bits_2bpp+2:0] mem_rd_addr_2bpp;
      wire [pat_addr_bits_2bpp+2:0] mem_wr_addr_2bpp;
      wire [15:0] mem_rd_data_2bpp;

      //Data multiplexer signals.
      reg [3:0] c2_mux_2bpp;  //Intermediate data. Holds 2 pixels.

      //2BPP pattern memory is at offset 0x50000. Perform similar actions as above. This time each
      //location can hold a single row so all the bits of the pattern Y coordinate are used.
      assign mem_wr_en_2bpp = (cpu_addr[18:16] == 3'd5) &&
                              ((cpu_wr_en == 4'b0011) || (cpu_wr_en == 4'b1100));
      assign mem_rd_addr_2bpp = { pattern_num[pat_addr_bits_2bpp-1:0], pat_y[2:0] };
      assign mem_wr_addr_2bpp = { cpu_addr[pat_addr_bits_2bpp+3:2], |cpu_wr_en[3:2] };

      //2BPP pattern memory.
      ram_16r_16w #(
        .init_file(init_file_2bpp),
        .memory_size(patterns_2bpp*8),
        .addr_width(pat_addr_bits_2bpp+3)
      )
      pattern_memory_2bpp (
        .clk(clk),
        .hold(hold),
        .wr_en(mem_wr_en_2bpp),
        .rd_addr(mem_rd_addr_2bpp),
        .wr_addr(mem_wr_addr_2bpp),
        .rd_data(mem_rd_data_2bpp),
        .wr_data(mem_wr_data)
      );

      //Pipeline block for 2bpp patterns.
      always @(posedge clk) begin
        if (reset) begin
          c2_mux_2bpp <= 4'd0;
          color_2bpp <= 2'd0;
        end
        else if (!hold) begin
          //Each location holds a single row of pixels. Select a pair of pixels with x[2:1].
          c2_mux_2bpp <= (c1_pat_x[2:1] == 2'd3)? mem_rd_data_2bpp[15:12]:
                         (c1_pat_x[2:1] == 2'd2)? mem_rd_data_2bpp[11:8]:
                         (c1_pat_x[2:1] == 2'd1)? mem_rd_data_2bpp[7:4]:
                                                  mem_rd_data_2bpp[3:0];

          //The intermediate multiplexer holds 2 pixels. Select the final pixel with x[0].
          color_2bpp <= (c2_pat_x[0] == 1'd1)? c2_mux_2bpp[3:2]:
                                               c2_mux_2bpp[1:0];
        end
      end
    end
    else begin
      //2BPP color resolution disabled.
      initial color_2bpp <= 2'd0;
    end

    //4BPP color resolution section.
    //------------------------------
    if (patterns_4bpp) begin
      //The following table shows memory usage for 4BPP patterns in different FPGA families.

      //   Family   | Patterns per BRAM | Max BRAM count
      //------------+-------------------+---------------
      //   iCE40    |         16        |      16
      // Cyclone IV |         32        |       8
      // Spartan 3E |         64        |       4
      // Spartan 6  |         64        | 4 (RAMB16BWER)

      if (patterns_4bpp > 256)
        invalid_parameter_value patterns_4bpp();

      //Pattern memory signals.
`ifndef XST
      localparam pat_addr_bits_4bpp = $clog2(patterns_4bpp);  //All similar to the above cases
`else
      parameter pat_addr_bits_4bpp = $clog2(patterns_4bpp);
`endif
      wire mem_wr_en_4bpp;
      wire [pat_addr_bits_4bpp+3:0] mem_rd_addr_4bpp;
      wire [pat_addr_bits_4bpp+3:0] mem_wr_addr_4bpp;
      wire [15:0] mem_rd_data_4bpp;

      //Data multiplexer signals.
      reg [7:0] c2_mux_4bpp;  //Intermediate data. Holds 2 pixels.

      //4BPP pattern memory is at offset 0x60000. Perform similar actions as above. This time each
      //location can hold half a row so all the bits of the pattern Y coordinate are used and also
      //the MSB of the pattern X coordinate.
      assign mem_wr_en_4bpp = (cpu_addr[18:16] == 3'd6) &&
                              ((cpu_wr_en == 4'b0011) || (cpu_wr_en == 4'b1100));
      assign mem_rd_addr_4bpp = { pattern_num[pat_addr_bits_4bpp-1:0], pat_y[2:0], pat_x[2] };
      assign mem_wr_addr_4bpp = { cpu_addr[pat_addr_bits_4bpp+4:2], |cpu_wr_en[3:2] };

      //4BPP pattern memory.
      ram_16r_16w #(
        .init_file(init_file_4bpp),
        .memory_size(patterns_4bpp*16),
        .addr_width(pat_addr_bits_4bpp+4)
      )
      pattern_memory_4bpp (
        .clk(clk),
        .hold(hold),
        .wr_en(mem_wr_en_4bpp),
        .rd_addr(mem_rd_addr_4bpp),
        .wr_addr(mem_wr_addr_4bpp),
        .rd_data(mem_rd_data_4bpp),
        .wr_data(mem_wr_data)
      );

      //Pipeline block for 4bpp patterns.
      always @(posedge clk) begin
        if (reset) begin
          c2_mux_4bpp <= 8'd0;
          color_4bpp <= 4'd0;
        end
        else if (!hold) begin
          //Each location holds half a row (4 pixels). Select a pair of pixels with x[1].
          c2_mux_4bpp <= (c1_pat_x[1] == 1'd1)? mem_rd_data_4bpp[15:8]:
                                                mem_rd_data_4bpp[7:0];

          //The intermediate multiplexer holds 2 pixels. Select the final pixel with x[0].
          color_4bpp <= (c2_pat_x[0] == 1'd1)? c2_mux_4bpp[7:4]:
                                               c2_mux_4bpp[3:0];
        end
      end
    end
    else begin
      //4BPP color resolution disabled.
      initial color_4bpp <= 4'd0;
    end
  endgenerate
endmodule
