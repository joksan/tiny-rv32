//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - Palette memory module.      |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_palette #(
  parameter init_file = ""
)
(
  //System and common signals.
  input clk,
  input reset,
  input hold,

  //Processor bus signals.
  input [3:0] cpu_wr_en,
  input [31:2] cpu_addr,
  input [31:0] cpu_wr_data,

  //Pipeline input signals from tile_video_pattern (see respective module for port descriptions).
  input in_wr_en,
  input [8:0] in_scr_x,
  input [0:0] in_scr_y,
  input [7:0] palette_sel,
  input color_1bpp,
  input [1:0] color_2bpp,
  input [3:0] color_4bpp,

  //Pipeline output signals.
  output reg out_wr_en,         //Write enable for next stage in pipeline (tile_video_row_buffer)
  output reg [8:0] out_scr_x,   //X coordinate of pixel in screen
  output reg [0:0] out_scr_y,   //Y coordinate of pixel in screen (LSB only)
  output [3:0] red,             //RGB color components
  output [3:0] green,
  output [3:0] blue
);
  //Palette memory signals.
  //-----------------------
  //The color palette has a fixed 256-color size. It's also global and shared between all image
  //patterns. Depending on color resolution a pattern can use 2 (1BPP), 4 (2BPP) or 16 (4BPP)
  //different entries from the palette. Said entries have to be adjacent, with the palette selection
  //byte indicating which range/subset of the color palette is used. The following tables illustrate
  //the color selection scheme.

  //            1BPP                          2BPP                         4BPP
  // palette select byte | range   palette select byte | range   palette select byte | range
  // --------------------+------   --------------------+------   --------------------+------
  //           1_0000000 | 0-1               01_000000 | 0-3              00_XX_0000 | 0-15
  //           1_0000001 | 2-3               01_000001 | 4-7              00_XX_0001 | 16-31
  //                 ... | ...                     ... | ...                     ... | ...
  //           1_1111111 | 254-255           01_111111 | 252-255          00_XX_1111 | 240-255

  //In the case of iCE40 FPGAs, the palette fits in a single BRAM. In other FPGA families, space
  //usage efficiency may be lower if they have larger (lower granularity) BRAMs.

  wire mem_wr_en;             //Memory write enable (single signal for 16 bits - covers 2 lanes)
  wire [7:0] mem_rd_addr;     //Address to read from palette memory
  wire [7:0] mem_wr_addr;     //Address to write to palette memory
  wire [15:0] mem_rd_data;    //Data read from palette memory
  wire [15:0] mem_wr_data;    //Data written to palette memory

  //Hardware description section.
  //-----------------------------
  //Palette memory is at offset 0x20000. Activate the memory write enable signal only if the
  //relevant address bits are decoded correctly. Since only 16-bit writes are supported, activate
  //the write enable signal whenever either the lower or higher write enable signals from the CPU
  //are activated.
  assign mem_wr_en = (cpu_addr[18:16] == 3'd2) &&
                     ((cpu_wr_en == 4'b0011) || (cpu_wr_en == 4'b1100));

  //Bits 7 and 6 of the palette selection byte indicate the color resolution, while bits 6 to 0
  //indicate the palette range to use.
  assign mem_rd_addr = (palette_sel[7] == 1'b1)?    { palette_sel[6:0], color_1bpp }:
                       (palette_sel[7:6] == 2'b01)? { palette_sel[5:0], color_2bpp }:
                                                    { palette_sel[3:0], color_4bpp };

  //Adjust the LSB of the palette memory address depending on whether the lower or higher half word
  //is being written.
  assign mem_wr_addr = { cpu_addr[8:2], |cpu_wr_en[3:2] };

  //Use either the lower or higher portion from the CPU data bus based on which half is being
  //written.
  assign mem_wr_data = (|cpu_wr_en[3:2])? cpu_wr_data[31:16]: cpu_wr_data[15:0];

  //Output RGB data comes directly from memory.
  assign red = mem_rd_data[11:8];
  assign green = mem_rd_data[7:4];
  assign blue = mem_rd_data[3:0];

  //Palette memory.
  ram_16r_16w #(
    .init_file(init_file),
    .memory_size(256),
    .addr_width($clog2(256))
  )
  palette_memory (
    .clk(clk),
    .hold(hold),
    .wr_en(mem_wr_en),
    .rd_addr(mem_rd_addr),
    .wr_addr(mem_wr_addr),
    .rd_data(mem_rd_data),
    .wr_data(mem_wr_data)
  );

  //Palette memory pipeline block.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      //Reset the pipeline output signals.
      out_wr_en <= 1'b0;
      out_scr_x <= 9'd0;
      out_scr_y <= 1'd0;
    end

    //Update the pipeline only when the hold signal is not asserted.
    else if (!hold) begin
      out_scr_x <= in_scr_x;
      out_scr_y <= in_scr_y;
      out_wr_en <= in_wr_en;
    end
  end
endmodule
