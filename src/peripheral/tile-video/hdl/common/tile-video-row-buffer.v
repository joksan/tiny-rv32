//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - Row buffer module.          |
//|                                                                                                |
//| This is a generic implementation that depends on the ram-16r-16w module. Note that the output  |
//| DAC resolution is 12 bits but the ram-16r-16w module is 16 bits wide. Also, the configured     |
//| length for the ram-16r-16w module is 1024 words, of which only 640 words (two lines of 320     |
//| pixels each) are used. This means that 4 data bits and 384 words are unused (if compacted this |
//| could easily fit in little less than half the space), therefore this implementation should     |
//| only be used in devices where this space loss is tolerated or cannot be avoided due to BRAM    |
//| size/granularity.                                                                              |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_row_buffer (
  //System and common signals.
  input clk,
  input reset,
  output hold_req,    //Hold request (asserted when a pixel write takes more than one clock cycle)

  //Input signals that control row buffer output.
  input pixel_cken,   //Enables the output to be updated (last data is retained when inactive)
  input pixel_oen,    //Enables next pixel to be output (output is forced to 0 when inactive)
  input prow_start,   //Physical row is about to start (resets the output address counter to 0)
  input lrow_odd,     //Logical row odd (selects which row of the buffer is output, 0 means even)

  //Pipeline input signals from tile_video_palette (see respective module for port descriptions).
  input wr_en,
  input [8:0] scr_x,
  input [0:0] scr_y,
  input [3:0] in_red,
  input [3:0] in_green,
  input [3:0] in_blue,

  //Video output signals.
  output reg [3:0] out_red,     //Output pixel (RGB) data
  output reg [3:0] out_green,
  output reg [3:0] out_blue
);
  //Signals related to row buffer memory.
  wire [9:0] mem_rd_addr;           //Address to read from row buffer memory
  wire [9:0] mem_wr_addr;           //Address to write to row buffer memory
  wire [15:0] mem_rd_data;          //Data read from row buffer memory
  wire [15:0] mem_wr_data;          //Data written to row buffer memory

  //Signals related to the output portion of the row buffer.
  reg [8:0] out_addr_counter;       //Output address counter (used in conjunction with the fraction)

  //Row buffer memory.
  ram_16r_16w #(
    .memory_size(1024),
    .addr_width($clog2(1024))
  )
  row_buffer_memory (
    .clk(clk),
    .hold(1'd0),
    .wr_en(wr_en),
    .rd_addr(mem_rd_addr),
    .wr_addr(mem_wr_addr),
    .rd_data(mem_rd_data),
    .wr_data(mem_wr_data)
  );

  //Data output block.
  always @(posedge clk) begin
    if (reset) begin
      out_addr_counter <= 9'd0;
      out_red <= 4'd0;
      out_green <= 4'd0;
      out_blue <= 4'd0;
    end
    else begin
      //Update the following registers every pixel clock cycle.
      if (pixel_cken) begin
        //Default output value is 0 so output signals remain dark/low during blanking intervals.
        out_red <= 4'd0;
        out_green <= 4'd0;
        out_blue <= 4'd0;

        //Update the following registers during the active state of the video signal.
        if (pixel_oen) begin
          //Increment the output address counter.
          out_addr_counter <= out_addr_counter + 1;

          //Pass all color components from memory to the outputs.
          out_red <= mem_rd_data[11:8];
          out_green <= mem_rd_data[7:4];
          out_blue <= mem_rd_data[3:0];
        end
      end

      //Reset the memory address counter if a new physical row is about to start. This overrides the
      //increment logic from above.
      if (prow_start) begin
        out_addr_counter <= 9'd0;
      end
    end
  end

  //The final memory read address is composed by using the odd-numbered logical row indicator as the
  //MSB, effectively implementing a ping-pong buffer with each half being used on alternating rows.
  assign mem_rd_addr = { lrow_odd, out_addr_counter };

  //Hold request is not used in this implementation. All writes complete in one clock cycle.
  assign hold_req = 1'b0;

  //The MSB of the memory write address is taken directly from the LSB of the row write address (Y
  //coordinate), effectively implementing a ping-pong buffer with each half being used on
  //alternating rows. The lower bits are taken from the column write address (X coordinate).
  assign mem_wr_addr = { scr_y, scr_x };

  //The memory write data is just all the input color components combined.
  assign mem_wr_data = { 4'd0, in_red, in_green, in_blue };
endmodule
