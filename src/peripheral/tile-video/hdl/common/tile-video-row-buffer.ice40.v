//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - Row buffer module.          |
//|                                                                                                |
//| This is a generic implementation that is particularly aimed at iCE40 series FPGAs and uses 2   |
//| BRAMs. Other device series might be able to implement this as no vendor-specific extensions    |
//| are used, but will only be efficient if a suitable BRAM primitive is available that provides   |
//| multiple write-enable signals for individual nibbles.                                          |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_row_buffer (
  //System and common signals.
  input clk,
  input reset,
  output hold_req,    //Hold request (asserted when a pixel write takes more than one clock cycle)

  //Input signals that control row buffer output.
  input pixel_cken,   //Enables the output to be updated (last data is retained when inactive)
  input pixel_oen,    //Enables next pixel to be output (output is forced to 0 when inactive)
  input prow_start,   //Physical row is about to start (resets the output address counter to 0)
  input lrow_odd,     //Logical row odd (selects which row of the buffer is output, 0 means even)

  //Pipeline input signals from tile_video_palette (see respective module for port descriptions).
  input wr_en,
  input [8:0] scr_x,
  input [0:0] scr_y,
  input [3:0] in_red,
  input [3:0] in_green,
  input [3:0] in_blue,

  //Video output signals.
  output reg [3:0] out_red,     //Output pixel (RGB) data
  output reg [3:0] out_green,
  output reg [3:0] out_blue
);
  //Definitions used to access nibbles in the memory. Avoids number duplication errors.
  `define nibble3 15:12
  `define nibble2 11:8
  `define nibble1 7:4
  `define nibble0 3:0

  //Generation variable used to instantiate 4-lane memory write ports.
  genvar i;

  //Signals related to row buffer memory.
  reg [15:0] memory [0:511];        //Row buffer memory
  reg [3:0] mem_wr_en;              //Memory write enable for each individual nibble lane
  wire [8:0] mem_rd_addr;           //Address to read from row buffer memory
  wire [8:0] mem_wr_addr;           //Address to write to row buffer memory
  reg [15:0] mem_rd_data;           //Data read from row buffer memory
  reg [15:0] mem_wr_data;           //Data written to row buffer memory

  //Signals related to the output portion of the row buffer.
  reg [1:0] out_fraction_counter;   //Address fraction counter (used to unpack nibbles from memory)
  reg [7:0] out_addr_counter;       //Output address counter (used in conjunction with the fraction)

  reg [3:0] out_red_next;           //Temporary buffers used to hold partial data for the next pixel
  reg [3:0] out_green_next;
  reg [3:0] out_blue_next;

  //Signals related to the input portion of the row buffer.
  reg [10:0] in_pix_com_addr;       //Input pixel component address (has nibble granularity)
  reg in_wr_en;                     //Input write enable (valid after pixel address calculation)
  reg in_fifo_ready;                //Input fifo is ready to accept more data

  reg [3:0] in_red_next;            //Temporary buffers used to hold partial data for the next pixel
  reg [3:0] in_green_next;
  reg [3:0] in_blue_next;

  //Read port block for row buffer memory.
  always @(posedge clk) begin
    //Memory data is read every clock cycle regardless of other timing signals. This is ok, as the
    //address is updated every pixel cycle (4 system clock cycles) and data will be ready 3 cycles
    //before than required.
    mem_rd_data <= memory[mem_rd_addr];
  end

  //Write port block for row buffer memory with 4-lane write enable. Each lane is 4 bits wide.
  generate
    for (i = 0; i < 4; i = i + 1) begin
      always @(posedge clk) begin
        if (mem_wr_en[i])
          memory[mem_wr_addr][i*4+3:i*4] <= mem_wr_data[i*4+3:i*4];
      end
    end
  endgenerate

  //Data output block.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      out_fraction_counter <= 2'd0;
      out_addr_counter <= 8'd0;
      out_red_next <= 4'd0;
      out_green_next <= 4'd0;
      out_blue_next <= 4'd0;
      out_red <= 4'd0;
      out_green <= 4'd0;
      out_blue <= 4'd0;
    end
    else begin
      //Update the following registers every pixel clock cycle.
      if (pixel_cken) begin
        //Default output value is 0 so output signals remain dark/low during blanking intervals.
        out_red <= 4'd0;
        out_green <= 4'd0;
        out_blue <= 4'd0;

        //Update the following registers during the active state of the video signal.
        if (pixel_oen) begin
          //Increment the output address fraction counter.
          out_fraction_counter <= out_fraction_counter + 1;

          //Unpack the red, green and blue components of the video data. This is done in such a way
          //that 4 pixels are unpacked from each 3 memory words.
          case (out_fraction_counter)
            2'd0: begin
              //Pixel is aligned with the lower 3 nibbles. Take the 3 components directly from
              //memory.
              out_red <= mem_rd_data[`nibble2];
              out_green <= mem_rd_data[`nibble1];
              out_blue <= mem_rd_data[`nibble0];

              //Store the remaining blue component for the next pixel. Set the other components to
              //"X" to improve multiplexer optimization.
              out_red_next <= 4'hX;
              out_green_next <= 4'hX;
              out_blue_next <= mem_rd_data[`nibble3];

              //Increment the output address counter.
              out_addr_counter <= out_addr_counter + 1;
            end
            2'd1: begin
              //Pixel is not aligned. Take the red and green components from the lower nibbles of
              //memory. Complete the blue component with the excess nibble from last cycle.
              out_red <= mem_rd_data[`nibble1];
              out_green <= mem_rd_data[`nibble0];
              out_blue <= out_blue_next;

              //Store the remaining 2 nibbles for the next cycle.
              out_red_next <= 4'hX;
              out_green_next <= mem_rd_data[`nibble3];
              out_blue_next <= mem_rd_data[`nibble2];

              //Increment the output address counter.
              out_addr_counter <= out_addr_counter + 1;
            end
            2'd2: begin
              //Pixel is not aligned. Take the red component from the low nibble of memory. Complete
              //the green and blue components with the excess nibbles from last cycle.
              out_red <= mem_rd_data[`nibble0];
              out_green <= out_green_next;
              out_blue <= out_blue_next;

              //This time a full pixel remains. Store it for the next cycle.
              out_red_next <= mem_rd_data[`nibble3];
              out_green_next <= mem_rd_data[`nibble2];
              out_blue_next <= mem_rd_data[`nibble1];

              //Note: Ouput address counter increment is skipped, as a full pixel is in store.
            end
            2'd3: begin
              //Take all components from the excess nibbles from last cycle.
              out_red <= out_red_next;
              out_green <= out_green_next;
              out_blue <= out_blue_next;

              //No nibbles remain this time as a new location was not read. Optimize them with "X".
              out_red_next <= 4'hX;
              out_green_next <= 4'hX;
              out_blue_next <= 4'hX;

              //Increment the output address counter.
              out_addr_counter <= out_addr_counter + 1;
            end
          endcase
        end
      end

      //Reset the fraction and output address counters if a new physical row is about to start. This
      //overrides the increment logic from above.
      if (prow_start) begin
        out_fraction_counter <= 2'd0;
        out_addr_counter <= 8'd0;
      end
    end
  end

  //The final memory read address is composed by using the odd-numbered logical row indicator as the
  //MSB, effectively implementing a ping-pong buffer with each half being used on alternating rows.
  assign mem_rd_addr = { lrow_odd, out_addr_counter };

  //Data input block.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      in_pix_com_addr <= 11'd0;
      in_wr_en <= 1'b0;
      in_fifo_ready <= 1'b0;
      in_red_next <= 4'd0;
      in_green_next <= 4'd0;
      in_blue_next <= 4'd0;
    end
    else begin
      //The input of the fifo can take 2 clock cycles at most to accept a pixel. Set the ready
      //signal on the cycle after the hold request is asserted (also works for clearing).
      in_fifo_ready <= hold_req;

      //Write enable is passed on to the next cycle as long as the fifo is not busy. Else it's state
      //is retained so that the operation can complete on the next cycle.
      if (!hold_req)
        in_wr_en <= wr_en;

      //Translate the following signals to their registers whenever a write is requested, except for
      //when the fifo is busy.
      if (wr_en && !hold_req) begin
        //The lower bits of the input pixel component address are calculated by multiplying the
        //column write address (X coordinate) by 3. This address has nibble granularity and allows
        //to pack pixel data, allowing for efficient memory usage.
        in_pix_com_addr[9:0] <= scr_x + { scr_x, 1'd0 };

        //The MSB of the input pixel component address is taken directly from the LSB of the row
        //write address (Y coordinate), effectively implementing a ping-pong buffer with each half
        //being used on alternating rows.
        in_pix_com_addr[10] <= scr_y[0];

        //Pixel component data is simply passed on to the next cycle.
        in_red_next <= in_red;
        in_green_next <= in_green;
        in_blue_next <= in_blue;
      end

      //If the fifo became busy, increment the input pixel component address so that the next pixel
      //portion is written on the next cycle. The address is incremented in a way that the lower
      //bits (which indicate the nibble) don't change.
      if (hold_req)
        in_pix_com_addr[9:2] <= in_pix_com_addr[9:2] + 1;
    end
  end

  //The hold request signal is asserted when trying to perform a write in such a way that the pixel
  //spans 2 memory locations. Force the hold request signal to clear once the fifo is ready.
  assign hold_req = in_wr_en &&
                    ((in_pix_com_addr[1:0] == 2'd2) || (in_pix_com_addr[1:0] == 2'd3)) &&
                    !in_fifo_ready;

  //The memory write address comprises the MSBs of the input pixel component address, leaving out
  //the LSBs which indicate the starting nibble of the pixel.
  assign mem_wr_addr = in_pix_com_addr[10:2];

  //Combinatorial block used to multiplex memory write data and control write enable lanes.
  always @* begin
    //The default value of the memory write data is set to "X" to improve multiplexer optimization.
    mem_wr_data[15:0] = 16'hXXXX;

    //All memory write enable lanes are cleared by default to avoid writing.
    mem_wr_en = 4'd0;

    //Update the signals based on the starting nibble (lower bits of the input pixel component
    //address) when the input write enable signal is set.
    if (in_wr_en) begin
      case (in_pix_com_addr[1:0])
        2'd0: begin
          //Written pixel fits in the lower nibbles of the memory word.
          mem_wr_data = { 4'bXXXX, in_red_next, in_green_next, in_blue_next };
          mem_wr_en = 4'b0111;
        end
        2'd1: begin
          //Written pixel fits in the higher nibbles of the memory word.
          mem_wr_data = { in_red_next, in_green_next, in_blue_next, 4'bXXXX };
          mem_wr_en = 4'b1110;
        end
        2'd2: begin
          //Written pixel is split in 2 nibbles on the first cycle (signaled by hold request), then 1
          //nibble on the next.
          if (hold_req) begin
            mem_wr_data = { in_green_next, in_blue_next, 8'bXXXXXXXX };
            mem_wr_en = 4'b1100;
          end
          else begin
            mem_wr_data = { 12'bXXXXXXXXXXXX, in_red_next };
            mem_wr_en = 4'b0001;
          end
        end
        2'd3: begin
          //Written pixel is split in 1 nibble on the first cycle, then 2 nibbles on the next.
          if (hold_req) begin
            mem_wr_data = { in_blue_next, 12'bXXXXXXXXXXXX };
            mem_wr_en = 4'b1000;
          end
          else begin
            mem_wr_data = { 8'bXXXXXXXX, in_red_next, in_green_next };
            mem_wr_en = 4'b0011;
          end
        end
      endcase
    end
  end
endmodule
