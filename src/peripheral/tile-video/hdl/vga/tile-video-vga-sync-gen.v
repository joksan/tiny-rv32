//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - VGA synchronization         |
//| generator module.                                                                              |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_vga_sync_gen #(
  parameter clk_freq = 50000000
)
(
  //System signals.
  input clk,
  input reset,

  //Control signals for tile_video_row_buffer.
  output reg pixel_cken,  //Pixel clock enable (outgoing RGB data is latched on next clock)
  output reg pixel_oen,   //Pixel output enable (set during the active state of the VGA signal)
  output reg prow_start,  //Physical row is about to start
  output reg lrow_odd,    //An odd-numbered logical row is beign drawn

  //Control signals for tile_video_compositor.
  output reg lrow_prep,   //Logical row needs to be prepared

  //CPU notification signals.
  output reg frame_end,   //Frame ends after this pixel

  //Video output signals.
  output reg h_sync,      //Horizontal sync output
  output reg v_sync       //Vertical sync output
);
  //Note: Although the output resolution is 320x240 (logical resolution), the actual monitor
  //resolution is 640x480 (physical resolution). Each logical pixel is doubled in the vertical and
  //horizontal direction. Pixel frequency for the physical resolution at a framerate of 60Hz is
  //approximately 25MHz, and because of pixel doubling, the required logical pixel frequency is
  //12.5MHz.

  //States for both the horizontal and vertical synchronization state machines.
  localparam [1:0] st_front_porch = 2'd0;
  localparam [1:0] st_sync        = 2'd1;
  localparam [1:0] st_back_porch  = 2'd2;
  localparam [1:0] st_active      = 2'd3;
  reg [1:0] state_h;
  reg [1:0] state_v;

  //Sub pixel clock counter. Updates every system clock cycle. Size depends on clock frequency. When
  //operating at 50MHz, it's length is 2 bits so pixel clock is divided by 4. When operating at
  //25MHz, it's length is 1 bit so pixel clock is divided by 2. This way pixel frequency is kept at
  //12.5MHz.
  localparam sub_pixel_counter_len = (clk_freq == 50000000)? 2: 1;
  reg [sub_pixel_counter_len-1:0] sub_pixel_counter;

  //Logical column counter (range 0-319). Updates every pixel cycle or 4 system clock cycles.
  reg [8:0] lcol_counter;

  //Physical row counter (range 0-479). Updates every row cycle or 400 pixel cycles.
  reg [8:0] prow_counter;

  //This register is set while the logical column counter is on the last column (319). This gets
  //set even during blanking periods. The set state lasts for 1 pixel cycle.
  reg last_lcol_in_prow;

  //This register is set while the physical row counter is on the last physical row position (479).
  //The set state lasts for 1 row cycle.
  reg last_prow_in_frame;

  //This register is set while the physical row counter is on the 2 last logical row positions (238
  //and 239 or physical 476 through 479). The set state lasts for 4 row cycles.
  reg last_2lrow_in_frame;

  //Make sure that the clock frequency is correctly configured.
  generate
    if (clk_freq != 50000000 && clk_freq != 25000000)
      invalid_parameter_value clk_freq();
  endgenerate

  //Subpixel update block. Logic is updated every system clock cycle.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      sub_pixel_counter <= 0;
      pixel_cken <= 1'b0;
    end
    else begin
      //Increment the subpixel counter and set the pixel clock enable so that it activates during
      //the last count value of the subpixel counter (3 for 50Mhz, 1 for 25MHz). Note that the
      //comparison is done on the cycle before so the count is subtracted accordingly.
      sub_pixel_counter <= sub_pixel_counter + 1;
      pixel_cken <= sub_pixel_counter == 2 ** sub_pixel_counter_len - 2;
    end
  end

  //Horizontal update block. Logic is updated every pixel cycle.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      state_h <= st_front_porch;
      lcol_counter <= 9'd0;
      last_lcol_in_prow <= 1'b0;
      pixel_oen <= 1'b0;
      lrow_odd <= 1'b0;
      h_sync <= 1'b1;             //Horizontal sync is active low and disabled on reset
    end
    else if (pixel_cken) begin
      //Default values for registers.
      h_sync <= 1'b1;   //Horizontal sync disabled by default

      //Registers updated every pixel cycle.
      lcol_counter <= lcol_counter + 1;             //Increment the logical column counter
      last_lcol_in_prow <= lcol_counter == 9'd318;  //Set when lcol_counter is about to reach 319
      lrow_odd <= prow_counter[1];                  //Update the odd-numbered logical row indicator

      //Horizontal FSM.
      case (state_h)
        st_front_porch: begin
          //Reset the logical column counter and transition to next state after 8 pixel cycles
          //(16 physical pixels).
          if (lcol_counter[5:0] == 6'd7) begin
            lcol_counter <= 0;
            state_h <= st_sync;
          end
        end
        st_sync: begin
          //Activate horizontal sync during this state (effective on the pixel cycle after the
          //first and disabled one pixel cycle after last).
          h_sync <= 1'b0;

          //Reset counter and transition to next state after 48 pixel cycles (96 physical).
          if (lcol_counter[5:0] == 6'd47) begin
            lcol_counter <= 0;
            state_h <= st_back_porch;
          end
        end
        st_back_porch: begin
          //Reset counter and transition to next state after 24 pixel cycles (48 physical).
          if (lcol_counter[5:0] == 6'd23) begin
            //Enable the pixel output on transition if the vertical state machine is also on the
            //active state.
            if (state_v == st_active)
              pixel_oen <= 1'b1;

            lcol_counter <= 0;
            state_h <= st_active;
          end
        end
        st_active: begin
          //Reset counter and transition to next state after 320 pixel cycles (640 physical).
          if (last_lcol_in_prow) begin
            pixel_oen <= 1'b0;          //Also disable pixel output on transition (if enabled)
            lcol_counter <= 0;
            state_h <= st_front_porch;
          end
        end
      endcase
    end
  end

  //Vertical update block. Logic is updated every row cycle.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      state_v <= st_front_porch;
      prow_counter <= 8'd0;
      last_prow_in_frame <= 1'b0;
      last_2lrow_in_frame <= 1'b0;
      v_sync <= 1'b1;
    end
    else if (pixel_cken && last_lcol_in_prow) begin
      //Registers updated every row cycle.
      prow_counter <= prow_counter + 1;               //Increment the physical row counter
      last_prow_in_frame <= prow_counter == 9'd478;   //Set when prow_counter is about to reach 479

      //Set the last 2 logical row in frame indicator while prow_counter is in range 476 through 479
      //(comparisons are done 1 row cycle before so count is below by one).
      last_2lrow_in_frame <= prow_counter == 9'd475 ||
                             prow_counter == 9'd476 ||
                             prow_counter == 9'd477 ||
                             prow_counter == 9'd478;

      //Vertical FSM.
      case (state_v)
        st_front_porch: begin
          //Reset the counter and transition to next state after 10 row cycles.
          if (prow_counter[5:0] == 9) begin
            v_sync <= 1'b0;       //Also activate vertical sync on transition
            prow_counter <= 0;
            state_v <= st_sync;
          end
        end
        st_sync: begin
          //Reset the counter and transition to next state after 2 row cycles.
          if (prow_counter[5:0] == 1) begin
            v_sync <= 1'b1;             //Also deactivate vertical sync on transition
            prow_counter <= 0;
            state_v <= st_back_porch;
          end
        end
        st_back_porch: begin
          //Reset the counter and transition to next state after 33 row cycles.
          if (prow_counter[5:0] == 32) begin
            prow_counter <= 0;
            state_v <= st_active;
          end
        end
        st_active: begin
          //Reset the counter and transition to next state after 480 row cycles.
          if (last_prow_in_frame) begin
            prow_counter <= 0;
            state_v <= st_front_porch;
          end
        end
      endcase
    end
  end

  //Control and notification signal block. Generated signals are set at the end of the pixel cycle
  //of the last pixel in a row, lasting a single system clock cycle.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      lrow_prep <= 1'b0;
      prow_start <= 1'b0;
      frame_end <= 1'b0;
    end
    else begin
      //Default values for registers.
      lrow_prep <= 1'b0;
      prow_start <= 1'b0;
      frame_end <= 1'b0;

      if (sub_pixel_counter == (2 ** sub_pixel_counter_len - 2) && last_lcol_in_prow) begin
        //The logical row preparation control signal is asserted at the end of rows 30 and 32 of the
        //vertical back porch to signal the start of the preparation of the row buffer.
        if (state_v == st_back_porch)
          lrow_prep <= prow_counter[4:0] == 5'd30 || prow_counter[5];

        //The logical row preparation control signal is also asserted at the end of each physical
        //odd row during the vertical active portion of the video signal, except for the last 2
        //logical (4 physical) rows.
        if (state_v == st_active)
          lrow_prep <= prow_counter[0] && !last_2lrow_in_frame;

        //The physical row start control signal is asserted at the end of the last physical row (32)
        //of the vertical back porch to signal the first row in the frame.
        if (state_v == st_back_porch)
          prow_start <= prow_counter[5] ;

        //The physical row start control signal is also asserted at the end of each physical row
        //during the vertical active state, except for the last row in the frame.
        if (state_v == st_active)
          prow_start <= !last_prow_in_frame;

        //The frame end notification is asserted at the end of the last physical row in the frame.
        frame_end <= last_prow_in_frame;
      end
    end
  end
endmodule
