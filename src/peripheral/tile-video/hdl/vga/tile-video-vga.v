//+------------------------------------------------------------------------------------------------+
//| Tile-based video generator for the tiny-rv32e softcore processor - Main module for VGA output. |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module tile_video_vga #(
  parameter clk_freq = 50000000,
  parameter tilemap_init_file = "",
  parameter patterns_1bpp = 0,
  parameter patterns_2bpp = 0,
  parameter patterns_4bpp = 0,
  parameter pattern_init_file_1bpp = "",
  parameter pattern_init_file_2bpp = "",
  parameter pattern_init_file_4bpp = "",
  parameter palette_init_file = ""
)
(
  //System signals.
  input clk,
  input reset,

  //Processor bus signals.
  input [3:0] wr_en,
  input [31:2] addr,
  input [31:0] wr_data,

  //Video output signals.
  output [3:0] red,
  output [3:0] green,
  output [3:0] blue,
  output h_sync,
  output v_sync
);
  //Common signals shared between submodules.
  wire hold;

  //Signals from the VGA synchronization generator module.
  wire sync_gen_pixel_cken;
  wire sync_gen_pixel_oen;
  wire sync_gen_prow_start;
  wire sync_gen_lrow_odd;
  wire sync_gen_lrow_prep;

  //Signals from the video compositor module.
  wire compositor_wr_en;
  wire [8:0] compositor_scr_x;
  wire [0:0] compositor_scr_y;
  wire [2:0] compositor_pat_x;
  wire [2:0] compositor_pat_y;
  wire [7:0] compositor_palette_sel;
  wire [7:0] compositor_pattern_num;

  //Signals from the pattern generator module.
  wire pattern_out_wr_en;
  wire [0:0] pattern_out_scr_y;
  wire [8:0] pattern_out_scr_x;
  wire [7:0] pattern_out_palette_sel;
  wire pattern_color_1bpp;
  wire [1:0] pattern_color_2bpp;
  wire [3:0] pattern_color_4bpp;

  //Signals for the palette memory module.
  wire palette_out_wr_en;
  wire [0:0] palette_out_scr_y;
  wire [8:0] palette_out_scr_x;
  wire [3:0] palette_red;
  wire [3:0] palette_green;
  wire [3:0] palette_blue;

  //VGA synchronization generator module instance.
  tile_video_vga_sync_gen  #(
    .clk_freq(clk_freq)
  )
  sync_gen (
    //System signals.
    .clk(clk),
    .reset(reset),

    //Control signals for tile_video_row_buffer.
    .pixel_cken(sync_gen_pixel_cken),
    .pixel_oen(sync_gen_pixel_oen),
    .prow_start(sync_gen_prow_start),
    .lrow_odd(sync_gen_lrow_odd),

    //Control signals for tile_video_compositor.
    .lrow_prep(sync_gen_lrow_prep),

    //CPU notification signals.
    .frame_end(),   //Intended for vertical blanking interrupts (unused for now)

    //Video output signals.
    .h_sync(h_sync),
    .v_sync(v_sync)
  );

  //Video compositor module instance.
  tile_video_compositor #(
    .init_file(tilemap_init_file)
  )
  compositor (
    //System and common signals.
    .clk(clk),
    .reset(reset),
    .hold(hold),

    //Processor bus signals.
    .cpu_wr_en(wr_en),
    .cpu_addr(addr),
    .cpu_wr_data(wr_data),

    //Input control signals.
    .lrow_prep(sync_gen_lrow_prep),

    //Pipeline output signals.
    .wr_en(compositor_wr_en),
    .scr_x(compositor_scr_x),
    .scr_y(compositor_scr_y),
    .pat_x(compositor_pat_x),
    .pat_y(compositor_pat_y),
    .palette_sel(compositor_palette_sel),
    .pattern_num(compositor_pattern_num)
  );

  //Pattern generator module instance.
  tile_video_pattern #(
    .patterns_1bpp(patterns_1bpp),
    .patterns_2bpp(patterns_2bpp),
    .patterns_4bpp(patterns_4bpp),
    .init_file_1bpp(pattern_init_file_1bpp),
    .init_file_2bpp(pattern_init_file_2bpp),
    .init_file_4bpp(pattern_init_file_4bpp)
  )
  pattern (
    //System and common signals.
    .clk (clk),
    .reset(reset),
    .hold(hold),

    //Processor bus signals.
    .cpu_wr_en(wr_en),
    .cpu_addr(addr),
    .cpu_wr_data(wr_data),

    //Pipeline input signals from tile_video_compositor.
    .in_wr_en(compositor_wr_en),
    .in_scr_x(compositor_scr_x),
    .in_scr_y(compositor_scr_y),
    .pat_x(compositor_pat_x),
    .pat_y(compositor_pat_y),
    .in_palette_sel(compositor_palette_sel),
    .pattern_num(compositor_pattern_num),

    //Pipeline output signals.
    .out_wr_en(pattern_out_wr_en),
    .out_scr_x(pattern_out_scr_x),
    .out_scr_y(pattern_out_scr_y),
    .out_palette_sel(pattern_out_palette_sel),
    .color_1bpp(pattern_color_1bpp),
    .color_2bpp(pattern_color_2bpp),
    .color_4bpp(pattern_color_4bpp)
  );

  //Palette memory module instance.
  tile_video_palette #(
    .init_file(palette_init_file)
  )
  palette (
    //System and common signals.
    .clk(clk),
    .reset(reset),
    .hold(hold),

    //Processor bus signals.
    .cpu_wr_en(wr_en),
    .cpu_addr(addr),
    .cpu_wr_data(wr_data),

    //Pipeline input signals from tile_video_pattern.
    .in_wr_en(pattern_out_wr_en),
    .in_scr_x(pattern_out_scr_x),
    .in_scr_y(pattern_out_scr_y),
    .palette_sel(pattern_out_palette_sel),
    .color_1bpp(pattern_color_1bpp),
    .color_2bpp(pattern_color_2bpp),
    .color_4bpp(pattern_color_4bpp),

    //Pipeline output signals.
    .out_wr_en(palette_out_wr_en),
    .out_scr_x(palette_out_scr_x),
    .out_scr_y(palette_out_scr_y),
    .red(palette_red),
    .green(palette_green),
    .blue(palette_blue)
  );

  //Row buffer module instance.
  tile_video_row_buffer row_buffer (
    //System and common signals.
    .clk(clk),
    .reset(reset),
    .hold_req(hold),

    //Input signals that control row buffer output.
    .pixel_cken(sync_gen_pixel_cken),
    .pixel_oen(sync_gen_pixel_oen),
    .prow_start(sync_gen_prow_start),
    .lrow_odd(sync_gen_lrow_odd),

    //Pipeline input signals from tile_video_palette.
    .wr_en(palette_out_wr_en),
    .scr_x(palette_out_scr_x),
    .scr_y(palette_out_scr_y),
    .in_red(palette_red),
    .in_green(palette_green),
    .in_blue(palette_blue),

    //Video output signals.
    .out_red(red),
    .out_green(green),
    .out_blue(blue)
  );
endmodule
