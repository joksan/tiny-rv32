//+------------------------------------------------------------------------------------------------+
//| RAM module with a 16-bit read port and a 16-bit write port.                                    |
//|                                                                                                |
//| Spartan-6 version for use with Xilinx ISE.                                                     |
//|                                                                                                |
//| This module is intended to work with the bmm-gen.py script for initialization. See the script  |
//| source for details.                                                                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module ram_16r_16w #(
  parameter init_file = "",
  parameter memory_size = 0,
  parameter addr_width = 0
)
(
  input clk,
  input hold,
  input wr_en,
  input [addr_width-1:0] rd_addr,
  input [addr_width-1:0] wr_addr,
  output [15:0] rd_data,
  input [15:0] wr_data
);
  genvar i;

  generate
    if (memory_size <= 512) begin
      //Length is 512 (or less) 16-bit words. Use a single RAMB8BWER instance. Both ports are used
      //for separated read and write operations. A RAMB8BWER instance provides 1024 bytes of memory
      //(parity is unused) and shows up as a single 512 x 16 memory.
      for (i = 0; i < 1; i = i + 1) begin: bram_inst
        RAMB8BWER #(
          .DATA_WIDTH_A(18),            //Set data width to 18 bits (16-bit data, 2-bit parity)
          .DATA_WIDTH_B(18),
          .DOA_REG(0),                  //Disable optional output register
          .DOB_REG(0),
          .EN_RSTRAM_A("FALSE"),        //Disable reset (not used)
          .EN_RSTRAM_B("FALSE"),
          .INIT_A(18'h00000),           //Clear port output registers at startup
          .INIT_B(18'h00000),
          .RAM_MODE("TDP"),             //Operate in true dual port mode
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")   //(concurrent read/write is not used anyway)
        )
        bram (
          .ADDRAWRADDR({ rd_addr, 4'd0 }),  //Port A handles read operations
          .DOADO(rd_data),
          .DIADI(16'd0),
          .WEAWEL(2'd0),
          .ENAWREN(!hold),
          .ADDRBRDADDR({ wr_addr, 4'd0 }),  //Port B handles write operations
          .DOBDO(),
          .DIBDI(wr_data),
          .WEBWEU({ 2 { wr_en } }),
          .ENBRDEN(wr_en),
          .CLKAWRCLK(clk),              //Both ports share the same clock
          .CLKBRDCLK(clk),
          .DIPADIP(2'd0),               //Parity unused
          .DIPBDIP(2'd0),
          .DOPADOP(), .DOPBDOP(),
          .REGCEA(1'b0),                //Optional output register unused/disabled
          .REGCEBREGCE(1'b0),
          .RSTA(1'b0), .RSTBRST(1'b0)   //Set/reset unused
        );
      end
    end
    else if (memory_size <= 16384) begin
      //Length is 16384 (or less) 16-bit words. Use from 1 to 16 RAMB16BWER instances. All instances
      //combined show up as a single [1024~16384] x 16 memory.
      parameter addr_bits = $clog2(memory_size);
      parameter data_bits = 16384 / 2 ** addr_bits;
      parameter num_bram = 2 ** addr_bits / 1024;
      parameter bram_width = (data_bits == 16)? 18:
                             (data_bits == 8)? 9: data_bits;
      for (i = 0; i < num_bram; i = i + 1) begin: bram_inst
        RAMB16BWER #(
          .DATA_WIDTH_A(bram_width),    //Set data width to calculated width
          .DATA_WIDTH_B(bram_width),
          .DOA_REG(0),                  //Disable optional output register
          .DOB_REG(0),
          .EN_RSTRAM_A("FALSE"),        //Disable reset (not used)
          .EN_RSTRAM_B("FALSE"),
          .INIT_A(36'h000000000),       //Clear port output registers at startup
          .INIT_B(36'h000000000),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA({ rd_addr[addr_bits-1:0], { 14 - addr_bits { 1'b0 } } }),  //Port A handles reads
          .DOA(rd_data[(i + 1) * data_bits - 1: i * data_bits]),
          .DIA(32'd0),
          .WEA(4'h0),
          .ENA(!hold),
          .ADDRB({ wr_addr[addr_bits-1:0], { 14 - addr_bits { 1'b0 } } }),  //Port B handles writes
          .DOB(),
          .DIB(wr_data[(i + 1) * data_bits - 1: i * data_bits]),
          .WEB({ 4 { wr_en } }),
          .ENB(wr_en),
          .CLKA(clk), .CLKB(clk),       //Both ports share the same clock
          .DIPA(4'h0), .DIPB(4'h0),     //Parity unused
          .DOPA(), .DOPB(),
          .REGCEA(1'b0),                //Optional output register unused/disabled
          .REGCEB(1'b0),
          .RSTA(1'b0), .RSTB(1'b0)      //Set/reset unused
        );
      end
    end
    else begin
      invalid_parameter_value length();
    end
  endgenerate
endmodule
