//+------------------------------------------------------------------------------------------------+
//| RAM module with a 16-bit read port and a 16-bit write port.                                    |
//|                                                                                                |
//| This is a generic version intended for simulation with Icarus or synthesis for iCE40 FPGAs     |
//| with Yosys and IceStorm. Although other device series might be able to implement this since no |
//| vendor-specific extensions are used, quick updates of BRAM contents may not be possible due to |
//| particular toolchain requirements such as instantiating device-specific BRAM primitives.       |
//|                                                                                                |
//| The main reason behind the 16-bit width and separate read/write ports is universality between  |
//| FPGA devices (this feature set is commonly availabe in all supported devices) and the ability  |
//| to use just one BRAM per instance (the iCE40 series for example has a maximum port width of 16 |
//| bits per BRAM port - if 32-bit width were supported then 2 BRAMs would need to be used at a    |
//| minimum for width expansion). This is of course of no concern in larger devices with improved  |
//| features, but helps to save resources in the smaller ones.                                     |
//|                                                                                                |
//| The limited bus width and the separate read/write ports have the unfortunate side effect when  |
//| when mapping this module to a CPU, that operations are limited to be read-only or write-only,  |
//| and accesses are limited to be of exactly 2 bytes at a time (not 1 or 4), meaning that the     |
//| application scope of this module may be limited for the sake of compatibility and optimum      |
//| resource usage (though it's still enough for the scope of this project).                       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module ram_16r_16w #(
  parameter init_file = "",
  parameter memory_size = 0,
  parameter addr_width = 0
)
(
  input clk,
  input hold,
  input wr_en,
  input [addr_width-1:0] rd_addr,
  input [addr_width-1:0] wr_addr,
  output reg [15:0] rd_data,
  input [15:0] wr_data
);
  reg [15:0] memory [0:memory_size-1];  //Memory contents

  //Memory initialization block.
  initial begin
    if (init_file != "") begin
      //Read from the initialization file if defined.
      $readmemh(init_file, memory);
    end
    else begin: memory_init
      //Else set every location to 0.
      integer i;
      for (i = 0; i < memory_size; i = i + 1)
        memory[i] <= 16'h0000;
    end
  end

  //Read port block for memory.
  always @(posedge clk)
    //Read the memory whenever the hold signal is not asserted.
    if (!hold)
      rd_data <= memory[rd_addr];

  //Write port block for memory. Only 16-bit writes are supported.
  always @(posedge clk)
    if (wr_en)
      memory[wr_addr] <= wr_data;
endmodule
