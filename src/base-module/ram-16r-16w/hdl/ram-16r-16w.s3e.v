//+------------------------------------------------------------------------------------------------+
//| RAM module with a 16-bit read port and a 16-bit write port.                                    |
//|                                                                                                |
//| Spartan-3E version for use with Xilinx ISE.                                                    |
//|                                                                                                |
//| This module is intended to work with the bmm-gen.py script for initialization. See the script  |
//| source for details.                                                                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module ram_16r_16w #(
  parameter init_file = "",
  parameter memory_size = 0,
  parameter addr_width = 0
)
(
  input clk,
  input hold,
  input wr_en,
  input [addr_width-1:0] rd_addr,
  input [addr_width-1:0] wr_addr,
  output [15:0] rd_data,
  input [15:0] wr_data
);
  genvar i;

  generate
    if (memory_size <= 1024) begin
      //Length is 1024 (or less) 16-bit words. Use a single RAMB16_S18_S18 (dual port) instance.
      //Both ports are used for separated read and write operations. A RAMB16_S18_S18 instance
      //provides 2048 bytes of memory (parity is unused) and shows up as a single 1024 x 16 memory.
      for (i = 0; i < 1; i = i + 1) begin: bram_inst
        RAMB16_S18_S18 #(
          .INIT_A(18'h00000),           //Clear port output registers at startup
          .INIT_B(18'h00000),
          .SRVAL_A(18'h00000),          //Clear output values when SSR is asserted
          .SRVAL_B(18'h00000),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")   //(concurrent read/write is not used anyway)
        )
        bram (
          .ADDRA(rd_addr),              //Port A handles read operations
          .DOA(rd_data),
          .DIA(16'd0),
          .WEA(1'd0),
          .ENA(!hold),
          .ADDRB(wr_addr),              //Port B handles write operations
          .DOB(),
          .DIB(wr_data),
          .WEB(wr_en),
          .ENB(wr_en),
          .CLKA(clk), .CLKB(clk),       //Both ports share the same clock
          .DIPA(2'd0), .DIPB(2'd0),     //Parity unused
          .DOPA(), .DOPB(),
          .SSRA(1'd0), .SSRB(1'd0)      //Synchronous set/reset unused
        );
      end
    end
    else if (memory_size <= 2048) begin
      //Length is 2048 (or less) 16-bit words. Use two RAMB16_S9_S9 instances. All instances
      //combined show up as a single 2048 x 16 memory.
      for (i = 0; i < 2; i = i + 1) begin: bram_inst
        RAMB16_S9_S9 #(
          .INIT_A(9'h000),              //Clear port output registers at startup
          .INIT_B(9'h000),
          .SRVAL_A(9'h000),             //Clear output values when SSR is asserted
          .SRVAL_B(9'h000),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA(rd_addr),              //Port A handles read operations
          .DOA(rd_data[i*8+7:i*8]),
          .DIA(8'd0),
          .WEA(1'd0),
          .ENA(!hold),
          .ADDRB(wr_addr),              //Port B handles write operations
          .DOB(),
          .DIB(wr_data[i*8+7:i*8]),
          .WEB(wr_en),
          .ENB(wr_en),
          .CLKA(clk), .CLKB(clk),       //Both ports share the same clock
          .DIPA(1'd0), .DIPB(1'd0),     //Parity unused
          .DOPA(), .DOPB(),
          .SSRA(1'd0), .SSRB(1'd0)      //Synchronous set/reset unused
        );
      end
    end
    else if (memory_size <= 4096) begin
      //Length is 4096 (or less) 16-bit words. Use four RAMB16_S4_S4 instances. All instances
      //combined show up as a single 4096 x 16 memory.
      for (i = 0; i < 4; i = i + 1) begin: bram_inst
        RAMB16_S4_S4 #(
          .INIT_A(4'h0),                //Clear port output registers at startup
          .INIT_B(4'h0),
          .SRVAL_A(4'h0),               //Clear output values when SSR is asserted
          .SRVAL_B(4'h0),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA(rd_addr),              //Port A handles read operations
          .DOA(rd_data[i*4+3:i*4]),
          .DIA(4'd0),
          .WEA(1'd0),
          .ENA(!hold),
          .ADDRB(wr_addr),              //Port B handles write operations
          .DOB(),
          .DIB(wr_data[i*4+3:i*4]),
          .WEB(wr_en),
          .ENB(wr_en),
          .CLKA(clk), .CLKB(clk),       //Both ports share the same clock
          .SSRA(1'd0), .SSRB(1'd0)      //Synchronous set/reset unused
        );
      end
    end
    else if (memory_size <= 8192) begin
      //Length is 8192 (or less) 16-bit words. Use eight RAMB16_S2_S2 instances. All instances
      //combined show up as a single 8192 x 16 memory.
      for (i = 0; i < 8; i = i + 1) begin: bram_inst
        RAMB16_S2_S2 #(
          .INIT_A(2'b00),               //Clear port output registers at startup
          .INIT_B(2'b00),
          .SRVAL_A(2'b00),              //Clear output values when SSR is asserted
          .SRVAL_B(2'b00),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA(rd_addr),              //Port A handles read operations
          .DOA(rd_data[i*2+1:i*2]),
          .DIA(2'd0),
          .WEA(1'd0),
          .ENA(!hold),
          .ADDRB(wr_addr),              //Port B handles write operations
          .DOB(),
          .DIB(wr_data[i*2+1:i*2]),
          .WEB(wr_en),
          .ENB(wr_en),
          .CLKA(clk), .CLKB(clk),       //Both ports share the same clock
          .SSRA(1'd0), .SSRB(1'd0)      //Synchronous set/reset unused
        );
      end
    end
    else if (memory_size <= 16384) begin
      //Length is 16384 (or less) 16-bit words. Use sixteen RAMB16_S1_S1 instances. All instances
      //combined show up as a single 16384 x 16 memory.
      for (i = 0; i < 16; i = i + 1) begin: bram_inst
        RAMB16_S1_S1 #(
          .INIT_A(1'b0),                //Clear port output registers at startup
          .INIT_B(1'b0),
          .SRVAL_A(1'b0),               //Clear output values when SSR is asserted
          .SRVAL_B(1'b0),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA(rd_addr),              //Port A handles read operations
          .DOA(rd_data[i]),
          .DIA(1'd0),
          .WEA(1'd0),
          .ENA(!hold),
          .ADDRB(wr_addr),              //Port B handles write operations
          .DOB(),
          .DIB(wr_data[i]),
          .WEB(wr_en),
          .ENB(wr_en),
          .CLKA(clk), .CLKB(clk),       //Both ports share the same clock
          .SSRA(1'd0), .SSRB(1'd0)      //Synchronous set/reset unused
        );
      end
    end
    else begin
      invalid_parameter_value length();
    end
  endgenerate
endmodule
