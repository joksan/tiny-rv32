//+------------------------------------------------------------------------------------------------+
//| Simple reset module for the tiny-rv32 softcore processors.                                     |
//|                                                                                                |
//| This module provides basic reset functionality for the tiny-rv32 processors, particularly in   |
//| ice40 FPGAs, where the system reset must be performed in a rather specific way.                |
//|                                                                                                |
//| Logic in this module is described in a generic way and can be trivially implemented in more    |
//| robust FPGAs, allowing its use on devices other than the ice40 series.                         |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module simple_reset (
  input clk,
  output reset
);
  //Reset counter register. Amount of bits may be different between simulation and synthesis and
  //also for different clock frequencies.
  reg [`RESET_COUNTER_BITS-1:0] reset_counter = 0;

  //Note:
  //In the iCE40 FPGAs it's not possible to initialize registers to values other than 0. Since the
  //reset behavior is not fully controllable, explicit initialization is not done throughout the
  //code base except for this single place, where the reset counter register is initialized to said
  //value. This is done in order to have a single place where reliable initialization is done and
  //also to have the simulation tool to behave correctly.

  //Reset is asserted until all counter bits become 1.
  assign reset = ~&reset_counter;

  //Count for as long as the reset signal is asserted.
  always @(posedge clk) begin
    if (reset)
      reset_counter <= reset_counter + 1'd1;
  end
endmodule
