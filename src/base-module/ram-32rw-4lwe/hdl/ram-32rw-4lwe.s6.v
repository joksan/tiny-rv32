//+------------------------------------------------------------------------------------------------+
//| RAM module with a single 32-bit read/write port and 4-lane write enable.                       |
//|                                                                                                |
//| Spartan-6 version for use with Xilinx ISE.                                                     |
//|                                                                                                |
//| This module is intended to work with the bmm-gen.py script for initialization. See the script  |
//| source for details.                                                                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module ram_32rw_4lwe #(
  parameter init_file = "",
  parameter length = 1024
)
(
  input clk,
  input rd_en,
  input [3:0] wr_en,
  input [31:2] addr,
  output [31:0] rd_data,
  input [31:0] wr_data
);
  genvar i;

  generate
    if (length <= 256) begin
      //Length is 256 (or less) 32-bit words. Use a single RAMB8BWER instance. Each port is used for
      //separate read and write operations. A RAMB8BWER instance provides 1024 bytes of memory
      //(parity is unused) and shows up as a single 256 x 32 memory.
      for (i = 0; i < 1; i = i + 1) begin: bram_inst
        RAMB8BWER #(
          .DATA_WIDTH_A(36),                  //Set data width to 36 bits (32 data + 4 parity)
          .DATA_WIDTH_B(36),
          .DOA_REG(0),                        //Disable optional output register
          .DOB_REG(0),
          .EN_RSTRAM_A("FALSE"),              //Disable reset (not used)
          .EN_RSTRAM_B("FALSE"),
          .INIT_A(18'h00000),                 //Clear port output registers at startup
          .INIT_B(18'h00000),
          .RAM_MODE("SDP"),                   //Operate in simple dual-port mode
          .WRITE_MODE_A("READ_FIRST"),        //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")         //(concurrent read/write is not used anyway)
        )
        bram (
          .ADDRAWRADDR({ addr[9:2], 5'd0 }),  //Port A handles write operations
          .DIADI(wr_data[15:0]),
          .DIBDI(wr_data[31:16]),
          .WEAWEL(wr_en[1:0]),
          .WEBWEU(wr_en[3:2]),
          .ENAWREN(|wr_en),
          .ADDRBRDADDR({ addr[9:2], 5'd0 }),  //Port B handles read operations
          .DOADO(rd_data[15:0]),
          .DOBDO(rd_data[31:16]),
          .ENBRDEN(rd_en),
          .CLKAWRCLK(clk), .CLKBRDCLK(clk),   //Both ports share the same clock
          .DIPADIP(2'd0), .DIPBDIP(2'd0),     //Parity unused
          .DOPADOP(), .DOPBDOP(),
          .REGCEA(1'b0), .REGCEBREGCE(1'b0),  //Optional output register unused/disabled
          .RSTA(1'b0), .RSTBRST(1'b0)         //Set/reset unused
        );
      end
    end
    else if (length <= 512) begin
      //Length is 512 (or less) 32-bit words. Use a single RAMB16BWER instance. A RAMB16BWER
      //instance provides 2048 bytes of memory and shows up as a single 512 x 32 memory.
      for (i = 0; i < 1; i = i + 1) begin: bram_inst
        RAMB16BWER #(
          .DATA_WIDTH_A(36),                  //Set data width to 36 bits
          .DATA_WIDTH_B(36),
          .DOA_REG(0),                        //Disable optional output register
          .DOB_REG(0),
          .EN_RSTRAM_A("FALSE"),              //Disable reset (not used)
          .EN_RSTRAM_B("FALSE"),
          .INIT_A(36'h000000000),             //Clear port output registers at startup
          .INIT_B(36'h000000000),
          .WRITE_MODE_A("READ_FIRST"),        //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA({ addr[10:2], 5'd0 }),       //Port A handles write operations
          .DOA(),
          .DIA(wr_data),
          .WEA(wr_en),
          .ENA(|wr_en),
          .ADDRB({ addr[10:2], 5'd0 }),       //Port B handles read operations
          .DOB(rd_data),
          .DIB(32'd0),
          .WEB(4'd0),
          .ENB(rd_en),
          .CLKA(clk), .CLKB(clk),             //Both ports share the same clock
          .DIPA(4'd0), .DIPB(4'd0),           //Parity unused
          .DOPA(), .DOPB(),
          .REGCEA(1'b0), .REGCEB(1'b0),       //Optional output register unused/disabled
          .RSTA(1'b0), .RSTB(1'b0)            //Set/reset unused
        );
      end
    end
    else if (length <= 1024) begin
      //Length is 1024 (or less) 32-bit words. Use two RAMB16BWER instances. Each holds 2 byte
      //lanes. All instances show up as a single 1024 x 32 memory.
      for (i = 0; i < 2; i = i + 1) begin: bram_inst
        RAMB16BWER #(
          .DATA_WIDTH_A(18),                  //Set data width to 18 bits
          .DATA_WIDTH_B(18),
          .DOA_REG(0),                        //Disable optional output register
          .DOB_REG(0),
          .EN_RSTRAM_A("FALSE"),              //Disable reset (not used)
          .EN_RSTRAM_B("FALSE"),
          .INIT_A(36'h000000000),             //Clear port output registers at startup
          .INIT_B(36'h000000000),
          .WRITE_MODE_A("READ_FIRST"),        //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA({ addr[11:2], 4'd0 }),       //Port A handles write operations
          .DOA(),
          .DIA(wr_data[i*16+15:i*16]),
          .WEA(wr_en[i*2+1:i*2]),
          .ENA(|wr_en),
          .ADDRB({ addr[11:2], 4'd0 }),       //Port B handles read operations
          .DOB(rd_data[i*16+15:i*16]),
          .DIB(32'd0),
          .WEB(4'd0),
          .ENB(rd_en),
          .CLKA(clk), .CLKB(clk),             //Both ports share the same clock
          .DIPA(4'd0), .DIPB(4'd0),           //Parity unused
          .DOPA(), .DOPB(),
          .REGCEA(1'b0), .REGCEB(1'b0),       //Optional output register unused/disabled
          .RSTA(1'b0), .RSTB(1'b0)            //Set/reset unused
        );
      end
    end
    else if (length <= 16384) begin
      //Length is 16384 (or less) 32-bit words. Use from 4 to 16 RAMB16BWER instances. Each holds
      //either 8, 4, 2 or 1 bits, depending on required size. All instances combined show up as a
      //single [2048~16384] x 32 memory.
      parameter addr_bits = $clog2(length);
      parameter data_bits = 16384 / 2 ** addr_bits;
      parameter num_bram = 2 ** addr_bits / 512;
      parameter bram_width = (data_bits == 8)? 9: data_bits;
      for (i = 0; i < num_bram; i = i + 1) begin: bram_inst
        RAMB16BWER #(
          .DATA_WIDTH_A(bram_width),          //Set data width to calculated width
          .DATA_WIDTH_B(bram_width),
          .DOA_REG(0),                        //Disable optional output register
          .DOB_REG(0),
          .EN_RSTRAM_A("FALSE"),              //Disable reset (not used)
          .EN_RSTRAM_B("FALSE"),
          .INIT_A(36'h000000000),             //Clear port output registers at startup
          .INIT_B(36'h000000000),
          .WRITE_MODE_A("READ_FIRST"),        //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")
        )
        bram (
          .ADDRA({ addr[addr_bits+1:2], { 14 - addr_bits { 1'b0 } } }),   //Port A handles writes
          .DOA(),
          .DIA(wr_data[(i + 1) * data_bits - 1: i * data_bits]),
          .WEA({ 4 { wr_en[i / (8 / data_bits)] } }),                     //Set all 4 bits (bugfix)
          .ENA(|wr_en),
          .ADDRB({ addr[addr_bits+1:2], { 14 - addr_bits { 1'b0 } } }),   //Port B handles reads
          .DOB(rd_data[(i + 1) * data_bits - 1: i * data_bits]),
          .DIB(32'd0),
          .WEB(4'd0),
          .ENB(rd_en),
          .CLKA(clk), .CLKB(clk),             //Both ports share the same clock
          .DIPA(4'd0), .DIPB(4'd0),           //Parity unused
          .DOPA(), .DOPB(),
          .REGCEA(1'b0), .REGCEB(1'b0),       //Optional output register unused/disabled
          .RSTA(1'b0), .RSTB(1'b0)            //Set/reset unused
        );
        //NOTE: According to Xilinx documentation (ug383), a single byte-wide write enable signal is
        //needed for BRAM operation when data width is 1, 2, 4 or 9 bits. This has been confirmed
        //experimentally to be wrong, as some BRAM locations cannot be written using just bit 0 of
        //WEA. As a fix, all 4 bits of WEA are driven with the same signal, which actually ensures
        //that writes can be made. Incidentally, this behavior does not manifest when operating with
        //data widths of 18 and 36 bits.
      end
    end
    else begin
      invalid_parameter_value length();
    end
  endgenerate
endmodule
