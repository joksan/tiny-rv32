//+------------------------------------------------------------------------------------------------+
//| RAM module with a single 32-bit read/write port and 4-lane write enable.                       |
//|                                                                                                |
//| This is a generic version intended for simulation with Icarus or synthesis for iCE40 FPGAs     |
//| with Yosys and IceStorm. Although other device series might be able to implement this since no |
//| vendor-specific extensions are used, quick updates of BRAM contents may not be possible due to |
//| particular toolchain requirements such as instantiating device-specific BRAM primitives.       |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module ram_32rw_4lwe #(
  parameter init_file = "",
  parameter length = 1024
)
(
  input clk,
  input rd_en,
  input [3:0] wr_en,
  input [31:2] addr,
  output reg [31:0] rd_data,
  input [31:0] wr_data
);
  reg [31:0] data [0:length-1];   //Stored data
  genvar i;

  //Load initial RAM contents.
  initial begin
    if (init_file != "") begin
      $readmemh(init_file, data);
    end
  end

  //Write port block with 4-lane write enable.
  generate
    for (i = 0; i < 4; i = i + 1) begin
      always @(posedge clk) begin
        if (wr_en[i])
          data[addr][i*8 + 7 : i*8] <= wr_data[i*8 + 7 : i*8];
      end
    end
  endgenerate

  //Read port block.
  always @(posedge clk) begin
    if (rd_en)
      rd_data <= data[addr];
    else
      rd_data <= 32'hXXXXXXXX;
  end
endmodule
