//+------------------------------------------------------------------------------------------------+
//| RAM module with a single 32-bit read/write port and 4-lane write enable.                       |
//|                                                                                                |
//| Spartan-3E version for use with Xilinx ISE.                                                    |
//|                                                                                                |
//| This module is intended to work with the bmm-gen.py script for initialization. See the script  |
//| source for details.                                                                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module ram_32rw_4lwe #(
  parameter init_file = "",
  parameter length = 1024
)
(
  input clk,
  input rd_en,
  input [3:0] wr_en,
  input [31:2] addr,
  output [31:0] rd_data,
  input [31:0] wr_data
);
  genvar i;

  generate
    if (length <= 1024) begin
      //Length is 1024 (or less) 32-bit words. Use two RAMB16_S9_S9 (dual port) instances for a
      //total of four 9-bit ports. Each port is used to control access to a single byte lane. Each
      //RAMB16_S9_S9 instance provides 2048 bytes of memory (parity is unused) mapped as 1024 x 16.
      //Both instances show up as a single 1024 x 32 memory.
      for (i = 0; i < 2; i = i + 1) begin: bram_inst
        RAMB16_S9_S9 #(
          .INIT_A(9'h000),              //Clear port output registers at startup
          .INIT_B(9'h000),
          .SRVAL_A(9'h000),             //Clear output values when SSR is asserted
          .SRVAL_B(9'h000),
          .WRITE_MODE_A("READ_FIRST"),  //Set write mode to READ_FIRST for consistency reasons
          .WRITE_MODE_B("READ_FIRST")   //(concurrent read/write is not used anyway)
        )
        bram (
          .ADDRA({ addr[11:2], 1'b0 }),   //Port A handles even addresses (data bits 7-0 and 23-16)
          .DOA(rd_data[i*16+7:i*16]),
          .DIA(wr_data[i*16+7:i*16]),
          .WEA(wr_en[i*2]),
          .ADDRB({ addr[11:2], 1'b1 }),   //Port B handles odd addresses (data bits 15-8 and 31-24)
          .DOB(rd_data[i*16+15:i*16+8]),
          .DIB(wr_data[i*16+15:i*16+8]),
          .WEB(wr_en[i*2+1]),
          .CLKA(clk),  .CLKB(clk),        //Both ports share the same clock
          .DIPA(1'd0), .DIPB(1'd0),       //Parity unused
          .DOPA(),     .DOPB(),
          .ENA(1'd1),  .ENB(1'd1),        //Both ports always enabled
          .SSRA(1'd0), .SSRB(1'd0)        //Synchronous set/reset unused
        );
      end
    end
    else if (length <= 2048) begin
      //Length is 2048 (or less) 32-bit words. Use four RAMB16_S9 instances. Each holds a single
      //byte lane. All instances show up as a single 2048 x 32 memory.
      for (i = 0; i < 4; i = i + 1) begin: bram_inst
        RAMB16_S9 #(
          .INIT(9'h000),              //Clear output registers at startup
          .SRVAL(9'h000),             //Clear output value when SSR is asserted
          .WRITE_MODE("READ_FIRST")   //Set write mode to READ_FIRST
        )
        bram (
          .ADDR(addr[12:2]),          //Each instance handles all addressess
          .DO(rd_data[i*8+7:i*8]),    //Each instance handles a single byte lane
          .DI(wr_data[i*8+7:i*8]),
          .WE(wr_en[i]),
          .CLK(clk),
          .DIP(1'd0), .DOP(),         //Parity unused
          .EN(1'd1),                  //Always enabled
          .SSR(1'd0)                  //Synchronous set/reset unused
        );
      end
    end
    else if (length <= 4096) begin
      //Length is 4096 (or less) 32-bit words. Use eight RAMB16_S4 instances. Each holds half a byte
      //lane. All instances show up as a single 4096 x 32 memory.
      for (i = 0; i < 8; i = i + 1) begin: bram_inst
        RAMB16_S4 #(
          .INIT(4'h0),                //Clear output registers at startup
          .SRVAL(4'h0),               //Clear output value when SSR is asserted
          .WRITE_MODE("READ_FIRST")   //Set write mode to READ_FIRST
        )
        bram (
          .ADDR(addr[13:2]),          //Each instance handles all addresses
          .DO(rd_data[i*4+3:i*4]),    //Each instance handles half a byte lane
          .DI(wr_data[i*4+3:i*4]),
          .WE(wr_en[i/2]),
          .CLK(clk),
          .EN(1'd1),                  //Always enabled
          .SSR(1'd0)                  //Synchronous set/reset unused
        );
      end
    end
    else if (length <= 8192) begin
      //Length is 8192 (or less) 32-bit words. Use sixteen RAMB16_S2 instances. Each holds a quarter
      //byte lane. All instances show up as a single 8192 x 32 memory.
      for (i = 0; i < 16; i = i + 1) begin: bram_inst
        RAMB16_S2 #(
          .INIT(2'b00),               //Clear output registers at startup
          .SRVAL(2'b00),              //Clear output value when SSR is asserted
          .WRITE_MODE("READ_FIRST")   //Set write mode to READ_FIRST
        )
        bram (
          .ADDR(addr[14:2]),          //Each instance handles all addresses
          .DO(rd_data[i*2+1:i*2]),    //Each instance handles a quarter byte lane
          .DI(wr_data[i*2+1:i*2]),
          .WE(wr_en[i/4]),
          .CLK(clk),
          .EN(1'd1),                  //Always enabled
          .SSR(1'd0)                  //Synchronous set/reset unused
        );
      end
    end
    else begin
      invalid_parameter_value length();
    end
  endgenerate
endmodule
