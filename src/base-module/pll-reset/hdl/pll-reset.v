//+------------------------------------------------------------------------------------------------+
//| PLL and reset module for the tiny-rv32e softcore processor.                                    |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module pll_reset #(
  parameter clk_freq_in = 0,
  parameter clk_freq_out = 0
)
(
  input clk_in,
  output clk_out,
  output reset
);
  //PLL output lock signal.
  wire lock;

  //Reset counter register. Amount of bits may be different between simulation and synthesis.
  reg [`RESET_COUNTER_BITS-1:0] reset_counter = 0;

  //Note:
  //In the iCE40 FPGAs it's not possible to initialize registers to values other than 0. Since the
  //reset behavior is not fully controllable, explicit initialization is not done throughout the
  //code base except for this single place, where the reset counter register is initialized to said
  //value. This is done in order to have a single place where reliable initialization is done and
  //also to have the simulation tool to behave correctly.

  //Reset is asserted until all counter bits become 1.
  assign reset = ~&reset_counter;

  //Count until the PLL is locked and for as long as the reset signal is asserted.
  always @(posedge clk_out) begin
    if (reset & lock)
      reset_counter <= reset_counter + 1;
  end

`ifdef __ICARUS__
  //This section applies to Icarus simulation only. It provides simulated clock and lock signals.

  //Clock and lock registers.
  reg clk_out_q;
  reg lock_q;

  //Asign the register values to module signals.
  assign clk_out = clk_out_q;
  assign lock = lock_q;

  //Make sure that the output clock frequency is correctly configured.
  generate
    if (clk_freq_out == 0)
      invalid_parameter_value clk_freq_out();
  endgenerate

  //Simulate a single lock pulse, then generate an steady clock signal.
  initial begin
    clk_out_q <= 1'b1;
    lock_q <= 1'b0;
    #(1000000000.0/clk_freq_out);
    lock_q <= 1'b1;
    forever begin
      #(1000000000.0/clk_freq_out/2);
      clk_out_q <= !clk_out_q;
    end
  end
`else //__ICARUS__
  //This section applies to synthesis only.

  generate
`ifdef ice40
    if (clk_freq_in == 12000000 && clk_freq_out == 50000000) begin
      //iCE40 PLL instance.
      SB_PLL40_CORE #(
        //This PLL configuration was generated automatically using the icepll tool from the IceStorm
        //project with the following command:
        //$ icepll -i 12 -o 50 -f settings.v
        .FEEDBACK_PATH("SIMPLE"),
        .DIVR(4'b0000),         //DIVR =  0
        .DIVF(7'b1000010),      //DIVF = 66
        .DIVQ(3'b100),          //DIVQ =  4
        .FILTER_RANGE(3'b001)   //FILTER_RANGE = 1
      )
      pll (
        .LOCK(lock),
        .RESETB(1'b1),
        .BYPASS(1'b0),
        .REFERENCECLK(clk_in),
        .PLLOUTCORE(clk_out)
      );
    end
    if (clk_freq_in == 16000000 && clk_freq_out == 25000000) begin
      SB_PLL40_CORE #(
        //IceStorm command:
        //$ icepll -i 16 -o 25 -f settings.v
        .FEEDBACK_PATH("SIMPLE"),
        .DIVR(4'b0000),         //DIVR =  0
        .DIVF(7'b0110001),      //DIVF = 49
        .DIVQ(3'b101),          //DIVQ =  5
        .FILTER_RANGE(3'b001)   //FILTER_RANGE = 1
      )
      pll (
        .LOCK(lock),
        .RESETB(1'b1),
        .BYPASS(1'b0),
        .REFERENCECLK(clk_in),
        .PLLOUTCORE(clk_out)
      );
    end
`elsif spartan
    if (clk_freq_in == 50000000 && clk_freq_out == 50000000) begin
      assign lock = 1'b1;
      assign clk_out = clk_in;
    end
    if (clk_freq_in == 32000000 && clk_freq_out == 50000000) begin
      wire clk_fx;

      //Digital clock manager instance.
      DCM_SP #(
        .CLKFX_DIVIDE(16),
        .CLKFX_MULTIPLY(25),
        .CLKIN_PERIOD(31.25)
      )
      DCM_SP_inst (
        .CLKIN(clk_in),
        .CLKFX(clk_fx),
        .LOCKED(lock),
        .RST(1'b0)
      );

      //Global buffer for the synthesized clock.
      BUFG output_buffer (
        .I(clk_fx),
        .O(clk_out)
      );
    end
`endif
  endgenerate
`endif //__ICARUS__
endmodule
