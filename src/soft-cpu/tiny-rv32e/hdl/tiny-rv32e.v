//+------------------------------------------------------------------------------------------------+
//| Main source file for the tiny-rv32e softcore processor.                                        |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

//Macros used to perform sign and zero extension on an arbitrary slice of a vector, indicated by the
//high and low bit positions. The size of the result is also specified.
`define sign_extend(vect, high, low, size)\
  { { (size - (high - low + 1)) { vect[high] } }, vect[high:low] }
`define zero_extend(vect, high, low, size)\
  { { (size - (high - low + 1)) { 1'b0 } }, vect[high:low] }

//Opcode field definitions. Vector ranges are grouped here to avoid number duplication errors.
`define opf_inst_size(opc) opc[1:0]       //Instruction size (11 = 32-bit)
`define opf_inst_type(opc) opc[6:2]       //Major instruction type
`define opf_inst_args(opc) opc[31:7]      //All instruction arguments, including minor inst. type
`define opf_funct3(opc) opc[14:12]        //Minor instruction type
`define opf_funct7_modifier(opc) opc[30]  //This bit differentiates: srli/srai, add/sub and srl/sra

`define opf_rd(opc) opc[10:7]             //Destination register
`define opf_rs1(opc) opc[18:15]           //Source register 1
`define opf_rs2(opc) opc[23:20]           //Source register 2

`define opf_i_immediate(opc) opc[31:20]                                     //I-immediate
`define opf_s_immediate(opc) { opc[31:25], opc[11:7] }                      //S-immediate
`define opf_b_immediate(opc) { opc[31], opc[7], opc[30:25], opc[11:8] }     //B-immediate
`define opf_u_immediate(opc) opc[31:12]                                     //U-immediate
`define opf_j_immediate(opc) { opc[31], opc[19:12], opc[20], opc[30:21] }   //J-immediate

//Main processor module.
module tiny_rv32e_cpu (
  input clk,              //Clock input
  input reset,            //Reset signal
  input hold,             //Hold signal (pauses execution)
  output rd_en,           //Read enable
  output [3:0] wr_en,     //Write enable (4 lanes)
  output [31:2] addr,     //Memory/peripheral address
  input [31:0] rd_data,   //Input data from memory/peripherals
  output [31:0] wr_data   //Output data to memory/peripherals
);
  //RV32E instruction types.
  localparam [4:0] inst_type_lui     = 5'b01101;
  localparam [4:0] inst_type_auipc   = 5'b00101;
  localparam [4:0] inst_type_jal     = 5'b11011;
  localparam [4:0] inst_type_jalr    = 5'b11001;
  localparam [4:0] inst_type_branch  = 5'b11000;
  localparam [4:0] inst_type_load    = 5'b00000;
  localparam [4:0] inst_type_store   = 5'b01000;
  localparam [4:0] inst_type_alu_lit = 5'b00100;
  localparam [4:0] inst_type_alu_reg = 5'b01100;

  //Registers and signals related to the main register array (X0 - X15).
  reg [3:0] regs_rd_sel_d, regs_rd_sel_q;
  wire [3:0] regs_rs1_sel;
  wire [3:0] regs_rs2_sel;
  wire [31:0] regs_rd;
  wire [31:0] regs_rs1;
  wire [31:0] regs_rs2;

  //Registers and signals related to the program counter.
  reg pc_req_next;
  reg pc_req_jal_d, pc_req_jal_q;
  reg pc_req_jalr_d, pc_req_jalr_q;
  reg pc_req_branch_d, pc_req_branch_q;
  wire [31:2] pc_value;
  wire [31:0] pc_result;
  wire pc_ready;

  //Registers and signals related to the memory access unit.
  reg mau_req_rd_d, mau_req_rd_q;
  reg mau_req_wr_d, mau_req_wr_q;
  wire [31:0] mau_result;
  wire mau_ready;
  wire mau_rd_en;
  wire [31:2] mau_addr;

  //Registers and signals related to the arithmetic logic unit.
  reg alu_req_lit_d, alu_req_lit_q;
  reg alu_req_reg_d, alu_req_reg_q;
  wire [31:0] alu_result;
  wire alu_ready;

  //Miscellaneous purpose registers.
  reg [31:7] inst_args_d, inst_args_q;
  reg [19:0] lui_result_d, lui_result_q;
  reg [31:2] auipc_result_d, auipc_result_q;

  //Processor states.
  localparam [2:0] st_store_fetch    = 3'd0;
  localparam [2:0] st_decode         = 3'd1;
  localparam [2:0] st_pc_exec        = 3'd2;
  localparam [2:0] st_mau_exec       = 3'd3;
  localparam [2:0] st_alu_exec       = 3'd4;
  localparam [2:0] st_invalid_opcode = 3'd5;
  reg [2:0] state_d, state_q;

  //Main register array instance.
  reg_array regs (
    .clk(clk),
    .reset(reset),
    .hold(hold),
    .rd_sel(regs_rd_sel_q),
    .rs1_sel(regs_rs1_sel),
    .rs2_sel(regs_rs2_sel),
    .rd(regs_rd),
    .rs1(regs_rs1),
    .rs2(regs_rs2)
  );

  //Program counter instance.
  program_counter pc (
    .clk(clk),
    .reset(reset),
    .hold(hold),
    .req_next(pc_req_next),
    .req_jal(pc_req_jal_q),
    .req_jalr(pc_req_jalr_q),
    .req_branch(pc_req_branch_q),
    .rs1(regs_rs1),
    .rs2(regs_rs2),
    .i_imm(`opf_i_immediate(inst_args_q)),
    .b_imm(`opf_b_immediate(inst_args_q)),
    .j_imm(`opf_j_immediate(inst_args_q)),
    .funct3(`opf_funct3(inst_args_q)),
    .value(pc_value),
    .result(pc_result),
    .ready(pc_ready)
  );

  //Memory access unit instance.
  memory_access_unit mau (
    //Processor interface ports.
    .clk(clk),
    .reset(reset),
    .hold(hold),
    .req_rd(mau_req_rd_q),
    .req_wr(mau_req_wr_q),
    .rs1(regs_rs1),
    .rs2(regs_rs2),
    .i_imm(`opf_i_immediate(inst_args_q)),
    .s_imm(`opf_s_immediate(inst_args_q)),
    .funct3(`opf_funct3(inst_args_q)),
    .result(mau_result),
    .ready(mau_ready),

    //Memory interface ports.
    .rd_en(mau_rd_en),
    .wr_en(wr_en),
    .addr(mau_addr),
    .rd_data(rd_data),
    .wr_data(wr_data)
  );

  //Arithmetic logic unit instance.
  arithmetic_logic_unit alu (
    .clk(clk),
    .reset(reset),
    .hold(hold),
    .req_lit(alu_req_lit_q),
    .req_reg(alu_req_reg_q),
    .rs1(regs_rs1),
    .rs2(regs_rs2),
    .i_imm(`opf_i_immediate(inst_args_q)),
    .funct3(`opf_funct3(inst_args_q)),
    .funct7_modifier(`opf_funct7_modifier(inst_args_q)),
    .result(alu_result),
    .ready(alu_ready)
  );

  //Output port assignments.
  assign rd_en = (state_q == st_store_fetch || mau_rd_en) && !hold;
  assign addr = (state_q == st_mau_exec)? mau_addr: pc_value;

  //Register signal assignments.
  assign regs_rs1_sel = `opf_rs1(rd_data);
  assign regs_rs2_sel = `opf_rs2(rd_data);
  assign regs_rd = { lui_result_q, 12'd0 } | { auipc_result_q, 2'd0 } |
                   pc_result | mau_result | alu_result;

  //Main cpu finite state machine (Mealy machine).
  always @* begin
    //Default values for registers that may retain their value for more than one cycle.
    inst_args_d = inst_args_q;
    lui_result_d = lui_result_q;
    auipc_result_d = auipc_result_q;
    pc_req_jal_d = pc_req_jal_q;
    pc_req_jalr_d = pc_req_jalr_q;
    pc_req_branch_d = pc_req_branch_q;
    mau_req_rd_d = mau_req_rd_q;
    mau_req_wr_d = mau_req_wr_q;
    alu_req_lit_d = alu_req_lit_q;
    alu_req_reg_d = alu_req_reg_q;
    state_d = state_q;

    //Default values for registers that retain their value for a single cycle.
    regs_rd_sel_d = 4'd0;

    //Default values for combinatorial signals.
    pc_req_next = 1'b0;

    case (state_q)
      st_store_fetch: begin
        //This state allows for the result of the last instruction to be stored (if needed) and also
        //for the next instruction to be fetched. No operation is scheduled for the next cycle, as
        //the instruction needs to be decoded first.
        state_d = st_decode;
      end
      st_decode: begin
        //Default values for registers updated in the decode state.
        lui_result_d = 20'd0;
        auipc_result_d = 30'd0;

        //Registers updated in the decode state.
        inst_args_d = `opf_inst_args(rd_data);

        case (`opf_inst_type(rd_data))
          inst_type_lui: begin //LUI
            lui_result_d = `opf_u_immediate(rd_data);
            regs_rd_sel_d = `opf_rd(rd_data);
            pc_req_next = 1'b1;
            state_d = st_store_fetch;
          end
          inst_type_auipc: begin //AUIPC
            //The result of AUIPC is evaluated in the range [31:2], discarding the 2 LSB.
            auipc_result_d = pc_value + { `opf_u_immediate(rd_data), 10'd0 };
            regs_rd_sel_d = `opf_rd(rd_data);
            pc_req_next = 1'b1;
            state_d = st_store_fetch;
          end
          inst_type_jal: begin //JAL
            pc_req_jal_d = 1'b1;
            state_d = st_pc_exec;
          end
          inst_type_jalr: begin //JALR
            pc_req_jalr_d = 1'b1;
            state_d = st_pc_exec;
          end
          inst_type_branch: begin //BEQ, BNE, BLT, BGE, BLTU, BGEU
            pc_req_branch_d = 1'b1;
            state_d = st_pc_exec;
          end
          inst_type_load: begin //LB, LH, LW, LBU, LHU
            mau_req_rd_d = 1'b1;
            state_d = st_mau_exec;
          end
          inst_type_store: begin //SB, SH, SW
            mau_req_wr_d = 1'b1;
            state_d = st_mau_exec;
          end
          inst_type_alu_lit: begin //ADDI, SLLI, SLTI, SLTIU, XORI, SRLI, SRAI, ORI, ANDI
            alu_req_lit_d = 1'b1;
            state_d = st_alu_exec;
          end
          inst_type_alu_reg: begin //ADD, SUB, SLL, SLT, SLTU, XOR, SRL, SRA, OR, AND
            alu_req_reg_d = 1'b1;
            state_d = st_alu_exec;
          end
          default: begin
            state_d = st_invalid_opcode;
          end
        endcase

        //If the instruction is not 32-bit wide, trigger the invalid opcode state. This overrides
        //any previous transition without adding dependencies to other processor logic.
        if (`opf_inst_size(rd_data) != 2'b11)
          state_d = st_invalid_opcode;
      end
      st_pc_exec: begin
        //This state waits for jump/branch instructions to finish and cleans up afterwards.
        if (pc_ready) begin
          pc_req_jal_d = 1'b0;
          pc_req_jalr_d = 1'b0;
          pc_req_branch_d = 1'b0;

          if (!pc_req_branch_q)
            regs_rd_sel_d = `opf_rd(inst_args_q);

          state_d = st_store_fetch;
        end
      end
      st_mau_exec: begin
        //This state waits for load/store instructions to finish and cleans up afterwards.
        if (mau_ready) begin
          mau_req_rd_d = 1'b0;
          mau_req_wr_d = 1'b0;

          if (mau_req_rd_q)
            regs_rd_sel_d = `opf_rd(inst_args_q);

          pc_req_next = 1'b1;
          state_d = st_store_fetch;
        end
      end
      st_alu_exec: begin
        //This state waits for ALU instructions to finish and cleans up afterwards.
        if (alu_ready) begin
          alu_req_lit_d = 1'b0;
          alu_req_reg_d = 1'b0;
          regs_rd_sel_d = `opf_rd(inst_args_q);
          pc_req_next = 1'b1;
          state_d = st_store_fetch;
        end
      end
      st_invalid_opcode: begin
        //An invalid opcode was found. Stop and lock the processor in this state.
      end
      default: begin
        //Also lock the processor in case something goes wrong unexpectedly.
        state_d = st_invalid_opcode;
      end
    endcase
  end

  //Register description for the main cpu finite state machine.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      inst_args_q <= 25'd0;
      lui_result_q <= 20'd0;
      auipc_result_q <= 30'd0;
      regs_rd_sel_q <= 4'd0;
      pc_req_jal_q <= 1'b0;
      pc_req_jalr_q <= 1'b0;
      pc_req_branch_q <= 1'b0;
      mau_req_rd_q <= 1'b0;
      mau_req_wr_q <= 1'b0;
      alu_req_lit_q <= 1'b0;
      alu_req_reg_q <= 1'b0;
      state_q <= st_store_fetch;
    end
    else if (!hold) begin
      inst_args_q <= inst_args_d;
      lui_result_q <= lui_result_d;
      auipc_result_q <= auipc_result_d;
      regs_rd_sel_q <= regs_rd_sel_d;
      pc_req_jal_q <= pc_req_jal_d;
      pc_req_jalr_q <= pc_req_jalr_d;
      pc_req_branch_q <= pc_req_branch_d;
      mau_req_rd_q <= mau_req_rd_d;
      mau_req_wr_q <= mau_req_wr_d;
      alu_req_lit_q <= alu_req_lit_d;
      alu_req_reg_q <= alu_req_reg_d;
      state_q <= state_d;
    end
  end
endmodule

//Main register array module.
module reg_array (
  input clk,
  input reset,
  input hold,
  input [3:0] rd_sel,     //Register selection inputs
  input [3:0] rs1_sel,
  input [3:0] rs2_sel,
  input [31:0] rd,        //Input value for rd
  output reg [31:0] rs1,  //Latched output for rs1
  output reg [31:0] rs2   //Latched output for rs2
);
  reg [31:0] x [1:15];    //Register array. Holds R1 through R15. R0 is always 0 so it's omitted.
  reg [31:0] rs1_mux;     //Multiplexer output for rs1
  reg [31:0] rs2_mux;     //Multiplexer output for rs2
  genvar i;
  integer j;

  //Register writes are encoded as a generate block. Indexing with rd_sel directly is avoided and
  //a genvar is used instead. This avoids addressing register x0 directly, as it's not in the array.
  //Each register is generated in such a way that address decode logic is generated in the clock
  //enable path.
  generate
    for (i = 1; i <= 15; i = i + 1) begin
      always @(posedge clk, posedge reset) begin
        if (reset)
          x[i] <= 32'd0;
        else if (!hold)
          if (rd_sel == i)
            x[i] <= rd;
      end
    end
  endgenerate

  //Register reads are done by multiplexing the register array. Indexing with rs1_sel/rs2_sel
  //directly is also avoided for the same reason as register writes, so a cumulative OR expression
  //(which should be nicely reduced) is used instead.
  always @* begin
    rs1_mux = 32'd0;
    rs2_mux = 32'd0;
    for (j = 1; j <= 15; j = j + 1) begin
      if (rs1_sel == j)
        rs1_mux = rs1_mux | x[j];
      if (rs2_sel == j)
        rs2_mux = rs2_mux | x[j];
    end
  end

  //Register multiplexer outputs are latched to mitigate combinatorial delay.
  always @(posedge clk, posedge reset) begin
    if (reset) begin
      rs1 <= 32'd0;
      rs2 <= 32'd0;
    end
    else if (!hold) begin
      rs1 <= rs1_mux;
      rs2 <= rs2_mux;
    end
  end
endmodule

//Program counter module.
module program_counter (
  input clk,
  input reset,
  input hold,
  input req_next,
  input req_jal,
  input req_jalr,
  input req_branch,
  input [31:0] rs1,
  input [31:0] rs2,
  input [11:0] i_imm,
  input [12:1] b_imm,
  input [20:1] j_imm,
  input [2:0] funct3,
  output [31:2] value,
  output [31:0] result,
  output ready
);
  localparam funct3_beq  = 3'b000;
  localparam funct3_bne  = 3'b001;
  localparam funct3_blt  = 3'b100;
  localparam funct3_bge  = 3'b101;
  localparam funct3_bltu = 3'b110;
  localparam funct3_bgeu = 3'b111;

  localparam st_wait = 1'd0;
  localparam st_exec = 1'd1;
  reg state;

  reg cmp_eq;
  reg cmp_add32;
  reg cmp_carry32;

  reg [31:2] pc;
  reg [31:2] pc_next;
  reg [31:0] return_addr;
  reg [31:2] jal_dest;
  reg [31:2] jalr_dest;
  reg [31:2] branch_dest;

  assign value = pc;
  assign result = return_addr;
  assign ready = state == st_exec;

  always @(posedge clk, posedge reset) begin
    if (reset) begin
      state <= st_wait;
      cmp_eq <= 1'b0;
      cmp_add32 <= 1'd0;
      cmp_carry32 <= 1'd0;
      pc <= 30'd0;
      pc_next <= 30'd0;
      return_addr <= 32'd0;
      jal_dest <= 30'd0;
      jalr_dest <= 30'd0;
      branch_dest <= 30'd0;
    end
    else if (!hold) begin
      return_addr <= 32'd0;

      pc_next <= pc + 1'd1; //Actually value + 4, since range is [31:2]
      cmp_eq <= rs1 == rs2;
      cmp_add32 <= rs1[31] + rs2[31];
      cmp_carry32 <= ({ 1'd0, rs1 } - { 1'd0, rs2 }) >> 32;

      case (state)
        st_wait: begin
          if (req_jal || req_jalr || req_branch)
            state <= st_exec;

          if (req_next)
            pc <= pc_next;
          if (req_jal)
            jal_dest <= pc + `sign_extend(j_imm, 20, 2, 30);
          if (req_jalr)
            jalr_dest <= (rs1 + `sign_extend(i_imm, 11, 0, 32)) >> 2;
          if (req_branch)
            branch_dest <= pc + `sign_extend(b_imm, 12, 2, 30);
        end
        st_exec: begin
          state <= st_wait;

          if (req_jal || req_jalr)
            return_addr <= pc_next << 2;

          if (req_jal)
            pc <= jal_dest;
          if (req_jalr)
            pc <= jalr_dest;
          if (req_branch) begin
            case (funct3)
              funct3_beq: begin
                pc <= cmp_eq? branch_dest: pc_next;
              end
              funct3_bne: begin
                pc <= !cmp_eq? branch_dest: pc_next;
              end
              funct3_blt: begin
                pc <= (cmp_carry32 ^ cmp_add32)? branch_dest: pc_next;
              end
              funct3_bge: begin
                pc <= !(cmp_carry32 ^ cmp_add32)? branch_dest: pc_next;
              end
              funct3_bltu: begin
                pc <= cmp_carry32? branch_dest: pc_next;
              end
              funct3_bgeu: begin
                pc <= !cmp_carry32? branch_dest: pc_next;
              end
              default begin
                pc <= 30'hXXXXXXXX;
              end
            endcase
          end
        end
      endcase
    end
  end
endmodule

//Memory access unit module.
module memory_access_unit (
  //Processor interface ports.
  input clk,
  input reset,
  input hold,
  input req_rd,
  input req_wr,
  input [31:0] rs1,
  input [31:0] rs2,
  input [11:0] i_imm,
  input [11:0] s_imm,
  input [2:0] funct3,
  output [31:0] result,
  output ready,

  //Memory interface ports.
  output reg rd_en,
  output reg [3:0] wr_en,
  output [31:2] addr,
  input [31:0] rd_data,
  output reg [31:0] wr_data
);
  localparam [2:0] funct3_lb  = 3'b000;
  localparam [2:0] funct3_lh  = 3'b001;
  localparam [2:0] funct3_lw  = 3'b010;
  localparam [2:0] funct3_lbu = 3'b100;
  localparam [2:0] funct3_lhu = 3'b101;

  localparam [2:0] funct3_sb  = 3'b000;
  localparam [2:0] funct3_sh  = 3'b001;
  localparam [2:0] funct3_sw  = 3'b010;

  localparam st_wait = 1'd0;
  localparam st_exec = 1'd1;

  reg state;            //Current FSM state

  reg [31:0] eff_addr;  //Calculated effective address

  //Routing flags. Used to multiplex the final result of a memory read operation.
  reg route_b0_to_b0;     //Route byte 0 to byte 0 (the rest are self explanatory)
  reg route_b1_to_b0;
  reg route_b2_to_b0;
  reg route_b3_to_b0;
  reg route_b1_to_b1;
  reg route_b3_to_b1;
  reg route_b32_to_b32;   //Route bytes 3 and 2 to bytes 3 and 2

  //Sign extension flags. Used to sign extend the final result.
  reg sig_ext_b0_to_b1;   //Sign extend from byte 0 (bit 7) to byte 1
  reg sig_ext_b1_to_b1;   //Sign extend from byte 1 (bit 15) to byte 1
  reg sig_ext_b2_to_b1;
  reg sig_ext_b3_to_b1;
  reg sig_ext_b0_to_b32;  //Sign extend from byte 0 (bit 7) to bytes 3 and 2
  reg sig_ext_b1_to_b32;
  reg sig_ext_b2_to_b32;
  reg sig_ext_b3_to_b32;

  always @(posedge clk, posedge reset) begin
    if (reset) begin
      state <= st_wait;
      eff_addr = 32'd0;
      route_b0_to_b0 <= 1'b0;
      route_b1_to_b0 <= 1'b0;
      route_b2_to_b0 <= 1'b0;
      route_b3_to_b0 <= 1'b0;
      route_b1_to_b1 <= 1'b0;
      route_b3_to_b1 <= 1'b0;
      route_b32_to_b32 <= 1'b0;
      sig_ext_b0_to_b1 <= 1'b0;
      sig_ext_b1_to_b1 <= 1'b0;
      sig_ext_b2_to_b1 <= 1'b0;
      sig_ext_b3_to_b1 <= 1'b0;
      sig_ext_b0_to_b32 <= 1'b0;
      sig_ext_b1_to_b32 <= 1'b0;
      sig_ext_b2_to_b32 <= 1'b0;
      sig_ext_b3_to_b32 <= 1'b0;
      rd_en <= 1'b0;
      wr_en <= 4'b0000;
      wr_data <= 32'd0;
    end
    else if (!hold) begin
      route_b0_to_b0 <= 1'b0;
      route_b1_to_b0 <= 1'b0;
      route_b2_to_b0 <= 1'b0;
      route_b3_to_b0 <= 1'b0;
      route_b1_to_b1 <= 1'b0;
      route_b3_to_b1 <= 1'b0;
      route_b32_to_b32 <= 1'b0;
      sig_ext_b0_to_b1 <= 1'b0;
      sig_ext_b1_to_b1 <= 1'b0;
      sig_ext_b2_to_b1 <= 1'b0;
      sig_ext_b3_to_b1 <= 1'b0;
      sig_ext_b0_to_b32 <= 1'b0;
      sig_ext_b1_to_b32 <= 1'b0;
      sig_ext_b2_to_b32 <= 1'b0;
      sig_ext_b3_to_b32 <= 1'b0;
      rd_en <= 1'b0;
      wr_en <= 4'b0000;
      wr_data <= 32'd0;

      case (state)
        st_wait: begin
          if (req_rd || req_wr)
            state <= st_exec;

          if (req_rd) begin
            eff_addr = rs1 + `sign_extend(i_imm, 11, 0, 32);
            rd_en <= 1'd1;
          end

          if (req_wr) begin
            eff_addr = rs1 + `sign_extend(s_imm, 11, 0, 32);
            case (eff_addr[1:0])
              2'd0: begin
                wr_en <= (funct3 == funct3_sb)? 4'b0001:
                         (funct3 == funct3_sh)? 4'b0011:
                         (funct3 == funct3_sw)? 4'b1111:
                                                4'bXXXX;
                wr_data <= rs2;
              end
              2'd1: begin
                wr_en <= (funct3 == funct3_sb)? 4'b0010:
                                                4'bXXXX;
                wr_data <= { 16'hXXXX, rs2[7:0], 8'hXX };
              end
              2'd2: begin
                wr_en <= (funct3 == funct3_sb)? 4'b0100:
                         (funct3 == funct3_sh)? 4'b1100:
                                                4'bXXXX;
                wr_data <= { rs2[15:0], 16'hXXXX };
              end
              2'd3: begin
                wr_en <= (funct3 == funct3_sb)? 4'b1000:
                                                4'bXXXX;
                wr_data <= { rs2[7:0], 24'hXXXXXX };
              end
              default: begin
                wr_en <= 4'bXXXX;
                wr_data <= 32'hXXXXXXXX;
              end
            endcase
          end
        end
        st_exec: begin
          //This state only manages data reads.
          if (req_rd) begin
            //Check wich opcode is being executed.
            case (funct3)
              funct3_lb, funct3_lbu: begin
                //Signed/unsigned byte loads.
                case (eff_addr[1:0])
                  2'd0: begin
                    //Read aligned to byte 0. Route the least significant byte to the same location
                    //and sign-extend from that byte to the rest.
                    route_b0_to_b0 <= 1'b1;
                    sig_ext_b0_to_b1 <= !funct3[2];
                    sig_ext_b0_to_b32 <= !funct3[2];
                  end
                  2'd1: begin
                    //Read aligned to byte 1. Route the second byte to the least significant one and
                    //sign extend the rest from there.
                    route_b1_to_b0 <= 1'b1;
                    sig_ext_b1_to_b1 <= !funct3[2];
                    sig_ext_b1_to_b32 <= !funct3[2];
                  end
                  2'd2: begin
                    route_b2_to_b0 <= 1'b1;
                    sig_ext_b2_to_b1 <= !funct3[2];
                    sig_ext_b2_to_b32 <= !funct3[2];
                  end
                  2'd3: begin
                    route_b3_to_b0 <= 1'b1;
                    sig_ext_b3_to_b1 <= !funct3[2];
                    sig_ext_b3_to_b32 <= !funct3[2];
                  end
                endcase
              end
              funct3_lh, funct3_lhu: begin
                //Signed/unsigned half-word loads.
                if (eff_addr[1] == 0) begin
                  //Read aligned to half-word 0. Route the least significant bytes to the same
                  //locations and sign extend from byte 1 to the rest.
                  route_b0_to_b0 <= 1'b1;
                  route_b1_to_b1 <= 1'b1;
                  sig_ext_b1_to_b32 <= !funct3[2];
                end
                else begin
                  //Read aligned to half-word 1.
                  route_b3_to_b1 <= 1'b1;
                  route_b2_to_b0 <= 1'b1;
                  sig_ext_b3_to_b32 <= !funct3[2];
                end
              end
              funct3_lw: begin
                //Word loads. Simply route all bytes to the same locations. No need to do any sign
                //extension.
                route_b0_to_b0 <= 1'b1;
                route_b1_to_b1 <= 1'b1;
                route_b32_to_b32 <= 1'b1;
              end
            endcase
          end

          state <= st_wait;
        end
      endcase
    end
  end

  assign addr = eff_addr[31:2];
  assign ready = state == st_exec;

  //Multiplex the lower 8 bits of the result by ANDing each source bit with the corresponding
  //routing flag and then combining them together by ORing them together. This produces a faster
  //multiplexer that plays nicely with other ORed data sources. Note that the lower bits of the
  //result are never sign extended.
  assign result[7:0] = ({ 8 { route_b0_to_b0 } } & rd_data[7:0]) |
                       ({ 8 { route_b1_to_b0 } } & rd_data[15:8]) |
                       ({ 8 { route_b2_to_b0 } } & rd_data[23:16]) |
                       ({ 8 { route_b3_to_b0 } } & rd_data[31:24]);

  //Perform a similar strategy for the next set of 8 data bits. This time only 2 sources are
  //actually possible due to alignment restrictions. Sign extension is done by ANDing the replicated
  //source bits with their corresponding sign extension flags and then ORing everything together.
  assign result[15:8] = ({ 8 { route_b1_to_b1 } } & rd_data[15:8]) |
                        ({ 8 { route_b3_to_b1 } } & rd_data[31:24]) |
                        { 8 { sig_ext_b0_to_b1 & rd_data[7] } } |
                        { 8 { sig_ext_b1_to_b1 & rd_data[15] } } |
                        { 8 { sig_ext_b2_to_b1 & rd_data[23] } } |
                        { 8 { sig_ext_b3_to_b1 & rd_data[31] } };

  //Perform a similar strategy for the last set of 16 data bits. This time only one source is
  //possible as the alignment is unique. Also perform a similar sign extension.
  assign result[31:16] = ({ 16 { route_b32_to_b32 } } & rd_data[31:16]) |
                         { 16 { sig_ext_b0_to_b32 & rd_data[7] } } |
                         { 16 { sig_ext_b1_to_b32 & rd_data[15] } } |
                         { 16 { sig_ext_b2_to_b32 & rd_data[23] } } |
                         { 16 { sig_ext_b3_to_b32 & rd_data[31]} };
endmodule

//Arithmetic logic unit module.
module arithmetic_logic_unit (
  input clk,
  input reset,
  input hold,
  input req_lit,
  input req_reg,
  input [31:0] rs1,
  input [31:0] rs2,
  input [11:0] i_imm,
  input [2:0] funct3,
  input funct7_modifier,
  output [31:0] result,
  output ready
);
  localparam [2:0] funct3_addx_sub = 3'b000;
  localparam [2:0] funct3_sllx     = 3'b001;
  localparam [2:0] funct3_sltx     = 3'b010;
  localparam [2:0] funct3_sltxu    = 3'b011;
  localparam [2:0] funct3_xorx     = 3'b100;
  localparam [2:0] funct3_srxx     = 3'b101;
  localparam [2:0] funct3_orx      = 3'b110;
  localparam [2:0] funct3_andx     = 3'b111;

  localparam st_wait = 1'b0;
  localparam st_exec = 1'b1;
  reg state;

  reg [31:0] op1;
  reg [31:0] op2;
  reg [4:0] shift_count;
  reg msb_shift_value;
  reg [31:0] sllx_intermediate;
  reg [31:0] srxx_intermediate;

  reg [31:0] addx_sub_result;
  reg [31:0] sllx_result;
  reg [0:0] sltxx_result;
  reg [31:0] bitwise_result;
  reg [31:0] srxx_result;

  assign result = addx_sub_result | sllx_result | `zero_extend(sltxx_result, 0, 0, 32) |
                  bitwise_result | srxx_result;

  //For the time being, the ALU is considered ready after the first cycle of the execution state.
  //This leaves space for adding multiple cycle instructions like multiplication later.
  assign ready = state == st_exec;

  always @(posedge clk, posedge reset) begin
    if (reset) begin
      state <= st_wait;
      op1 <= 32'd0;
      op2 <= 32'd0;
      shift_count = 5'd0;
      msb_shift_value = 1'b0;
      sllx_intermediate <= 32'd0;
      srxx_intermediate <= 32'd0;
      addx_sub_result <= 32'd0;
      sllx_result <= 32'd0;
      sltxx_result <= 1'd0;
      bitwise_result <= 32'd0;
      srxx_result <= 32'd0;
    end
    else if (!hold) begin
      addx_sub_result <= 32'd0;
      sllx_result <= 32'd0;
      sltxx_result <= 1'd0;
      bitwise_result <= 32'd0;
      srxx_result <= 32'd0;

      case (state)
        st_wait: begin
          if (req_lit || req_reg) begin
            op1 <= rs1;
            state <= st_exec;
          end

          if (req_lit)
            op2 <= `sign_extend(i_imm, 11, 0, 32);
          if (req_reg)
            op2 <= (funct7_modifier == 1'b0)? rs2:
                   (funct7_modifier == 1'b1)? -rs2:
                                              32'hXXXXXXXX;

          shift_count = req_lit? i_imm[4:0]: rs2[4:0];

          case (shift_count[1:0])
            2'd0: sllx_intermediate <= rs1;
            2'd1: sllx_intermediate <= { rs1[30:0], 1'd0 };
            2'd2: sllx_intermediate <= { rs1[29:0], 2'd0 };
            2'd3: sllx_intermediate <= { rs1[28:0], 3'd0 };
            default: sllx_intermediate <= 32'hXXXXXXXX;
          endcase

          msb_shift_value = (funct7_modifier == 1'b0)? 1'b0:
                            (funct7_modifier == 1'b1)? rs1[31]:
                                                       1'bX;

          case (shift_count[1:0])
            2'd0: srxx_intermediate <= rs1;
            2'd1: srxx_intermediate <= { { 1 { msb_shift_value } }, rs1[31:1] };
            2'd2: srxx_intermediate <= { { 2 { msb_shift_value } }, rs1[31:2] };
            2'd3: srxx_intermediate <= { { 3 { msb_shift_value } }, rs1[31:3] };
            default: srxx_intermediate <= 32'hXXXXXXXX;
          endcase
        end
        st_exec: begin
          //The ALU goes idle on the next cycle after execution.
          state <= st_wait;

          case (funct3)
            funct3_addx_sub: begin
              addx_sub_result <= op1 + op2;
            end
            funct3_sllx: begin
              case (shift_count[4:2])
                3'd0: sllx_result <= sllx_intermediate;
                3'd1: sllx_result <= { sllx_intermediate[27:0], 4'd0 };
                3'd2: sllx_result <= { sllx_intermediate[23:0], 8'd0 };
                3'd3: sllx_result <= { sllx_intermediate[19:0], 12'd0 };
                3'd4: sllx_result <= { sllx_intermediate[15:0], 16'd0 };
                3'd5: sllx_result <= { sllx_intermediate[11:0], 20'd0 };
                3'd6: sllx_result <= { sllx_intermediate[7:0], 24'd0 };
                3'd7: sllx_result <= { sllx_intermediate[3:0], 28'd0 };
                default: sllx_result <= 32'hXXXXXXXX;
              endcase
            end
            funct3_sltx: begin
              sltxx_result <= ($signed(op1) < $signed(op2))? 1'd1: 1'd0;
            end
            funct3_sltxu: begin
              sltxx_result <= ($unsigned(op1) < $unsigned(op2))? 1'd1: 1'd0;
            end
            funct3_xorx: begin
              bitwise_result <= op1 ^ op2;
            end
            funct3_srxx: begin
              case (shift_count[4:2])
                3'd0: srxx_result <= srxx_intermediate;
                3'd1: srxx_result <= { { 4 { msb_shift_value } }, srxx_intermediate[31:4] };
                3'd2: srxx_result <= { { 8 { msb_shift_value } }, srxx_intermediate[31:8] };
                3'd3: srxx_result <= { { 12 { msb_shift_value } }, srxx_intermediate[31:12] };
                3'd4: srxx_result <= { { 16 { msb_shift_value } }, srxx_intermediate[31:16] };
                3'd5: srxx_result <= { { 20 { msb_shift_value } }, srxx_intermediate[31:20] };
                3'd6: srxx_result <= { { 24 { msb_shift_value } }, srxx_intermediate[31:24] };
                3'd7: srxx_result <= { { 28 { msb_shift_value } }, srxx_intermediate[31:28] };
                default: srxx_result <= 32'hXXXXXXXX;
              endcase
            end
            funct3_orx: begin
              bitwise_result <= op1 | op2;
            end
            funct3_andx: begin
              bitwise_result <= op1 & op2;
            end
          endcase
        end
      endcase
    end
  end
endmodule
