#+-------------------------------------------------------------------------------------------------+
#| Minimal startup code for tiny-rv32 softcore processors.                                         |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Referenced external symbols.
.extern __stack_start
.extern main

#Exported symbols.
.global _start

#Startup section is allocatable (a) and executable (x).
.section .startup, "ax"

#Reset vector code.
_start:
  la sp, __stack_start  #Load the initial stack pointer.
  tail main             #Jump to main function.

#Please note:
#In other scenarios such as hard-core microcontrollers, particularly those based on FLASH memory,
#the .data and .bss sections are usually initialized here. This is not necessary in this case, as
#the complete RAM memory is already initialized during FPGA initialization. Also note that the
#default fill value for RAM is 0x00 (as implemented by the hex-convert.py tool), which will work
#just fine for the .bss section.
