/**************************************************************************************************/
/* Common linker script for tiny-rv32 softcore processors using a region of RAM implemented with  */
/* block RAM (BRAM) resources and a region of memory-mapped external flash.                       */
/*                                                                                                */
/* Author: Joksan Alvarado.                                                                       */
/**************************************************************************************************/

/* Set the default region for the read-only data section (constants) */
#ifndef RODATA_REGION
#define RODATA_REGION FLASH
#endif

/* Set the default region for the text section (code) */
#ifndef TEXT_REGION
#define TEXT_REGION FLASH
#endif

MEMORY {
  /* Define the memory regions. */
  RAM (rwx) : ORIGIN = 0, LENGTH = RAM_SIZE_WORDS*4
  FLASH (rx) : ORIGIN = FLASH_BASE_ADDR, LENGTH = FLASH_SIZE
}

/* Program etry point. */
ENTRY (_start)

SECTIONS {
  /* The startup code is at the beginning of RAM (reset vector). */
  .startup ORIGIN(RAM) : {
    *(.startup*)
  } > RAM

  /* User-defined functions can be stored in RAM for fast execution. */
  .ramfunc : {
    *(.ramfunc*)
  } > RAM

  /* The text section contains all regular code. */
  .text : {
    *(.text*)
  } > TEXT_REGION

  /* The rodata section contains all constants. */
  .rodata : {
    . = ALIGN(4);
    *(.rodata*)
  } > RODATA_REGION

  /* The data section contains all initialized data. */
  .data : {
    . = ALIGN(4);
    *(.data*)
  } > RAM

  /* The bss section contains all uninitialized data. */
  .bss : {
    . = ALIGN(4);
    *(.bss*)
    *(COMMON)
  } > RAM

  /* Determine stack size and boundaries. */
  __stack_size = LENGTH(RAM) / 8;
  __stack_start = ORIGIN(RAM) + LENGTH(RAM);
  __stack_limit = __stack_start - __stack_size;

  /* The stack section is a reserved placeholder. No symbols are actually allocated here. */
  .stack __stack_limit : {
    . += __stack_size;
  } > RAM
}
