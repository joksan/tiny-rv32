//+------------------------------------------------------------------------------------------------+
//| Generic disassembler for a simulated RV32E softcore processor.                                 |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

//Opcode field definitions. Vector ranges are grouped here to avoid number duplication errors.
`define field_opcode opcode[6:0]
`define field_funct3 opcode[14:12]
`define field_funct7 opcode[31:25]

`define field_rd opcode[10:7]
`define field_rs1 opcode[18:15]
`define field_rs2 opcode[23:20]

`define field_i_immediate opcode[31:20]
`define field_i_immediate_shift opcode[24:20]
`define field_s_immediate { opcode[31:25], opcode[11:7] }
`define field_b_immediate { opcode[31], opcode[7], opcode[30:25], opcode[11:8] }
`define field_u_immediate opcode[31:12]
`define field_j_immediate { opcode[31], opcode[19:12], opcode[20], opcode[30:21] }

//Disassembler module.
module rv32e_disassembler (
  input [31:0] opcode,                  //The opcode that is to be disassembled
  input enable,                         //Disassembly is done upon rising edges of this signal
  output reg [64*8:1] instruction = ""  //Disassembled instruction string
);
  //Finite state machine for the disassembler (Moore machine).
  always @(posedge enable, opcode) begin
    if (enable) begin
      case (`field_opcode)
        7'b0110111: begin
          $sformat(instruction, "lui x%1d 0x%h", `field_rd, `field_u_immediate);
        end
        7'b0010111: begin
          $sformat(instruction, "auipc x%1d 0x%h", `field_rd, `field_u_immediate);
        end
        7'b1101111: begin
          $sformat(instruction, "jal x%1d, %1d", `field_rd, $signed(`field_j_immediate << 1));
        end
        7'b1100111: begin
          $sformat(instruction, "jalr x%1d, x%1d, %1d",
                   `field_rd, `field_rs1, $signed(`field_i_immediate));
        end
        7'b1100011: begin
          case (`field_funct3)
            3'b000: $sformat(instruction, "beq x%1d, x%1d, %1d",
                             `field_rs1, `field_rs2, $signed(`field_b_immediate << 1));
            3'b001: $sformat(instruction, "bne x%1d, x%1d, %1d",
                             `field_rs1, `field_rs2, $signed(`field_b_immediate << 1));
            3'b100: $sformat(instruction, "blt x%1d, x%1d, %1d",
                             `field_rs1, `field_rs2, $signed(`field_b_immediate << 1));
            3'b101: $sformat(instruction, "bge x%1d, x%1d, %1d",
                             `field_rs1, `field_rs2, $signed(`field_b_immediate << 1));
            3'b110: $sformat(instruction, "bltu x%1d, x%1d, %1d",
                             `field_rs1, `field_rs2, $signed(`field_b_immediate << 1));
            3'b111: $sformat(instruction, "bgeu x%1d, x%1d, %1d",
                             `field_rs1, `field_rs2, $signed(`field_b_immediate << 1));
            default: instruction = "???";
          endcase
        end
        7'b0000011: begin
          case (`field_funct3)
            3'b000: $sformat(instruction, "lb x%1d, 0x%h(x%1d)",
                             `field_rd, `field_i_immediate, `field_rs1);
            3'b001: $sformat(instruction, "lh x%1d, 0x%h(x%1d)",
                             `field_rd, `field_i_immediate, `field_rs1);
            3'b010: $sformat(instruction, "lw x%1d, 0x%h(x%1d)",
                             `field_rd, `field_i_immediate, `field_rs1);
            3'b100: $sformat(instruction, "lbu x%1d, 0x%h(x%1d)",
                             `field_rd, `field_i_immediate, `field_rs1);
            3'b101: $sformat(instruction, "lhu x%1d, 0x%h(x%1d)",
                             `field_rd, `field_i_immediate, `field_rs1);
            default: instruction = "???";
          endcase
        end
        7'b0100011: begin
          case (`field_funct3)
            3'b000: $sformat(instruction, "sb x%1d, 0x%h(x%1d)",
                             `field_rs2, `field_s_immediate, `field_rs1);
            3'b001: $sformat(instruction, "sh x%1d, 0x%h(x%1d)",
                             `field_rs2, `field_s_immediate, `field_rs1);
            3'b010: $sformat(instruction, "sw x%1d, 0x%h(x%1d)",
                             `field_rs2, `field_s_immediate, `field_rs1);
            default: instruction = "???";
          endcase
        end
        7'b0010011: begin
          case (`field_funct3)
            3'b000: $sformat(instruction, "addi x%1d, x%1d, 0x%h",
                             `field_rd, `field_rs1, `field_i_immediate);
            3'b001:
              case (`field_funct7)
                7'b0000000: $sformat(instruction, "slli x%1d, x%1d, %1d",
                                     `field_rd, `field_rs1, `field_i_immediate_shift);
                default: instruction = "???";
              endcase
            3'b010: $sformat(instruction, "slti x%1d, x%1d, 0x%h",
                             `field_rd, `field_rs1, `field_i_immediate);
            3'b011: $sformat(instruction, "sltiu x%1d, x%1d, 0x%h",
                             `field_rd, `field_rs1, `field_i_immediate);
            3'b100: $sformat(instruction, "xori x%1d, x%1d, 0x%h",
                             `field_rd, `field_rs1, `field_i_immediate);
            3'b101:
              case (`field_funct7)
                7'b0000000: $sformat(instruction, "srli x%1d, x%1d, %1d",
                                     `field_rd, `field_rs1, `field_i_immediate_shift);
                7'b0100000: $sformat(instruction, "srai x%1d, x%1d, %1d",
                                     `field_rd, `field_rs1, `field_i_immediate_shift);
                default: instruction = "???";
              endcase
            3'b110: $sformat(instruction, "ori x%1d, x%1d, 0x%h",
                             `field_rd, `field_rs1, `field_i_immediate);
            3'b111: $sformat(instruction, "andi x%1d, x%1d, 0x%h",
                             `field_rd, `field_rs1, `field_i_immediate);
            default: instruction = "???";
          endcase
        end
        7'b0110011: begin
          case ({ `field_funct7 , `field_funct3 })
            { 7'b0000000, 3'b000 }: $sformat(instruction, "add x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0100000, 3'b000 }: $sformat(instruction, "sub x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b001 }: $sformat(instruction, "sll x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b010 }: $sformat(instruction, "slt x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b011 }: $sformat(instruction, "sltu x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b100 }: $sformat(instruction, "xor x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b101 }: $sformat(instruction, "srl x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0100000, 3'b101 }: $sformat(instruction, "sra x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b110 }: $sformat(instruction, "or x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            { 7'b0000000, 3'b111 }: $sformat(instruction, "and x%1d, x%1d, x%1d",
                                             `field_rd, `field_rs1, `field_rs2);
            default: instruction = "???";
          endcase
        end
        default: begin
          instruction = "???";
        end
      endcase
    end
  end
endmodule
