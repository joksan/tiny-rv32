//+------------------------------------------------------------------------------------------------+
//| Simulated external dual I/O SPI FLASH memory for the tiny-rv32e softcore processor.            |
//|                                                                                                |
//| This is a simplified implementation of an external dual I/O SPI FLASH memory, intended for     |
//| testing the mm-dspi-flash peripheral. Note that in order to keep thing simple, the command     |
//| byte is ignored and assumed to always be 0xBB (dual I/O fast read).                            |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

module dspi_flash_sim #(
  parameter init_file = "",       //Initialization file
  parameter mem_size = 0,         //Memory size in bytes
  parameter dummy_cycles = 8,     //Number of dummy cycles between address and data
  parameter user_data_offset = 0  //Address in memory where to load user data
)
(
  input sck,
  inout miso,
  inout mosi,
  input ss
);
  localparam address_bits = $clog2(mem_size);   //Number of memory address bits
  localparam spi_address_bytes = 3;             //Number of address bytes transferred through SPI

  integer cycle_count;              //Amount of cycles elapsed since the last time SS went low

  reg miso_q;                       //Output register for MISO port
  reg mosi_q;                       //Output register for MOSI port
  reg oe = 1'b1;                    //Output enable for MISO/MOSI ports
  reg [1:0] bit_pair_count = 2'd0;  //Amount of transferred bit pairs (4 per byte)
  reg [7:0] data_mem [2**address_bits-1:0];                     //Main memory array
  reg [address_bits-1:0] address = { address_bits { 1'b0 } };   //Current address

  //Make sure that the memory size is configured correctly.
  generate
    if (mem_size == 0)
      invalid_parameter_value mem_size();
  endgenerate

  //Send the contents of both output registers when the output is enabled. Otherwise leave the lines
  //in the high impedance state.
  assign miso = oe? miso_q: 1'bz;
  assign mosi = oe? mosi_q: 1'bz;

  //Initalize the memory contents.
  initial begin: spi_mem_init
    integer i;

    //First set all locations to uninitialized.
    for (i = 0; i < 2**address_bits; i = i + 1)
      data_mem[i] = 8'hXX;

    //Load the user data contents afterwards.
    if (init_file != "")
      $readmemh(init_file, data_mem, user_data_offset, mem_size - 1);
  end

  always @(negedge sck) begin
    //Only respond to clock if slave select is low.
    if (ss == 1'b0) begin
      //Increase the cycle count.
      cycle_count <= cycle_count + 1;

      //Shift in the address bits during the address window.
      if (cycle_count >= 8 && cycle_count < 8 + spi_address_bytes * 4) begin
        address <= { address[2**address_bits-3:0], miso, mosi };
      end

      //Shift out the data bits continuously upon reaching the first data cycle.
      if (cycle_count >= 8 + spi_address_bytes * 4 + dummy_cycles - 1) begin
        //Enable the output and send the next pair of bits.
        oe <= 1'b1;
        miso_q <= data_mem[address][7 - bit_pair_count*2];
        mosi_q <= data_mem[address][6 - bit_pair_count*2];

        //Increase the bit pair count. When it reaches the last pair, advance to the next address.
        bit_pair_count <= bit_pair_count + 1'd1;
        if (bit_pair_count == 2'd3) begin
          address <= address + 1'd1;
        end
      end
    end
  end

  always @(posedge ss) begin
    //When slave select rises, reset all relevant logic.
    cycle_count <= 0;
    oe <= 1'b0;
    bit_pair_count <= 2'd0;
  end
endmodule
