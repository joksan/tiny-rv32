#+-------------------------------------------------------------------------------------------------+
#| Converter program from Intel hex to other formats.                                              |
#|                                                                                                 |
#| This program takes a .hex file, parses it and exports its content in one of the supported       |
#| output formats.                                                                                 |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

from sys import argv
from os.path import basename

from memory_map.memory_map import *
from hex_parse.hex_parse import *
from verilog_writer.verilog_writer import *
from mif_writer.mif_writer import *

#Default values for all options
input_hex_file = None
output_file = None
output_format = None
word_width = 1
base_address = 0
memory_size = None
print_address = False
trim_output = False
help_requested = False

#Iterate all program arguments starting from the second (first one is the program name). Set the
#corresponding variable according to each option.
i = iter(argv[1::])
try:
  while True:
    arg = next(i)
    if arg == '-i': input_hex_file = next(i)
    elif arg == '-o': output_file = next(i)
    elif arg == '-f': output_format = next(i)
    elif arg == '-w': word_width = int(next(i))
    elif arg == '-b': base_address = int(next(i), 0)
    elif arg == '-s': memory_size = int(next(i), 0)
    elif arg == '-a': print_address = True
    elif arg == '-t': trim_output = True
    elif arg == '-h': help_requested = True
    else: raise ValueError('Unknown option: ' + arg)
except StopIteration:
  pass

#Check wether help was requested
show_usage = False
if help_requested:
  show_usage = True
else:
  #Help not requested, check wethern an option is missing
  if input_hex_file is None:
    print('Missing option: -i')
    show_usage = True
  elif output_file is None:
    print('Missing option: -o')
    show_usage = True
  elif output_format is None:
    print('Missing option: -f')
    show_usage = True
  elif memory_size is None:
    print('Missing option: -s')
    show_usage = True

#Make sure that the requested output type is valid
if output_format is not None and output_format not in ('vlog', 'bin', 'mif'):
  print('Unsupported output format: {}'.format(output_format))
  show_usage = True

#The -a option is only valid for the verilog output type
if print_address and output_format != 'vlog':
  print('Option -a is only valid with vlog output type')
  show_usage = True

#The -t option is only valid for the binary output type
if trim_output and output_format != 'bin':
  print('Option -t is only valid with bin output type')
  show_usage = True

#Show the usage, if required
if show_usage:
  print('Usage:', basename(__file__), '[options]')
  print('Required options:')
  print('  -i input_hex_file: Specify the intel hex file from where data is read')
  print('  -o output_file: Specify the output file to write')
  print('  -f output_format: Specify the output format. Supported formats:')
  print('     vlog: Verilog data file (for use with $readmemh)')
  print('     bin: Raw binary output')
  print('     mif: Memory initialization file (for use with Quartus Prime)')
  print('  -w word_width: Specify word width in bytes (default is 1)')
  print('  -b base_address: Specify the start of the data range (default is 0)')
  print('  -s memory_size: Specify memory size in words')
  print('  -a: Print the memory address at the beginning of each line (vlog only)')
  print('  -t: Trim the output to the last word present (bin only)')
  print('Other options:')
  print('  -h: Show this help')
  print()
  print('Note: Total size in bytes is word_width * memory_size.')
  if help_requested: exit()
  else: exit(-1)

#All options validated now, create a new memory map
hex_file_map = memory_map(memory_size * word_width)

#Parse the input hex file and insert it into the memory map
hex_parse_file(input_hex_file, base_address, hex_file_map)

#Convert to the requested output format
if output_format == 'vlog':
  #Write the output as a verilog data file
  write_verilog_mem_data(output_file, hex_file_map, word_width, print_address)
elif output_format == 'bin':
  #Write the output as a binary file
  with open(output_file, 'wb') as handle:
    if trim_output:
      #If the output is to be trimmed, get the last empty block address and round it up to the
      #nearest word size, then write the partial memory map.
      last_word_address = hex_file_map.last_empty_block_address()
      if last_word_address % word_width != 0:
        last_word_address += word_width - last_word_address % word_width
      handle.write(bytes(hex_file_map[0:last_word_address]))
    else:
      #Otherwise just write the complete memory map
      handle.write(bytes(hex_file_map))
elif output_format == 'mif':
  #Write the output as a memory initialization file
  write_mif(output_file, hex_file_map, word_width)
