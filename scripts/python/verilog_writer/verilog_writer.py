#+-------------------------------------------------------------------------------------------------+
#| Verilog data file writer library.                                                               |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

def write_verilog_mem_data(file_name, mem_map, word_width, print_address):
  #Assert that the memory map size is multiple of word width
  if len(mem_map) % word_width != 0:
    raise ValueError('Memory map size is not a multiple of word width')

  #Open the file
  handle = open(file_name, 'w')

  #Write the rom data
  for i in range(0, len(mem_map), word_width):
    #Get the next word-wide data slice, reverse the order to format it MSB first
    data = mem_map[i:i+word_width][::-1]

    #Write the start of the entry which includes the address, if required
    if print_address:
      handle.write('@{:X} '.format(i // word_width))

    #Write the data bytes
    handle.write(''.join('{:02X}'.format(i) for i in data))

    #Write the end of the entry
    handle.write('\n')

  #All done, close the file
  handle.close()
