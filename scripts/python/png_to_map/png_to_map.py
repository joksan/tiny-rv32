#+-------------------------------------------------------------------------------------------------+
#| Conversion library for PNG images into pattern memory maps.                                     |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

from PIL import Image

def png_to_map(image_file, pattern_map, bits_per_pixel):
  #Open the image file for reading
  im = Image.open(image_file)

  #Make sure that the image has valid dimensions
  if im.width % 8 != 0 or im.height % 8 != 0:
    raise ValueError('Image width and height must be a multiple of 8 pixels')

  #Also verify that the image fits inside the pattern map
  if im.width * im.height * bits_per_pixel // 8 > len(pattern_map):
    raise ValueError('Image is larger than the output can hold')

  #Set the starting map address
  addr = 0

  #Perform a conversion algorithm based on image resolution
  if bits_per_pixel == 1:
    #1BPP image
    #Iterate over every 8x8 pattern, from left to right and from top to bottom
    for j in range(0, im.height, 8):
      for i in range(0, im.width, 8):
        #Iterate over every pattern row
        for k in range(0, 8):
          #Crop a row from the pattern
          row = im.crop((i, j + k, i + 8, j + k + 1))

          #Convert the row to a bit pattern. The leftmost pixel occupies the LSB, so the order is
          #reversed to allow shifting to the left.
          data = 0
          for pix in list(row.getdata())[::-1]:
            data = (data << 1) | pix

          #Insert the bit pattern into the map and increment the map address
          pattern_map.insert(addr, (data,))
          addr += 1
  elif bits_per_pixel == 2:
    #2BPP image
    #Iterate over every 8x8 pattern, from left to right and from top to bottom
    for j in range(0, im.height, 8):
      for i in range(0, im.width, 8):
        #Iterate over every pattern in half rows, from left to right and from top to bottom
        for l in range(0, 8):
          for k in range(0, 2):
            #Crop a half row from the pattern
            hrow = im.crop((i + k * 4, j + l, i + k * 4 + 4, j + l + 1))

            #Convert the half row to a bit pattern. The leftmost pixel occupies the 2 LSB, so the
            #order is reversed to allow shifting to the left.
            data = 0
            for pix in list(hrow.getdata())[::-1]:
              data = (data << 2) | pix

            #Insert the bit pattern into the map and increment the map address
            pattern_map.insert(addr, (data,))
            addr += 1
  elif bits_per_pixel == 4:
    #4BPP image
    #Iterate over every 8x8 pattern, from left to right and from top to bottom
    for j in range(0, im.height, 8):
      for i in range(0, im.width, 8):
        #Iterate over every pattern in quarter rows, from left to right and from top to bottom
        for l in range(0, 8):
          for k in range(0, 4):
            #Crop a quarter row from the pattern
            qrow = im.crop((i + k * 2, j + l, i + k * 2 + 2, j + l + 1))

            #Convert the quarter row to a bit pattern. The leftmost pixel occupies the 4 LSB.
            data = qrow.getpixel((0, 0)) | (qrow.getpixel((1, 0)) << 4)

            #Insert the bit pattern into the map and increment the map address
            pattern_map.insert(addr, (data,))
            addr += 1
  else:
    raise ValueError('Invalid parameter: bits_per_pixel')
