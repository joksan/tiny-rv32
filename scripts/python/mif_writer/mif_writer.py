#+-------------------------------------------------------------------------------------------------+
#| Memory initialization file (.mif) writer library.                                               |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

def write_mif(file_name, mem_map, word_width):
  #Assert that the memory map size is multiple of word width
  if len(mem_map) % word_width != 0:
    raise ValueError('Memory map size is not a multiple of word width')

  #Open the file
  handle = open(file_name, 'w')

  #Write the header data
  handle.write('DEPTH = {:d};\n'.format(len(mem_map) // word_width))
  handle.write('WIDTH = {:d};\n'.format(word_width * 8))
  handle.write('ADDRESS_RADIX = DEC;\n')
  handle.write('DATA_RADIX = HEX;\n')
  handle.write('CONTENT BEGIN\n')

  #Write the rom data
  for i in range(0, len(mem_map), word_width):
    #Get the next word-wide data slice, reverse the order to format it MSB first
    data = mem_map[i:i+word_width][::-1]

    #Write the start of the entry which includes the address
    handle.write('{:d}: '.format(i // word_width))

    #Write the data bytes
    handle.write(''.join('{:02X}'.format(i) for i in data))

    #Write the end of the entry
    handle.write(';\n')

  #All done, write the trailer and close the file
  handle.write('END;\n')
  handle.close()
