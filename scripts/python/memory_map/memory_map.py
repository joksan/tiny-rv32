#+-------------------------------------------------------------------------------------------------+
#| Abstraction library for segmented memory maps.                                                  |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Iterator for the memory map class
class memory_map_iter:
  def __init__(self, mem_map):
    self.mem_map = mem_map
    self.pos = 0

  def __next__(self):
    if self.pos < self.mem_map.max_size:
      retval = self.mem_map[self.pos]
      self.pos += 1
      return retval
    else:
      raise StopIteration()

#Memory map class. Can store an arbitrary map of fixed size with gaps at arbitrary locations
class memory_map:
  def __init__(self, max_size):
    self.max_size = max_size
    self.data = []

  #Data insertion function. This function checks for collisions before insertion
  def insert(self, addr, data):
    #Check whether the address is negative. If so, truncate the request so only elements with
    #positive addresses are kept.
    if addr < 0:
      data = data[-addr::]
      addr = 0

    #Check whether the data length exceeds the capacity. If so, truncate the request so only
    #elements that can fit are kept.
    if addr + len(data) > self.max_size:
      data = data[: self.max_size - addr]

    #Assert that no empty tuple remains or is passed
    if len(data) == 0:
      return

    #Search for the position where the new entry could be inserted
    for i in range(len(self.data)):
      #Check wether the new entry address is after or at/before the current entry address
      if addr > self.data[i]['addr']:
        #If after, check that the new entry doesn't collide ahead of the current entry
        if self.data[i]['addr'] + len(self.data[i]['data']) > addr:
          raise ValueError('Data collides with existing data')
      else:
        #If at or before, check that the new entry doesn't collide before the current entry
        if addr + len(data) > self.data[i]['addr']:
          raise ValueError('Data collides with existing data')

        #Now insert the new entry at the position of the current one
        self.data.insert(i, {'addr': addr, 'data': data})

        #Merge the entry with the next one, if it follows immediately
        if self.data[i]['addr'] + len(self.data[i]['data']) == self.data[i + 1]['addr']:
          self.data[i]['data'] = self.data[i]['data'] + self.data[i + 1]['data']
          del self.data[i + 1]

        #Merge the entry with the previous one (if any), if it precedes immediately
        if i > 0:
          if self.data[i - 1]['addr'] + len(self.data[i - 1]['data']) == self.data[i]['addr']:
            self.data[i - 1]['data'] = self.data[i - 1]['data'] + self.data[i]['data']
            del self.data[i]

        #Insertion finished, return immediately
        return

    #If no insert position was found, it should go at the end
    if len(self.data) == 0:
      #In case of an empty list, just add it
      self.data.append({'addr': addr, 'data': data})
    else:
      #Check wether this entry immediately follows the last one
      if self.data[-1]['addr'] + len(self.data[-1]['data']) == addr:
        #If it immediately follows, merge it
        self.data[-1]['data'] = self.data[-1]['data'] + data
      else:
        #If it doesn't immediately follow, add it as a new entry
        self.data.append({'addr': addr, 'data': data})

  #Element access function
  def __getitem__(self, key):
    if isinstance(key, int):
      #Assert that the key range is within the limits
      if key < 0 or key >= self.max_size:
        raise IndexError('Requested address is outside memory boundaries')

      #Run through the list. Check wether the address fits inside any of the entries
      for i in range(len(self.data)):
        if self.data[i]['addr'] <= key < self.data[i]['addr'] + len(self.data[i]['data']):
          #If a match is found, return the data at the location
          return self.data[i]['data'][key - self.data[i]['addr']]

      #If no data is found, return the fill value
      return 0
    elif isinstance(key, slice):
      #Assert that the key range is within the limits
      if key.start < 0 or key.stop > self.max_size:
        raise IndexError('Requested range is outside memory boundaries')

      #Assert that the key range is in increasing order
      if key.start > key.stop:
        raise IndexError('Decreasing ranges are unsupported')

      #Prepare an "empty" (filled with the fill value) list that can hold the requested range
      retval = [0] * (key.stop - key.start)

      #Iterate over all stored entries
      for i in range(len(self.data)):
        #Get the start and stop addresses from the entry
        entry_start = self.data[i]['addr']
        entry_stop = self.data[i]['addr'] + len(self.data[i]['data'])

        #Check wether the entry range collides with the key range
        if entry_start < key.stop and entry_stop > key.start:
          #If it collides, get the address range relative to the source list (stored entry), making
          #sure they start at 0
          src_start = max(key.start, entry_start) - entry_start
          src_stop = min(key.stop, entry_stop) - entry_start

          #Do the same for the destination address range (return value)
          dst_start = max(key.start, entry_start) - key.start
          dst_stop = min(key.stop, entry_stop) - key.start

          #Overwrite the destination entries with the source ones
          retval[dst_start: dst_stop] = list(self.data[i]['data'][src_start: src_stop])

      #Once finished, return the resulting list as a tuple
      return tuple(retval)
    else:
      raise TypeError('Unsupported key type')

  #Length function
  def __len__(self):
    return self.max_size

  #Iterator function
  def __iter__(self):
    return memory_map_iter(self)

  #This function returns the address of the last empty block. Useful to trim the output to just the
  #amount of data needed.
  def last_empty_block_address(self):
    if not self.data:
      return 0
    else:
      return self.data[-1]['addr'] + len(self.data[-1]['data'])
