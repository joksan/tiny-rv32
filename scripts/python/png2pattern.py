#+-------------------------------------------------------------------------------------------------+
#| Converter program from PNG images to Verilog data files containing image pattern data.          |
#|                                                                                                 |
#| This program takes a .png file, splits it into 8x8 patterns and exports its content as a        |
#| verilog data file, suitable for use with $readmemh.                                             |
#|                                                                                                 |
#| The image must be in indexed color and contain at most 2 (1BPP), 4 (2BPP) or 16 (4BPP)          |
#| different colors. Palette data is ignored, as colors are handled separately by the video        |
#| generator. Only color index values are stored.                                                  |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

from sys import argv
from os.path import basename

from memory_map.memory_map import *
from png_to_map.png_to_map import *
from verilog_writer.verilog_writer import *

#Default values for all options
input_png_file = None
verilog_data_file = None
bits_per_pixel = None
pattern_count = None
help_requested = False

#Iterate all program arguments starting from the second (first one is the program name). Set the
#corresponding variable according to each option.
i = iter(argv[1::])
try:
  while True:
    arg = next(i)
    if arg == '-i': input_png_file = next(i)
    elif arg == '-o': verilog_data_file = next(i)
    elif arg == '-b': bits_per_pixel = int(next(i))
    elif arg == '-c': pattern_count = int(next(i))
    elif arg == '-h': help_requested = True
    else: raise ValueError('Unknown option: ' + arg)
except StopIteration:
  pass

#Check wether help was requested
show_usage = False
if help_requested:
  show_usage = True
else:
  #Help not requested, check wethern an option is missing
  if input_png_file is None:
    print('Missing option: -i')
    show_usage = True
  elif verilog_data_file is None:
    print('Missing option: -o')
    show_usage = True
  elif bits_per_pixel is None:
    print('Missing option: -b')
    show_usage = True
  elif pattern_count is None:
    print('Missing option: -c')
    show_usage = True

#Show the usage, if required
if show_usage:
  print('Usage:', basename(__file__), '[options]')
  print('Required options:')
  print('  -i input_png_file: Specify the input PNG image file for conversion')
  print('  -o verilog_data_file: Specify the output file to write data')
  print('  -b bits_per_pixel: Specify the color mode of the generated pattern')
  print('  -c pattern_count: Specify memory size in amount of patterns')
  print('Other options:')
  print('  -h: Show this help')
  if help_requested: exit()
  else: exit(-1)

#All options validated now, create a new memory map
pattern_map = memory_map(pattern_count * 64 * bits_per_pixel // 8)

#Read the PNG image file and insert it into the memory map
png_to_map(input_png_file, pattern_map, bits_per_pixel)

#Write the output hex file
write_verilog_mem_data(verilog_data_file, pattern_map, 2, False)
