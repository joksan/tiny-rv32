#+-------------------------------------------------------------------------------------------------+
#| Intel hex parser library.                                                                       |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Record types
DATA_RECORD = 0
EOF_RECORD = 1
EXTENDED_LINEAR_ADDRESS_RECORD = 4

#This function parses a single hex file line and returns its parsed data as a tuple containing the
#address, record type and data.
def hex_parse_line(line):
  #Start from the beginning of the line
  offset = 0

  #Assert that the line starts with the correct start code (:)
  if line[offset] != ':':
    raise ValueError('Incorrect file format')
  offset += 1

  #Parse the byte count (2 characters)
  byte_count = int(line[offset:offset+2], 16)
  offset += 2

  #Parse address (4 characters)
  address = int(line[offset:offset+4], 16)
  offset += 4

  #Parse the record type (2 characters)
  record_type = int(line[offset:offset+2], 16)
  offset += 2

  #Parse the data bytes
  data = []
  for i in range(0, byte_count):
    data.append(int(line[offset:offset+2], 16))
    offset += 2

  #Parse the checksum
  file_checksum = int(line[offset:offset+2], 16)
  offset += 2

  #Calculate the checksum
  calc_checksum = 0
  for i in range(1, 8 + byte_count * 2 + 1, 2):
    calc_checksum += int(line[i:i+2], 16)
  calc_checksum = -calc_checksum & 0xFF

  #Assert that the checksum matches
  if file_checksum != calc_checksum:
    raise ValueError('Bad line checksum')

  return address, record_type, data

#This function opens an intel hex file and parses its contents. Data is inserted into the specified
#memory map.
def hex_parse_file(file_name, base_address, mem_map):
  reference_address = 0

  #Open the file
  handle = open(file_name, 'r')

  #Iterate over each line
  for line in handle:
    #Parse the line
    record_address, record_type, data_line = hex_parse_line(line)

    #Check the record type
    if record_type == DATA_RECORD:
      #If it's a data record, insert it into the memory map
      mem_map.insert(reference_address + record_address - base_address, data_line)
    elif record_type == EOF_RECORD:
      #If it's an end of file record, stop the process
      break
    elif record_type == EXTENDED_LINEAR_ADDRESS_RECORD:
      if len(data_line) != 2:
        raise ValueError('Malformed record')
      reference_address = data_line[0] << 24 | data_line[1] << 16
    else:
      #Unknown record
      raise ValueError('Unknown or unimplemented record type')

  #All done, close the file
  handle.close()
