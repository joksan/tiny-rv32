#+-------------------------------------------------------------------------------------------------+
#| Test routine for the memory-mapped dual I/O SPI FLASH controller peripheral.                    |
#|                                                                                                 |
#| This routine is intended to be verified visually with a simulation run.                         |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

.section .rodata
test_data:
.byte 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF

.text
.global main
main:
  la s1, test_data  #Load the address of the test data

  lw t1, (s1)       #Read the first and second addresses consecutively
  lw t1, 4 (s1)
  #Note: The controller has already started reading the first address on its own after reset. Upon
  #encountering a request for that same address, it will just continue the ongoing read.

  nop               #Let the controller read the next word ahead (next read is right when available)
  nop
  nop
  lw t1, 8 (s1)     #Read the next address (should delay only one cycle)

  nop               #Once again, let the controller read the next word ahead
  nop
  nop
  lw t1, 8 (s1)     #Read the same address as last time (will trigger an slave deselect)

  lw t1, (s1)       #Read the first address again - don't let it finish (triggers another deselect)

  nop               #Let the controller finish reading the next word ahead
  nop
  nop
  nop
  lw t1, 4 (s1)     #Do a consecutive read (controller should be in stand-by now, should not delay)

  nop               #Wait once more for the next word
  nop
  nop
  nop
  lw t1, (s1)       #Trigger an slave deselect from stand by

  lw t1, 4 (s1)     #Do a couple of consecutive reads
  lw t1, 8 (s1)

loop:
  j loop            #Test finished. Loop indefinitely.
