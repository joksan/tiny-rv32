#+-------------------------------------------------------------------------------------------------+
#| Test routine for the master SPI peripheral.                                                     |
#|                                                                                                 |
#| This routine is intended to be verified visually with a simulation run.                         |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

.text
.global main
main:
  li s1, MSPI0_BASE_ADDR  #Load the peripheral address

  li t1, 0                #Activate chip select
  sb t1, 4 (s1)

  #Single byte transfer test.
  li t1, 0xA5             #Send 0xA5
  sb t1, (s1)
  nop                     #Wait
  nop
  nop
  nop
  nop
  lb t2, (s1)             #Read back

  #Double byte transfer test.
  li t1, 0x5A
  sb t1, (s1)
  li t1, 0x66
  sb t1, (s1)
  nop
  nop
  nop
  nop
  nop
  nop
  lb t2, (s1)
  lb t2, (s1)

  #Overflow test
  li t1, 0xAA             #Send 3 bytes at once
  sb t1, (s1)
  li t1, 0xBB
  sb t1, (s1)
  li t1, 0xCC             #Transfer still ongoing - this byte should be discarded
  sb t2, (s1)
  nop
  nop
  li t1, 0xDD             #Transfer complete but data not retrieved - this byte should be held
  sb t1, (s1)
  li t1, 0xEE             #Attempt to send another byte with the FIFO full - should be discarded too
  sb t1, (s1)
  lb t2, (s1)             #Retrieve a single byte - the byte that was held should be released now
  nop
  nop
  nop
  nop
  nop
  lb t2, (s1)             #Transfer done. Clean up received data.
  lb t2, (s1)

  #Back-to-back transmission test. Transmissions are intentionally kept separate by 4 instruction
  #(8 clock) cycles. Incoming data can be read at any time after the first transmission completes.
  li t1, 0x01             #Send the first byte
  sb t1, (s1)
  nop
  nop
  li t1, 0x02             #Send the second byte
  sb t1, (s1)
  lb t2, (s1)             #Retrieve the first byte
  nop
  li t1, 0x02             #Send the third byte
  sb t1, (s1)
  lb t2, (s1)             #Retrieve the second byte
  nop
  nop
  nop
  lb t2, (s1)             #Retrieve the third byte

  li t1, -1               #Deactivate chip select
  sb t1, 4 (s1)

loop:
  j loop                  #Test finished. Loop indefinitely.
