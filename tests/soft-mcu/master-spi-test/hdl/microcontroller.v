//+------------------------------------------------------------------------------------------------+
//| master-spi-test softcore microcontroller.                                                      |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

//Microcontroller module.
module microcontroller (
  input clk,
  output mspi0_sck,
  input mspi0_miso,
  output mspi0_mosi,
  output mspi0_ss
);
  //Signals related to the reset generator.
  wire reset;

  //Signals related to the processor.
  wire cpu0_rd_en;
  wire [3:0] cpu0_wr_en;
  wire [31:2] cpu0_addr;
  wire [31:0] cpu0_rd_data;
  wire [31:0] cpu0_wr_data;

  //Signals related to the RAM.
  wire cpu_mem0_rd_en;
  wire [3:0] cpu_mem0_wr_en;
  wire [31:0] cpu_mem0_rd_data;

  //Signals related to master SPI.
  wire mspi0_rd_en;
  wire [3:0] mspi0_wr_en;
  wire [31:0] mspi0_rd_data;

  //I/O control signals.
  reg io_read;

  //Reset generator instance.
  simple_reset reset_gen (
    .clk(clk),
    .reset(reset)
  );

  //Tiny-rv32e processor instance.
  tiny_rv32e_cpu cpu0 (
    .clk(clk),
    .reset(reset),
    .hold(1'd0),
    .rd_en(cpu0_rd_en),
    .wr_en(cpu0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //RAM instance.
  ram_32rw_4lwe #(
    .init_file(`CPU_MEM0_INIT_FILE),
    .length(`CPU_MEM0_LENGTH)
  )
  cpu_mem0 (
    .clk(clk),
    .rd_en(cpu_mem0_rd_en),
    .wr_en(cpu_mem0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu_mem0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //Master SPI instance.
  master_spi #(
    .clk_freq(`BOARD_CLOCK),
    .baud_rate(`MSPI0_BAUD_RATE),
    .cpol(`MSPI0_CPOL),
    .cpha(`MSPI0_CPHA),
    .num_slaves(1)
  )
  mspi0 (
    //System signals.
    .clk(clk),
    .reset(reset),

    //Processor bus signals.
    .rd_en(mspi0_rd_en),
    .wr_en(mspi0_wr_en),
    .addr(cpu0_addr),
    .rd_data(mspi0_rd_data),
    .wr_data(cpu0_wr_data),

    //Peripheral I/O signals.
    .sck(mspi0_sck),
    .miso(mspi0_miso),
    .mosi(mspi0_mosi),
    .ss(mspi0_ss)
  );

  //Decoding logic for multiplexing data reads from memory and I/O.
  always @(posedge clk) begin
    if (reset)
      io_read <= 1'b0;
    else
      //Addresses from 0x40000000 are mapped to I/O.
      io_read <= cpu0_rd_en && (cpu0_addr[31:30] != 2'd0);
  end

  //Read data assignment for CPU0.
  assign cpu0_rd_data = io_read? mspi0_rd_data: cpu_mem0_rd_data;

  //Read and write enable assignments for RAM.
  assign cpu_mem0_rd_en = (cpu0_addr[31:30] == 2'd0)? cpu0_rd_en: 1'b0;
  assign cpu_mem0_wr_en = (cpu0_addr[31:30] == 2'd0)? cpu0_wr_en: 4'h0;

  //Read and write enable assignments for master SPI.
  assign mspi0_rd_en = (cpu0_addr[31:30] == 2'd2)? cpu0_rd_en: 1'b0;
  assign mspi0_wr_en = (cpu0_addr[31:30] == 3'd2)? cpu0_wr_en: 4'h0;
endmodule
