//+------------------------------------------------------------------------------------------------+
//| Testbench for the master-spi-test softcore microcontroller.                                    |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

`timescale 1ns/1ps

module testbench ();
  //Softcore microcontroller ports.
  reg clk = 1'b1;
  wire mspi0_sck;
  reg mspi0_miso = 1'bz;
  wire mspi0_mosi;
  wire mspi0_ss;

  //Main processor registers.
  wire [31:0] x1;
  wire [31:0] x2;
  wire [31:0] x3;
  wire [31:0] x4;
  wire [31:0] x5;
  wire [31:0] x6;
  wire [31:0] x7;
  wire [31:0] x8;
  wire [31:0] x9;
  wire [31:0] x10;
  wire [31:0] x11;
  wire [31:0] x12;
  wire [31:0] x13;
  wire [31:0] x14;
  wire [31:0] x15;

  //Memory/peripheral address.
  wire [31:0] addr;

  //Disassembled instruction string.
  wire [64*8:1] instruction;

  //Memory arrays are not dumped to LXT files by default. Copy all register values from the CPU so
  //they can be easily viewed.
  assign x1 = mcu.cpu0.regs.x[1],
         x2 = mcu.cpu0.regs.x[2],
         x3 = mcu.cpu0.regs.x[3],
         x4 = mcu.cpu0.regs.x[4],
         x5 = mcu.cpu0.regs.x[5],
         x6 = mcu.cpu0.regs.x[6],
         x7 = mcu.cpu0.regs.x[7],
         x8 = mcu.cpu0.regs.x[8],
         x9 = mcu.cpu0.regs.x[9],
         x10 = mcu.cpu0.regs.x[10],
         x11 = mcu.cpu0.regs.x[11],
         x12 = mcu.cpu0.regs.x[12],
         x13 = mcu.cpu0.regs.x[13],
         x14 = mcu.cpu0.regs.x[14],
         x15 = mcu.cpu0.regs.x[15];

  //Create a properly aligned memory/peripheral address for viewing with a waveform viewer.
  assign addr = { mcu.cpu0.addr, 2'd0 };

  //Softcore microcontroller instance.
  microcontroller mcu (
    .clk(clk),
    .mspi0_sck(mspi0_sck),
    .mspi0_miso(mspi0_miso),
    .mspi0_mosi(mspi0_mosi),
    .mspi0_ss(mspi0_ss)
  );

  //Disassembler instance.
  rv32e_disassembler disassembler (
    .opcode(mcu.cpu0.rd_data),
    .enable(mcu.cpu0.state_q == mcu.cpu0.st_decode),
    .instruction(instruction)
  );

  //Initialization and control block.
  initial begin
    //Generate an LXT file and dump all generated data.
    $dumpfile(`ICARUS_LXT_FILE);
    $dumpvars(0, testbench);

    //Wait for a set amount of cycles.
    repeat (20480) @(posedge clk);

    //All done, finish simulation.
    $finish;
  end

  //Clock generation block.
  always begin
    clk <= 1'b1;
    #(1000000000.0/`BOARD_CLOCK/2);
    clk <= 1'b0;
    #(1000000000.0/`BOARD_CLOCK/2);
  end

  //Data generator task for SPI.
  task miso_send_byte;
    input [7:0] value;
    integer i;
    begin
      for (i = 0; i < 8; i = i + 1) begin
        //Place the bit first when CPHA=0.
        if (`MSPI0_CPHA == 0)
          mspi0_miso <= value[7];

        //Wait for the next edge of CLK that causes a data transition.
        if (`MSPI0_CPOL ^ `MSPI0_CPHA)
          @(posedge mspi0_sck);
        else
          @(negedge mspi0_sck);

        //Place the bit after when CPHA=1.
        if (`MSPI0_CPHA == 1)
          mspi0_miso <= value[7];

        //Shift the value for the next bit.
        value = value << 1;
      end
    end
  endtask

  //SPI input generation block.
  always begin
    //Wait for chip select to go low when CPHA=0 (when CPHA=1 the signal appears after the leading
    //edge of SCK).
    if (`MSPI0_CPHA == 0) begin
      @(negedge mspi0_ss); //mspi0_miso <= input_word[7];
    end

    //Sequentially generate data through the MISO line.
    miso_send_byte(8'h0F);
    miso_send_byte(8'h55);
    miso_send_byte(8'hA6);
    miso_send_byte(8'h12);
    miso_send_byte(8'h34);
    miso_send_byte(8'h56);
    miso_send_byte(8'h78);
    miso_send_byte(8'h9A);
    miso_send_byte(8'hBC);
  end
endmodule
