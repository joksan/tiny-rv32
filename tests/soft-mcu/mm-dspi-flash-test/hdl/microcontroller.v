//+------------------------------------------------------------------------------------------------+
//| mm-dspi-flash-test softcore microcontroller.                                                   |
//|                                                                                                |
//| Author: Joksan Alvarado.                                                                       |
//+------------------------------------------------------------------------------------------------+

//Microcontroller module.
module microcontroller (
  input clk,
  output mm_dspi_flash0_sck,
  inout mm_dspi_flash0_miso,
  inout mm_dspi_flash0_mosi,
  output mm_dspi_flash0_ss
);
  //Signals related to the reset generator.
  wire reset;

  //Signals related to the processor.
  wire cpu0_hold;
  wire cpu0_rd_en;
  wire [3:0] cpu0_wr_en;
  wire [31:2] cpu0_addr;
  wire [31:0] cpu0_rd_data;
  wire [31:0] cpu0_wr_data;

  //Signals related to the RAM.
  wire cpu_mem0_rd_en;
  wire [3:0] cpu_mem0_wr_en;
  wire [31:0] cpu_mem0_rd_data;

  //Signals related to the memory-mapped dual I/O SPI FLASH controller.
  wire mm_dspi_flash0_hold;
  wire mm_dspi_flash0_rd_en;
  wire [31:0] mm_dspi_flash0_rd_data;

  //I/O control signals.
  reg io_read;

  //Reset generator instance.
  simple_reset reset_gen (
    .clk(clk),
    .reset(reset)
  );

  //Tiny-rv32e processor instance.
  tiny_rv32e_cpu cpu0 (
    .clk(clk),
    .reset(reset),
    .hold(cpu0_hold),
    .rd_en(cpu0_rd_en),
    .wr_en(cpu0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //RAM instance.
  ram_32rw_4lwe #(
    .init_file(`CPU_MEM0_INIT_FILE),
    .length(`CPU_MEM0_LENGTH)
  )
  cpu_mem0 (
    .clk(clk),
    .rd_en(cpu_mem0_rd_en),
    .wr_en(cpu_mem0_wr_en),
    .addr(cpu0_addr),
    .rd_data(cpu_mem0_rd_data),
    .wr_data(cpu0_wr_data)
  );

  //Memory-mapped dual I/O SPI FLASH controller instance.
  mm_dspi_flash #(
    .clk_freq(`BOARD_CLOCK),
    .mem_size(`MM_DSPI_FLASH0_SIZE),
    .dummy_cycles(`MM_DSPI_FLASH0_DUMMY_CYCLES),
    .wake_up_time_us(`MM_DSPI_FLASH0_W_TIME_US)
  )
  mm_dspi_flash0 (
    //System signals.
    .clk(clk),
    .reset(reset),
    .hold(mm_dspi_flash0_hold),

    //Processor bus signals.
    .rd_en(mm_dspi_flash0_rd_en),
    .addr(cpu0_addr),
    .rd_data(mm_dspi_flash0_rd_data),

    //Peripheral I/O signals.
    .sck(mm_dspi_flash0_sck),
    .miso(mm_dspi_flash0_miso),
    .mosi(mm_dspi_flash0_mosi),
    .ss(mm_dspi_flash0_ss)
  );

  //Hold signal assignment for CPU0.
  assign cpu0_hold = mm_dspi_flash0_hold;

  //Decoding logic for multiplexing data reads from memory and I/O.
  always @(posedge clk) begin
    if (reset)
      io_read <= 1'b0;
    else if (!cpu0_hold)
      //Addresses from 0x40000000 are mapped to I/O.
      io_read <= cpu0_rd_en && (cpu0_addr[31:30] != 2'd0);
  end

  //Read data assignment for CPU0.
  assign cpu0_rd_data = io_read? mm_dspi_flash0_rd_data: cpu_mem0_rd_data;

  //Read and write enable assignments for RAM.
  assign cpu_mem0_rd_en = (cpu0_addr[31:30] == 2'd0)? cpu0_rd_en: 1'b0;
  assign cpu_mem0_wr_en = (cpu0_addr[31:30] == 2'd0)? cpu0_wr_en: 4'h0;

  //Read enable assignment for the memory-mapped dual I/O SPI FLASH controller.
  assign mm_dspi_flash0_rd_en = (cpu0_addr[31:30] == 2'd1)? cpu0_rd_en: 1'b0;
endmodule
