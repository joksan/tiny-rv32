#+-------------------------------------------------------------------------------------------------+
#| Makefile for the mm-dspi-flash-test softcore microcontroller.                                   |
#|                                                                                                 |
#| Note: This is a simulation-only softcore MCU. The definitions contained here are not sufficient |
#| for synthesis.                                                                                  |
#|                                                                                                 |
#| Author: Joksan Alvarado.                                                                        |
#+-------------------------------------------------------------------------------------------------+

#Set the softcore MCU path to the same location as this makefile.
MM_DSPI_FLASH_TEST_MCU_PATH := $(shell realpath --relative-to . $(dir $(lastword $(MAKEFILE_LIST))))

#Add the hdl subdirectory to the HDL source paths.
HDL_SRC_PATHS += $(MM_DSPI_FLASH_TEST_MCU_PATH)/hdl

#Add the HDL sources for this softcore MCU.
HDL_COMMON_SOURCES += microcontroller.v
HDL_SIM_SOURCES += testbench.v

#Add the base modules used by this softcore MCU.
BASE_MODULES += simple-reset
BASE_MODULES += ram-32rw-4lwe

#Add the peripherals used by this softcore MCU.
PERIPHERALS += mm-dspi-flash
PERIPHERALS += dspi-flash-sim

#Register the memory for the processor at address 0 with a default size of 1024 words (4KiB).
$(eval $(call F_ADD_CPU_BRAM,cpu-mem0,cpu_mem0,cpu0,0,1024))

#Register cpu0 as a tiny-rv32e softcore, using the bram-and-mm-flash-regions memory profile and
#minimal startup profile.
$(eval $(call F_ADD_SOFT_CPU,cpu0,tiny-rv32e,bram-and-mm-flash-regions,minimal-startup))

#Assign the simulated on-board SPI FLASH memory to the processor.
$(eval $(call F_SET_BOARD_SPI_FLASH_TO_CPU,cpu0,0x40000000))

#Definitions for the microcontroller module.
HDL_COMMON_DEFINES += CPU_MEM0_LENGTH=$(cpu-mem0-length)
HDL_SIM_DEFINES += CPU_MEM0_INIT_FILE=\"$(call F_SIM_BIF_COND,cpu-mem0)\"
HDL_COMMON_DEFINES += BOARD_CLOCK=$(BOARD_CLOCK)

#Pass memory configurations to the linker.
cpu0-ld-defines += RAM_SIZE_WORDS=$(cpu-mem0-length)
cpu0-ld-defines += FLASH_BASE_ADDR=$(APPLICATION_SPI_FLASH_BASE)
cpu0-ld-defines += FLASH_SIZE=$(APPLICATION_SPI_FLASH_SIZE)

#Configuration for the memory-mapped dual I/O SPI FLASH controller peripheral.
#-----------------------------------------------------------------------------
HDL_SIM_DEFINES += MM_DSPI_FLASH0_VLOG_FILE=\"$(APPLICATION_SPI_FLASH_VLOG_FILE)\"
HDL_COMMON_DEFINES += MM_DSPI_FLASH0_SIZE=$(BOARD_SPI_FLASH_SIZE)
HDL_COMMON_DEFINES += MM_DSPI_FLASH0_DUMMY_CYCLES=$(BOARD_SPI_FLASH_DUMMY_CYCLES)
HDL_SIM_DEFINES += MM_DSPI_FLASH0_USER_DATA_OFFSET=$(BOARD_SPI_FLASH_USER_DATA_OFFSET)
HDL_COMMON_DEFINES += MM_DSPI_FLASH0_W_TIME_US=$(call F_ZERO_IF_EMPTY,$(BOARD_SPI_FLASH_W_TIME_US))
