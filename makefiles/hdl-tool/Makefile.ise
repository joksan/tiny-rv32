#+-------------------------------------------------------------------------------------------------+
#| Makefile for Xilinx ISE functionality.                                                          |
#|                                                                                                 |
#| This makefile adds suport for the ISE toolset from Xilinx, which is used to perform the         |
#| complete FPGA workflow process from synthesis to bitfile generation. Since the install location |
#| can be changed by the user at will, it can be specified through a Make or environment variable  |
#| (read below for details).                                                                       |
#|                                                                                                 |
#| Please note: The ISE environment provides configuration scripts intented to be run by the user  |
#| before invoking the tools. Please refrain from running Make with these scripts loaded, as they  |
#| change the system library search order and might cause other tools to work improperly or not at |
#| all (ISE is discontinued and its libraries are considerably old at the time of this writing).   |
#| This makefile automatically deals with this situation by only loading the scripts when needed.  |
#|                                                                                                 |
#| Author: Joksan Alvarado                                                                         |
#+-------------------------------------------------------------------------------------------------+

#If not specified, set the ISE tools path to the default install location.
ISE_TOOLS_PATH ?= /opt/Xilinx/14.7/ISE_DS

#Make sure that either HDL_CONSTRAINT_PATH or HDL_CONSTRAINT_FILE variable is specified.
ifeq ($(HDL_CONSTRAINT_PATH),)
  ifeq ($(HDL_CONSTRAINT_FILE),)
    $(error HDL_CONSTRAINT_PATH and HDL_CONSTRAINT_FILE are not defined)
  endif
endif

#Make sure that the HDL_SYNTH_TOP_MODULE variable is specified.
ifeq ($(HDL_SYNTH_TOP_MODULE),)
  $(error HDL_SYNTH_TOP_MODULE is not defined)
endif

#Make sure that the ISE_DEVICE variable is specified.
ifeq ($(ISE_DEVICE),)
  $(error ISE_DEVICE is not defined)
endif

#Define the synthesis tool being used.
HDL_SYNTH_DEFINES += XST

#Define which supported devices belong in each supported family.
ISE_SPARTAN3E_DEVICES = xc3s500e% xc3s1200e%
ISE_SPARTAN6_DEVICES = xc6slx9%

#Check which device family is being used.
ifneq ($(filter $(ISE_SPARTAN3E_DEVICES), $(ISE_DEVICE)),)
  HDL_SYNTH_DEFINES += spartan spartan3e
  ISE_DEVICE_FAMILY = spartan3e
else ifneq ($(filter $(ISE_SPARTAN6_DEVICES), $(ISE_DEVICE)),)
  HDL_SYNTH_DEFINES += spartan spartan6
  ISE_DEVICE_FAMILY = spartan6
else
  $(error Unknown family for device: $(ISE_DEVICE))
endif

#If a constraint file wasn't assigned explicitly, then automatically set it to a .ucf file with the
#same name as the board at the constraint path.
HDL_CONSTRAINT_FILE ?= $(HDL_CONSTRAINT_PATH)/$(BOARD).ucf

#Filenames for all toolset targets.
ISE_PRJ_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).prj
ISE_XST_SCRIPT   = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).xst
ISE_NGC_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).ngc
ISE_BMM_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).bmm
ISE_NGD_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).ngd
ISE_MAP_NCD_FILE = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX)_map.ncd
ISE_MAP_PCF_FILE = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX)_map.pcf
ISE_NCD_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).ncd
HDL_BIT_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).bit
HDL_BIN_FILE     = $(HDL_BOARD_OUTPUT_DIR)/$(HDL_OUTPUT_PREFIX).bin

#Set the configuration script based on system architecture.
ifeq ($(shell uname -m),x86_64)
  ISE_SETTINGS_SCRIPT = $(ISE_TOOLS_PATH)/settings64.sh
else
  ISE_SETTINGS_SCRIPT = $(ISE_TOOLS_PATH)/settings32.sh
endif

#Commands used to configure ISE tools before execution. Unfortunately these tools have a tendency to
#create quite a lot of (mostly unwanted) files. Since this behavior cannot be controlled by program
#arguments, the directory they run from is changed to the board output directory.
ISE_SETUP = . $(ISE_SETTINGS_SCRIPT); cd $(HDL_BOARD_OUTPUT_DIR);

#This function returns the path to a given file relative to the board output directory. This is used
#to pass files to the ISE tools.
ISE_RP = $(shell realpath -sm --relative-to=$(HDL_BOARD_OUTPUT_DIR) $1)

#Command used to invoke the BMM generator script from its location.
BMM_GEN = python3 -B $(TINY_RV32_PATH)/scripts/python/bmm-gen.py

#Rule used to create the project file for XST.
$(ISE_PRJ_FILE): $$(HDL_SYNTH_SOURCES) $$(HDL_COMMON_SOURCES) | $$(call F_DIR,$$@)
	@echo creating prj file
	@echo $(call ISE_RP,$^) | sed 's/ /\n/g' | sed 's/^/verilog work /g' > $@

#Rule used to create XST script file.
$(ISE_XST_SCRIPT): $(ISE_PRJ_FILE) | $$(call F_DIR,$$@)
	@echo creating xst file
	@echo run > $@
	@echo -ifn $(call ISE_RP,$(ISE_PRJ_FILE)) >> $@
	@echo -ifmt mixed >> $@
	@echo -p $(ISE_DEVICE) >> $@
	@echo -top $(HDL_SYNTH_TOP_MODULE) >> $@
	@echo -ofn $(call ISE_RP,$(ISE_NGC_FILE)) >> $@
	@echo -ofmt NGC >> $@
	@echo -opt_mode Speed >> $@
	@echo -opt_level 1 >> $@
	@echo -define { $(HDL_SYNTH_DEFINES) $(HDL_COMMON_DEFINES) } >> $@
	@echo -use_new_parser yes >> $@

#Rule used to synthesize HDL sources with XST.
SYNTHESIS_TARGET = $(ISE_NGC_FILE)
$(ISE_NGC_FILE): $(ISE_XST_SCRIPT) | $$(call F_DIR,$$@)
	$(ISE_SETUP) xst -ifn $(call ISE_RP,$<)

#Rule used to automatically generate the block memory map (BMM) file.
$(ISE_BMM_FILE): | $$(call F_DIR,$$@)
	$(BMM_GEN) $@ $(ISE_DEVICE_FAMILY) \
	$(foreach B,$(BRAM_UNITS),$(B),$($(B)-instance),$($(B)-width),$($(B)-length))

#Rule used to invoke NGDBuild, which performs translation from generic to native description.
$(ISE_NGD_FILE): $(ISE_NGC_FILE) $(ISE_BMM_FILE) $(HDL_CONSTRAINT_FILE) | $$(call F_DIR,$$@)
	$(ISE_SETUP) ngdbuild -bm $(call ISE_RP,$(filter %.bmm,$^)) \
	-uc $(call ISE_RP,$(filter %.ucf,$^) $< $@)

#Rule used to call MAP, which maps the native description to the target FPGA device components.
$(ISE_MAP_NCD_FILE): $(ISE_NGD_FILE) | $$(call F_DIR,$$@)
	$(ISE_SETUP) map -w -o $(call ISE_RP,$@ $<)

#Rule used to call PAR, which places and routes the design.
PAR_TARGET = $(ISE_NCD_FILE)
$(ISE_NCD_FILE): $(ISE_MAP_NCD_FILE) | $$(call F_DIR,$$@)
	$(ISE_SETUP) par -w $(call ISE_RP,$< $@)

#This rule creates symbolic links to BRAM initialization data files in the output directory.
#Although verilog data files can be fed directly to the BitGen tool, this tool refuses to take files
#with extensions other than .mem, so links are created in order to "rename" them.
$(HDL_BOARD_OUTPUT_DIR)/%.mem: $(HDL_BOARD_OUTPUT_DIR)/%.data | $$(call F_DIR,$$@)
	ln -srf $< $@

#Note: The extensions are kept separate as in the future we may need to create more complex memory
#maps with an script (adding addresses makes the formats incompatible). Also we don't want to change
#the file extension just for the sake of usage of tool that is discontinued anyway.

#Rule used to call BitGen, which performs bitstream generation and block ram data initialization.
#Note that the bitstream is generated in both .bit and .bin formats.
BITGEN_TARGET = $(HDL_BIT_FILE)
$(HDL_BIT_FILE) $(HDL_BIN_FILE): $(ISE_NCD_FILE)\
                                 $$(foreach B,$$(BRAM_UNITS),$$(HDL_BOARD_OUTPUT_DIR)/$$(B).mem)\
                                 | $$(call F_DIR,$$@)
	$(ISE_SETUP) bitgen -w $(foreach B,$(BRAM_UNITS),-bd $(B).mem tag $(B)) -g Binary:Yes \
	$(call ISE_RP,$< $(HDL_BIT_FILE))

#Rule used to call TRACE, which performs timing analysis.
.PHONY: ise-timing
TIMING_TARGET = ise-timing
ise-timing: $(ISE_NCD_FILE)
	$(ISE_SETUP) trce $(call ISE_RP,$< $(ISE_MAP_PCF_FILE))
