#+-------------------------------------------------------------------------------------------------+
#| Makefile with common functionality for software tools.                                          |
#|                                                                                                 |
#| This makefile provides functionality that is shared between all software tools. This includes   |
#| default settings, shared variables, path configurations, common recipes for output directories  |
#| and target dependency management.                                                               |
#|                                                                                                 |
#| Author: Joksan Alvarado                                                                         |
#+-------------------------------------------------------------------------------------------------+

#Default filename settings.
SW_OUTPUT_DIR ?= sw-build

#Output directory for board-related files.
SW_BOARD_OUTPUT_DIR = $(SW_OUTPUT_DIR)/$(BOARD)

#Rule used to create board-specific output directories.
$(SW_BOARD_OUTPUT_DIR):
	mkdir -p $@

#Rule used to clean all generated software output files for all boards.
.PHONY: clean-sw
clean-sw:
	rm -rf $(SW_OUTPUT_DIR)

#Have "clean" depend on "clean-sw" so everything gets cleaned when requested.
clean: clean-sw

#The "sw" target is expected to run the processes for code compilation, linking and image generation
#for every CPU in the current project. Targets that implement this are expected to have "sw" depend
#on them by assigning SW_TARGET.
.PHONY: sw
sw: $$(SW_TARGET)
